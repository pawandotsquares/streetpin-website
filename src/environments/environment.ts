// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  aws_region: 'us-east-1',
  aws_bucketName: 'websiteimageupload',
  aws_bucketURL: 'https://websiteimageupload.s3.amazonaws.com/',
  //aws_bucketName: 'adminstreetpin',
  aws_folderName: '_cmsImages',
  region: 'eu-west-2',
  aws_IdentityPoolId: 'us-east-1:e527713d-bc34-4c54-8e32-4dc471201d6b',
  aws_apiVersion: '2012-10-17',
  facebook_client_id: '1393148214161425',
  google_client_id: '398947725024-shuck13e1q3v2nb37p11gj6bsdvudolv.apps.googleusercontent.com',
  twitter_in_client_id: '2Wf9libijF3Lsh2rxvA',
  site_url: 'http://192.168.2.86:4200/',
};
export const spinnerConfig: any = {
  bdColor: 'rgba(51,51,51,0.8)',
  size: 'large',
  color: '#fff',
  type: 'ball-circus',
  loadigText: 'Loading...'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
