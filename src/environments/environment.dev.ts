export const environment = {
  production: true,

  aws_region: 'us-east-1',
  region: 'eu-west-2',
  //aws_bucketName: 'adminstreetpin',
  aws_bucketName: 'websiteimageupload',
  aws_bucketURL: 'https://websiteimageupload.s3.amazonaws.com/',
  aws_folderName: '_cmsImages',
  aws_IdentityPoolId: 'us-east-1:e527713d-bc34-4c54-8e32-4dc471201d6b',
  aws_apiVersion: '2012-10-17',
  facebook_client_id: '1393148214161425',
  google_client_id: '398947725024-shuck13e1q3v2nb37p11gj6bsdvudolv.apps.googleusercontent.com',
  twitter_in_client_id: '2Wf9libijF3Lsh2rxvA',
  site_url: 'https://d2c1vgyoekfl5p.cloudfront.net/',
};