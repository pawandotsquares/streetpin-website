import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let url: string = state.url;
    return this.verifyLogin(url);
  }
  verifyLogin(url): boolean {

    if (!localStorage.getItem('userId')) {
      this.router.navigate(['/login']);
      return false;
    }
    if (!this.isLoggedIn()) {
      this.router.navigate(['/login']);
      return false;
    }
    else if (this.isLoggedIn()) {
      return true;
    }
  }
  public isLoggedIn(): boolean {
    let status = false;
    if (localStorage.getItem('isLoggedIn') == "true" && localStorage.getItem('role') == "web") {
      status = true;
    }
    else {
      status = false;
    }
    return status;
  }
}
