import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  logout(): void {
    localStorage.removeItem("SPcurrentUser");
    localStorage.removeItem('token');
    localStorage.setItem('isLoggedIn', "false");
    localStorage.setItem('webLoggedIn', "false");
    localStorage.removeItem('userId')
    //localStorage.removeItem('returnUrl')
    localStorage.removeItem('WebUserProfileImage')
    /* localStorage.clear(); */
  }
}
