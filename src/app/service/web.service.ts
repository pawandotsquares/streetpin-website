import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { Profile } from '../models/profile';
import { Pinboard } from 'src/app/models/pinboard';
import { Offer } from 'src/app/models/Offer';
import { Post } from 'src/app/models/Post';
//const apiUrl = 'https://ylqndxd5v1.execute-api.eu-west-2.amazonaws.com/dev';
//const apiUrl = 'https://3g79de4fq7.execute-api.eu-west-2.amazonaws.com/uat';
const apiUrl = 'https://ye2xe6v0w7.execute-api.eu-west-2.amazonaws.com/production';
import * as crypto from 'crypto-js';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};
const secret = "@AKIAJSN26KWXJ45CJJRFUA@";
@Injectable({
  providedIn: 'root'
})
export class WebService {
  public notification = new Subject<any>();

  constructor(private http: HttpClient) {
  }


  /* updateNotification(notificationDetail: any) {
    this.notification.next(notificationDetail);
  }

  subscribeNotification(): Observable<any> {
    return this.notification.asObservable();
  } */

  userlogin(email, password, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/user`, { email: email, password: password }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getlatlng(address) {

    var address1 = "address=" + address;
    var sensor = "sensor=false";
    var key = "key=AIzaSyCDt53lCO5hH0Ka6dYDLDdlRoeCIuGIq_M";
    var parameters = address1 + "&" + sensor + "&" + key;
    var output = "json";
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/' + output + '?' + parameters).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));

  }
  getAddressFromLatLng(address) {

    var latlng = "latlng=40.714224,-73.961452";
    var sensor = "sensor=false";
    var key = "key=AIzaSyCDt53lCO5hH0Ka6dYDLDdlRoeCIuGIq_M";
    var parameters = latlng + "&" + sensor + "&" + key;
    var output = "json";
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/' + output + '?' + parameters).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));

  }
  userSignUp(Profile: Profile, action): Observable<Profile> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/user`, Profile, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  profileData(userId, action){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/user`, { userId: userId}, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  publicProfileData(userName, action) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/user`, { userName: userName }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getUserData() {
    return this.http.post(apiUrl + `/getusers`, {}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  offerData(Offers: Offer, action): Observable<Offer> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/offer`, Offers, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  offersAction(id, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/offer`, { id: id }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getMyofferList(id, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/offer`, { userId: id }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getMyPostList(id, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/post`, { userId: id }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getCommentByPostId(id, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/post`, { id: id }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  addPostData(Post: Post, action): Observable<Post> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/post`, Post, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getPostCategory(action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/post`, {}, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  postAction(id, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/post`, { id: id }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  addComment(commentData, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/post`,  commentData , httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getOfferCategory(action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/offer`, {}, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  addOfferData(Offers: Offer, action, newExpDate, newStartDate): Observable<Offer> {

    Offers.startDate = newStartDate;
    Offers.expiryDate = newExpDate;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/offer`, Offers, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  profileUpdate(Profile: Profile, action): Observable<Profile> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/user`, Profile, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  verifyEmail(authKey, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/user`, { authKey: authKey }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  /* profileData(email, password) {
    return this.http.post(apiUrl + '/auth', { email: email, password: password }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  } */
  forgotPassword(email, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, { email: email }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  resetPassword(token, newPassword, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, { token: token, newPassword: newPassword }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getSubscriptionPlanList(action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, { }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  pinboardplan(action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/subscriptions-plan`, {"planId": 0,"planType": "1"}, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  subscribePlan(Subscription, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };

    return this.http.post(apiUrl + `/user`, Subscription, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  checkCouponValidity(promoCode, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };

    return this.http.post(apiUrl + `/user`, promoCode, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getUserSubscribedPlanList(userId, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, { userId: userId }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getUserCredits(userId, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, { userId: userId }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getMyPinboardList(userId, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/pinboard`, { userId: userId }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getPinboardIdByName(pinboardName, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/pinboard`, { pinboardName: pinboardName }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getCategories(action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, {}, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getUserNotification(userId, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, { userId: userId}, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getPinboardDetailsWithPostAndOffers(id, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/pinboard`, { id: id }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getPinboardDetailsWithPostAndOffersByName(pinboardName, pinboardType, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/pinboard`, { pinboardName: pinboardName, pinboardType: pinboardType }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getPinboardFollowerCount(pinboardId, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, { pinboardId: pinboardId }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  follow(userId, pinboardId, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/user`, { userId: userId, pinboardId: pinboardId }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getNearByPinBoard(latitude, longitude, radiusInMeter, categoryName, searchKeyWord, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/pinboard`, { latitude: latitude, longitude: longitude, radiusInMeter: radiusInMeter, categoryName: categoryName, searchKeyWord: searchKeyWord }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getNearByLocationPinBoardList(latitude, longitude, radiusInMeter, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/pinboard`, { latitude: latitude, longitude: longitude, radiusInMeter: radiusInMeter}, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getNearByLocationPinBoardListWithUser(userId, latitude, longitude, radiusInMeter, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),
    };
    return this.http.post(apiUrl + `/pinboard`, { userId: userId, latitude: latitude, longitude: longitude, radiusInMeter: radiusInMeter }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  pinboardContent(Pinboard: Pinboard, action): Observable<Pinboard> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/pinboard`, Pinboard, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  addPinboardData(Pinboard: Pinboard, newExpDate): Observable<Pinboard> {

   /*  Pinboard.expiryDate = newExpDate; */
    /* Pinboard.logo = logo
    Pinboard.backgroundImage = backgroundImage */
    /* Pinboard.userId = 2; */
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': "add"
      }),

    };
    /* body.expiryDate = newExpDate; */
    return this.http.post<Pinboard>(apiUrl + `/pinboard`, Pinboard, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getPageContent(pageId) {
    return this.http.post(apiUrl + `/get-static-page-list`, { "staticPageId": 0, "masterPageId": pageId, isWebsite : true }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  pinboardAction(id, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/pinboard`, { id: id }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  refreshAuthToken(authToken, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/user`, { authToken: authToken }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  setCurrentUser(currentUser: Profile): void {
    localStorage.setItem('SPcurrentUser', this.encrypt(currentUser));
  }
  getCurrentUser(): Profile {
    let data = localStorage.getItem('SPcurrentUser');
    if (data) {
      return new Profile(this.decrypt(data));
    }
    return null;

  }
  encrypt(data) {
    return crypto.AES.encrypt(JSON.stringify(data), secret,
      {
        keySize: 128 / 8,
        iv: secret,
        mode: crypto.mode.CBC,
        padding: crypto.pad.Pkcs7
      }).toString();
  }

  decrypt(data) {
    return JSON.parse(crypto.enc.Utf8.stringify(crypto.AES.decrypt(data, secret,
      {
        keySize: 128 / 8,
        iv: secret,
        mode: crypto.mode.CBC,
        padding: crypto.pad.Pkcs7
      })));
  }
}
