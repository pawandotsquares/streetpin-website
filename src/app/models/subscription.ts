export class Subscription {
    public planId: number;
    public planName: String = "";
    public description: String = "";
    public planAmount: number;
    public credits: number;
  ;

    constructor(params: any) {
        Object.assign(this, params);
    }
}