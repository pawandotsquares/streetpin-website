export interface BusinessInformation {
     id: number;
     userId: number;
     isAgreedProposal: number;
     pinboardStatus: number;
     phone: number;
     title: String;
     type: String;
     website: String;
     email: String;
     location: string;
     openingHours:any;
     backgroundImage: string;
     logo: string;
     about: string;
}