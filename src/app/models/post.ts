export interface Post {
    id: number;
    title: string
    userId: number;
    categoryId: number;
    pinboardId: number;
    pinboardTitle: any;
    imageAangle: any;
    category: number;
    createdDate: Date
    description: string
    expiryDate: Date
    image: string
    isActive: number
    isDeleted: number
    expiryTimePeriod:any
    isReported: number
    latitude: number
    longitude: number
    tags: string
    updatedDate: Date
    isChangeExpiryDate: any
    userName: any
}