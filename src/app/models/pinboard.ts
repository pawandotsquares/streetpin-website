import { BusinessInformation } from "./businessInformation";

export class Pinboard {
    public id: number;
    public userId: number;
    public user: any;
    public commentCount: any;
    public competingCategoriesListObj: any;
    public catId: number;
    public isLocationChange: boolean;
    public userNew: any;
    public isAgreedProposal: number;
    public pinboardStatus: number;
    public title: String = "";
    public type: any;
    public paymentId: any;
    public categoryId: any;
    public categoryName: any;
    public userLatitude: any;
    public adminUserId: any;
    public userLongitude: any;
    public latitude: any;
    public longitude: any;
    public backgroundImage: string = "";
    public isPaid: boolean;
    public planId: any;
    public logo: string = "";
    public categoryImageUrl: string = "";
    public categoryImage: string = "";
    public expiryDate: Date;
    public isAlllowOffers: any;
    public competingCategoriesList: any;
    public autoRecurring: boolean;
    public months: any;
    public subscriptionPlanId: any;
    public isAllowPosts: any;
    public businessInformation: BusinessInformation;

    constructor(params: any) {
        Object.assign(this, params);
        let businessInformation = params && params.businessInformation ? params.businessInformation : {};
        this.businessInformation = businessInformation as BusinessInformation
    }


}