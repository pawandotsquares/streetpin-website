export interface Offer {
    id: number;
    userId: number;
    categoryId: number;
    totalChargedCredit: number;
    offerName: String;
    title: String;
    emailId: String;
    webUrl: String;
    offerMedia: String;
    offerIcon: String;
    category: String;
    servicesHashtags: String;
    location: String;
    latitude: String;
    longitude: String;
    startDate: any;
    expiryDate: any;
    contactNumber: number;
    offerDescription: String;
    backgroundColor: String;
    isDeleted: number;
    pinboardList: any;
    pinboardObjList: any;
    userName: any;
}