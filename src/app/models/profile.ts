export class Profile {
    public userId: number;
    public firstName: String = "";
    public lastName: String = "";
    public email: String = "";
    public userName: String = "";
    public password: String = "";
    public profileImage: String = "";
    public websiteUrl: String = "";
    public intro: String = "";
    public phoneNumber: any;
    public acceptTC: boolean;
    public authToken: string = "";
    public active: boolean;
    public weeklyUpdates: boolean;
    public isInstantAlerts: boolean;
    public userRole: any;
    location: String;
    latitude: String;
    longitude: String;

    constructor(params: any) {
        Object.assign(this, params);
    }
}