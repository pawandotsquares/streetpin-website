import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-user-subscription',
  templateUrl: './user-subscription.component.html',
  styleUrls: ['./user-subscription.component.css']
})
export class UserSubscriptionComponent implements OnInit {
  @Output('changeModule')
  moduleChanged = new EventEmitter();
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  userId:any;
  subscriptionArr : any;
  totalCredits : any;
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    /* this.subscriptionList(); */
    this.getUserCredits();
  }
  getUserCredits(){
    this.WebService.getUserCredits(this.userId, 'getUserCredits').subscribe(resultCreditsData => {
      console.log(resultCreditsData);
      this.spinner.hide();
      if (resultCreditsData['statusCode'] == 200) {
        this.totalCredits = resultCreditsData['responsePacket']['totalCredits'];
      } else {
        /* this.toastr.error(resultCreditsData['message']); */
      }
    },
      error => {

      })
  }
  subscriptionList(){
    this.WebService.getUserSubscribedPlanList(this.userId, 'getUserSubscribedPlanList').subscribe(resultSubscriptionData => {
      this.spinner.hide();
      if (resultSubscriptionData['statusCode'] == 200) {
        this.subscriptionArr = resultSubscriptionData['responsePacket'];
      } else {
        /* this.toastr.error(resultSubscriptionData['message']); */
      }
    },
      error => {

      })
  }
}
