import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { WebService } from 'src/app/service/web.service';
import { Subscription } from 'rxjs';
import * as hello from 'hellojs/dist/hello.all.js'

@Component({
  selector: 'app-static-menu',
  templateUrl: './static-menu.component.html',
  styleUrls: ['./static-menu.component.css']
})
export class StaticMenuComponent implements OnInit {

  @Output() parentButtonClick = new EventEmitter();
  public filterBoxOpen: boolean = false;
  webLoggedIn: any;

  handleParentClick() {
    localStorage.setItem("findMe",'5')
    this.router.navigate(['/']);
    this.parentButtonClick.emit(true);
  }

  constructor(
    private authService: AuthService,
    private router: Router,
    private WebService: WebService
  ) { }


  ngOnInit() {

    this.webLoggedIn = localStorage.getItem('webLoggedIn');
    if (this.webLoggedIn == 'true') {
      this.webLoggedIn = 'true';

    } else {
      this.webLoggedIn = 'false';
    }

  }
  logout(): void {
    this.authService.logout();
    //this.helloLogout();
    this.router.navigate(['/login']);
  }

  helloLogout() {
    hello('windows').logout().then(function () {
      console.log('logout');
    }, function (e) {
      console.log('Not logout' + e);
    })
  }
}
