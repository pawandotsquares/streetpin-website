import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { WebService } from 'src/app/service/web.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  webLoggedIn: any;
  unViewedCount: number;
  userId: any;
  WebUserProfileImage: any;
  searchBoxShow:boolean;
  public navActive: boolean = false;
  public searchActive: boolean = true;
  subscription: Subscription;
  constructor(
    private authService: AuthService,
    private router: Router,
    private WebService: WebService
  ) { }

  ngOnInit() {
    /* this.subscription = this.WebService.subscribeNotification().subscribe((notification) => {
      alert(notification);
    }); */

    this.searchBoxShow = false
    this.webLoggedIn = localStorage.getItem('webLoggedIn');
    this.WebUserProfileImage = localStorage.getItem('WebUserProfileImage');
    this.userId = localStorage.getItem('userId');
    if (this.webLoggedIn == 'true') {
      this.webLoggedIn = 'true';
    } else {
      this.webLoggedIn = 'false';
    }

    //this.getUserNotification();
  }
  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  getUserNotification() {
    this.WebService.getUserNotification(this.userId, 'getUserNotification').subscribe(resultNotificationCount => {
      if (resultNotificationCount['statusCode'] == 200) {
        this.unViewedCount = resultNotificationCount['responsePacket']['unViewedCount'];

      }
    },
      error => {

      })
  }


  /* ngOnDestroy(): void {
    this.subscription.unsubscribe();
  } */

}
