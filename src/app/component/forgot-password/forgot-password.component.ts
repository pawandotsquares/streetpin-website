import { Component, OnInit } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  public user: any = {};
  returnUrl: string;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  token: any;
  htmlOptions: any = {};
  constructor(
    private WebService: WebService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  //fucntion called when form submitted: add and edit form
  onSubmit() {
    this.spinner.show();
    this.WebService.forgotPassword(this.user.email, 'userForgotPassword').subscribe(result => {
      this.spinner.hide();
      if (result['statusCode'] == 200) {
        this.toastr.success('Email has been sent', 'Success');
        let self = this;
        setTimeout(function () {
          self.router.navigate(['/login']);
        }, 500);
      } else {
        this.toastr.error(result['message']);
      }
    },
      error => {

      })
  }
}
