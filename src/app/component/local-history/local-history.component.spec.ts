import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalHistoryComponent } from './local-history.component';

describe('LocalHistoryComponent', () => {
  let component: LocalHistoryComponent;
  let fixture: ComponentFixture<LocalHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
