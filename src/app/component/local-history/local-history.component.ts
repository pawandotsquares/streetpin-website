import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { WebService } from 'src/app/service/web.service';
import { Profile } from 'src/app/models/profile';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-local-history',
  templateUrl: './local-history.component.html',
  styleUrls: ['./local-history.component.css']
})
export class LocalHistoryComponent implements OnInit {
  webLoggedIn: any;
  userId: any;
  public Profile: Profile = new Profile({});
  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private WebService: WebService,
  ) { }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    this.webLoggedIn = localStorage.getItem('webLoggedIn');
  if (this.webLoggedIn == 'true') {
    this.webLoggedIn = 'true';
     if (localStorage.getItem("LoggedInTime")){
      var t0 = Number(localStorage.getItem("LoggedInTime"));
      if (isNaN(t0)) t0 = 0;
      var t1 = new Date().getTime();
      var duration = t1 - t0;
      if (duration < 55 * 60 * 1000) {
        if (duration > 40 * 60 * 1000) {
          this.refreshAuthToken();
          var loginTime = new Date().getTime();
          localStorage.setItem("LoggedInTime", loginTime.toString());
        }
      } else {
        this.toastr.error("Your session has been expired, Please log in again.");
        this.logout();
      }
    }

  } else {
    this.webLoggedIn = 'false';
  }
  }
  logout(): void {
    localStorage.removeItem("SPcurrentUser");
    localStorage.removeItem('userId')
    localStorage.setItem('isLoggedIn', "false");
    localStorage.setItem('webLoggedIn', "false");
    this.authService.logout();
    this.router.navigate(['/login']);
    localStorage.removeItem('WebUserProfileImage')
  }
  refreshAuthToken(){
    var token = this.WebService.getCurrentUser().authToken;
    this.WebService.refreshAuthToken(token, 'refreshAuthToken').subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        var authToken = resultProfileData['responsePacket'].authToken;
        this.getProfileData(authToken)
      }
    },
      error => {

      })
  }
  getProfileData(authToken){
    this.WebService.profileData(this.userId, 'getUserProgile').subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        this.Profile = new Profile(resultProfileData['responsePacket'])
        this.Profile.authToken = authToken;
        this.WebService.setCurrentUser(this.Profile);
      }
    },
      error => {

      })
  }
}
