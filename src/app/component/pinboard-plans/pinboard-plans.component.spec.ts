import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinboardPlansComponent } from './pinboard-plans.component';

describe('PinboardPlansComponent', () => {
  let component: PinboardPlansComponent;
  let fixture: ComponentFixture<PinboardPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinboardPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinboardPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
