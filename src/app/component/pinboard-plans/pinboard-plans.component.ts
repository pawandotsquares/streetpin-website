import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
/* import { Subscription } from 'src/app/models/subscription'; */
import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from "ngx-stripe";
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-pinboard-plans',
  templateUrl: './pinboard-plans.component.html',
  styleUrls: ['./pinboard-plans.component.css']
})
export class PinboardPlansComponent implements OnInit {
  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  user: any = {};
  promoCode: any = {};
  htmlOptions: any = {};
  couponData: any;
  couponDiscountValue: any;
  oldPrice: any;
  totalCredits: any;
  cardOptions: ElementOptions = {
    hidePostalCode: true,
    style: {
      base: {
        iconColor: 'rgb(45, 48, 51)',
        color: 'rgb(114, 114, 114)',
        lineHeight: '40px',
        // fontWeight: 300,
        // fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '16px',
        '::placeholder': {
          color: 'rgb(114, 114, 114)'
        }
      }
    }
  };
  elementsOptions: ElementsOptions = {
    locale: 'en'
  };
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  userId: any;
  webLoggedIn: any;
  subscription: any = {};
  subscriptionArr: any;
  subscriptionPlanList: any;

  /* public Profile: Subscription = new Subscription({}); */
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authService: AuthService,
    private stripeService: StripeService,
  ) { }

  ngOnInit() {
    this.webLoggedIn = localStorage.getItem('webLoggedIn');
    console.log(this.webLoggedIn)
    localStorage.setItem('returnUrl', '/pricing');
    this.htmlOptions = {
      haveApplyCode: true,
      applyCode: false,
      appliedCode: false
    }

    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    this.pinboardPlanList();
    this.subscriptionList();

  }

  pinboardPlanList() {
    this.WebService.pinboardplan('get').subscribe(resultSubscriptionData => {
      this.spinner.hide();
      if (resultSubscriptionData['statusCode'] == 200) {
        console.log(resultSubscriptionData['responsePacket']);
        this.subscriptionArr = resultSubscriptionData['responsePacket'];
      } else {
        /* this.toastr.error(resultSubscriptionData['message']); */
      }
    },
      error => {

      })
  }
  pinboardPlanIDs(planId: any){
    localStorage.setItem('planId', planId)
    this.router.navigateByUrl('/mypinboard');
  }

  subscriptionList() {
    this.WebService.getSubscriptionPlanList('getSubscriptionPlanList').subscribe(resultSubscriptionData => {
      this.spinner.hide();
      if (resultSubscriptionData['statusCode'] == 200) {
        console.log(resultSubscriptionData['responsePacket']);
        this.subscriptionPlanList = resultSubscriptionData['responsePacket'];

      } else {
        /* this.toastr.error(resultSubscriptionData['message']); */
      }
    },
      error => {

      })
  }
}
