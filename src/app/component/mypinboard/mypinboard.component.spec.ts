import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MypinboardComponent } from './mypinboard.component';

describe('MypinboardComponent', () => {
  let component: MypinboardComponent;
  let fixture: ComponentFixture<MypinboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MypinboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MypinboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
