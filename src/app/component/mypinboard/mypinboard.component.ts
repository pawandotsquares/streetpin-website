import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { Pinboard } from 'src/app/models/pinboard';
import * as AWS from 'aws-sdk'
import { AuthService } from 'src/app/service/auth.service';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from "ngx-stripe";
import { NgxImageCompressService } from 'ngx-image-compress';
import { getDistanceFromLatLonInKm } from 'src/app/util/helper';

declare var $: any;

@Component({
	selector: 'app-mypinboard',
	templateUrl: './mypinboard.component.html',
	styleUrls: ['./mypinboard.component.css']
})

export class MypinboardComponent implements OnInit {

	@ViewChild(StripeCardComponent) card: StripeCardComponent;
	@ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent;

	public orientation: number = -1;
	imageChangedEvent: any = '';
	imageChangedEventLogo: any = '';
	imageChangedEventBackground: any = '';
	isUploaded: any = '';
	isLogo: boolean;
	isBackground: boolean;
	croppedImage: any = '';
	showCropper = true;
	public show: boolean = false;
	public IsCropedImage: boolean = false;
	public tampImageWithoutCrop: string = "";
	public tampImageWithCrop: string = "";
	public tampImageWithoutCropL: string = "";
	public tampImageWithCropB: string = "";
	cropImageObj: any = {};

	couponData: any;
	pinobardPreviousStatus: any;
	pinobardPreviousType: any;
	couponDiscountValue: any;
	oldPrice: any;
	yearlyCharges: number;
	MonthlyCharges: number;
	latC: any;
	lngC: any;
	isPublishedClicked: any;
	oldExpiryDate: any;
	oldPlanPrice: any;
	oldPlanCredit: any;
	planAmount: any;
	planId: any;
	credits: any;
	isValid: boolean
	monthDrowdown: boolean;
	dropdownList = [];
	pinboardCatArrayOld = [];
	pinboardCatArray = [];
	pinboardCatArrayObj = [];
	dropdownSettings = {};
	finalExpiryDate: any;
	cardOptions: ElementOptions = {
		hidePostalCode: true,
		style: {
			base: {
				iconColor: 'rgb(45, 48, 51)',
				color: 'rgb(114, 114, 114)',
				lineHeight: '40px',
				// fontWeight: 300,
				// fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSize: '16px',
				'::placeholder': {
					color: 'rgb(114, 114, 114)'
				}
			}
		}
	};
	subscription: any = {};
	promoCode: any = {};
	elementsOptions: ElementsOptions = {
		locale: 'en'
	};
	@Output('changeModule')
	public moduleChanged = new EventEmitter<string>();
	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};
	p: number = 1;
	pinboardData: any;
	currDate: any;
	userId: any;
	firstName: any;
	newPinUser: any;
	email: any;
	pinUser: any = [];
	minDate: Date;
	htmlOptions: any = {};
	logo: string;
	categoryImage: string;
	backgroundImage: string;
	pinboardArr: any;
	imageUplaodOption: boolean;
	freeType: boolean;
	planList: boolean;
	paidPinboard: boolean;
	categorydata: any;
	subscriptionArr: any;
	globalFilesArray: any = []
	fromTimeArray = [];
	toTimeArray = [];
	users: any = [];

	public pinboard: Pinboard = new Pinboard({});

	constructor(
		private WebService: WebService,
		private router: Router,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
		private authService: AuthService,
		private stripeService: StripeService,
		private imageCompress: NgxImageCompressService,
	) { }
	openingTime: any = {
		mon: {
			checked: true,
			start: '9 AM',
			end: '10 PM'
		},
		tue: {
			checked: true,
			start: '9 AM',
			end: '10 PM'
		},
		wed: {
			checked: true,
			start: '9 AM',
			end: '10 PM'
		},
		thu: {
			checked: true,
			start: '9 AM',
			end: '10 PM'
		},
		fri: {
			checked: true,
			start: '9 AM',
			end: '10 PM'
		},
		sat: {
			checked: false,
			start: '',
			end: ''
		},
		sun: {
			checked: false,
			start: '',
			end: ''
		}
	}
	ngOnInit() {
		this.cropImageObj = {
			IsLogoCropedImage: false,
			IsBackgroundCropedImage: false,
			showCropperB: true,
			showCropperL: true,
		};
		this.isLogo = false;
		this.isBackground = false;
		localStorage.setItem('returnUrl', '/mypinboard');
		this.pinboard.subscriptionPlanId = null
		this.pinboard.months = 1;
		this.pinobardPreviousStatus = 1;
		this.pinobardPreviousType = 0;
		this.oldExpiryDate = ""
		this.isPublishedClicked = false;
		this.paidPinboard = false;
		this.monthDrowdown = false;
		this.freeType = false;
		this.planList = false;
		this.pinboard.autoRecurring = true;
		this.pinboard.isLocationChange = false
		this.minDate = new Date();
		this.spinner.show();
		this.userId = localStorage.getItem('userId');
		this.email = localStorage.getItem('webEmail');
		this.pinUser = { userId: this.userId, email: this.email };
		this.newPinUser = [this.pinUser]
		/* this.pinUser.push({ userId: this.userId, firstName: this.firstName }); */
		this.myPinboardList();
		this.populate()
		this.getCategories();
		this.pinboardPlanList();
		this.setHtmlOptions();
		//this.getOfferCategory();
		this.dropdownSettings = {
			singleSelection: false,
			enableCheckAll: false,
			idField: 'categoryId',
			textField: 'categoryName',
			selectAllText: 'Select All',
			unSelectAllText: 'Deselect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};


	}
	//fucntion is used for add edit view show hide
	setHtmlOptions() {
		this.htmlOptions = {
			editMode: false,
			haveApplyCode: true,
			applyCode: false,
			appliedCode: false
		}

		if (localStorage.getItem('planId') == '1' || localStorage.getItem('planId') == '2' || localStorage.getItem('planId') == '3') {
			this.addPinboard()
		}
	}

	//fucntion is called when user click on form cancle button
	cancelButton() {
		this.htmlOptions['editMode'] = false
		this.cancelPromoCode()

	}
	saveButton() {
		this.cancelPromoCode()
		this.isPublishedClicked = false;
		if (this.pinboard.type == 2 && !this.pinboard.isPaid) {
			this.pinboard.pinboardStatus = 1;
		} else {
			if (this.pinobardPreviousStatus == 2) {
				this.pinboard.pinboardStatus = this.pinobardPreviousStatus;
			} else {
				this.pinboard.pinboardStatus = 1;
			}

		}

		document.getElementById("formSubmit").click();
		//document.getElementById('ngFormPinboard').submit();
	}
	publishButton() {
		this.cancelPromoCode()
		this.isPublishedClicked = true;
		if ((this.pinboard.type == 1 && this.pinobardPreviousStatus == 2) || this.pinboard.type == null) {
			this.pinboard.type = null
			document.getElementById("formSubmit").click();
		} else if (this.pinboard.type == 1 && this.pinobardPreviousStatus == 1) {
			this.pinboard.pinboardStatus = 2;
			document.getElementById("formSubmit").click();
		} else if (this.pinboard.type == 2) {
			this.pinboard.pinboardStatus = 1;
			document.getElementById("formSubmit").click();
			/* if (this.pinboard.subscriptionPlanId == 2){
				this.planAmount = this.pinboard.months * this.MonthlyCharges
				this.credits = this.pinboard.months * 1
				document.getElementById("openPaymentPopup").click();
			} else if (this.pinboard.subscriptionPlanId == 1){
				this.planAmount = this.yearlyCharges;
				this.credits =  10
				document.getElementById("openPaymentPopup").click();
			}else{
				document.getElementById("formSubmit").click();
			} */

		} else {
			document.getElementById("formSubmit").click();
		}

	}

	pinboardType(e: any) {
		this.latC = "";
		this.lngC = "";
		if (e == 1) {
			let self = this;

			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) {
					self.pinboard.userLatitude = position.coords.latitude;
					self.pinboard.userLongitude = position.coords.longitude;
				}, function (error) {
					self.pinboard.type = null;
					self.toastr.error("Free Pinboards can only be created within 250m of your current location, please allow StreetPin to use your browser/device's location to proceed");
				}, { timeout: 5000 });
			} else {
				self.toastr.error('no geolocation support');
			}

			this.freeType = true;
			this.planList = false;
		} else {
			this.pinboard.subscriptionPlanId = null;
			this.latC = "";
			this.lngC = "";
			this.freeType = false;
			this.planList = true;
		}
		this.reInitLocationPicker();
	}
	//fucntion is called when user click on add button from listing page
	addPinboard() {

		this.pinobardPreviousStatus = 1;
		this.pinobardPreviousType = null;
		this.htmlOptions.categoryImage = 'null';
		this.htmlOptions.logo = 'null';
		this.htmlOptions.backgroundImage = 'null';
		this.htmlOptions.button = false;
		this.htmlOptions.editMode = true;
		this.pinboard = new Pinboard({});
		this.pinboard.subscriptionPlanId = null
		this.pinboard.type = null;
		if (localStorage.getItem('planId') == '1' || localStorage.getItem('planId') == '2') {
			this.pinboard.type = 2;
			this.pinboard.subscriptionPlanId = parseInt(localStorage.getItem('planId'))
			localStorage.setItem('planId', '0');
		}
		if (localStorage.getItem('planId') == '3') {
			this.pinboard.type = 1;
			localStorage.setItem('planId', '0');
		}
		this.pinboard.pinboardStatus = 1;
		this.pinboard.categoryId = null;
		this.pinboard.isAlllowOffers = true;
		this.pinboard.isAllowPosts = true;
		this.pinboard.months = 1;
		this.initLocationPicker();
	}

	reInitLocationPicker() {

		if (navigator.geolocation) {

			navigator.geolocation.getCurrentPosition((position) => {

				$('#us3').locationpicker("location", { latitude: position.coords.latitude, longitude: position.coords.longitude,radius: this.pinboard.type == 2 ? 1 : 250 });
			});
		}
	}

	initLocationPicker() {
		let self = this;

		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition((position) => {
				self.pinboard.latitude = position.coords.latitude;
				self.pinboard.longitude = position.coords.longitude;
				//this.pinboard.businessInformation.location

				$('#us3').locationpicker({
					location: {
						latitude: position.coords.latitude,
						longitude: position.coords.longitude
					},
					radius: self.pinboard.type == 2 ? 0 : 250,
					inputBinding: {
						latitudeInput: $('#us3-lat'),
						longitudeInput: $('#us3-lon'),
						radiusInput: $('#us3-radius'),
						locationNameInput: $('#us3-address')
					},
					markerIcon: 'assets/images/logo-pin-map.png',
					enableAutocomplete: true,
					zoom: 16,
					oninitialized: function (component){
						var addressComponents = $(component).locationpicker('map').location.addressComponents;
						self.updateLocation(addressComponents);
											} ,
					onchanged: function (currentLocation, radius, isMarkerDropped) {
						var addressComponents = $(this).locationpicker('map').location.addressComponents;
						self.updateLocation(addressComponents);
						if (self.htmlOptions.editMode) {
							self.pinboard.isLocationChange = true;
						}
						self.pinboard.latitude = currentLocation.latitude;
						self.pinboard.longitude = currentLocation.longitude;

						if (self.pinboard.type != 2) {
							let distance = getDistanceFromLatLonInKm(position.coords.latitude, position.coords.longitude, currentLocation.latitude, currentLocation.longitude);

							if (distance * 1000 > 250) {
								/* $('#us3-lat').val(position.coords.latitude);
								$('#us3-lon').val(position.coords.longitude); */
								alert("Free Pinboards can only be created within 250m of your current location.");
								//$('#doIT').trigger('click');
								self.reInitLocationPicker();
							}
						}
					}
				});

			});
		}


	}
	updateLocation(addressComponents: any){
		this.pinboard.businessInformation.location = addressComponents.addressLine1 + " " + addressComponents.addressLine2 + " " + addressComponents.city + " " + addressComponents.stateOrProvince + " " + addressComponents.country + " " + addressComponents.postalCode;
	}
	initLocationPickerEdit() {
		let self = this;

		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition((position) => {
				$('#us3').locationpicker({
					location: {
						latitude: self.pinboard.latitude,
						longitude: self.pinboard.longitude
					},
					radius: self.pinboard.type == 2 ? 0 : 250,
					inputBinding: {
						latitudeInput: $('#us3-lat'),
						longitudeInput: $('#us3-lon'),
						radiusInput: $('#us3-radius'),
						locationNameInput: $('#us3-address')
					},
					markerIcon: 'assets/images/logo-pin-map.png',
					enableAutocomplete: true,
					zoom: 16,
					onchanged: function (currentLocation, radius, isMarkerDropped) {
						var addressComponents = $(this).locationpicker('map').location.addressComponents;
						self.updateLocation(addressComponents);
						if (self.htmlOptions.editMode) {
							self.pinboard.isLocationChange = true;
						}
						self.pinboard.latitude = currentLocation.latitude;
						self.pinboard.longitude = currentLocation.longitude;
						console.log(self.pinboard.latitude)
						console.log(self.pinboard.longitude)
						if (self.pinboard.type != 2) {
							let distance = getDistanceFromLatLonInKm(position.coords.latitude, position.coords.longitude, currentLocation.latitude, currentLocation.longitude);

							if (distance * 1000 > 250) {
								/* $('#us3-lat').val(position.coords.latitude);
								$('#us3-lon').val(position.coords.longitude); */
								alert("Free Pinboards can only be created within 250m of your current location.");
								//$('#doIT').trigger('click');
								self.reInitLocationPicker();
							}
						}
					}
				});

			});
		}


	}

	//fucntion is called when user click on add button from listing page
	editPinboard(id) {

		this.htmlOptions.newupload = true;
		this.spinner.show();
		this.htmlOptions.button = true;
		this.htmlOptions.editMode = true;
		this.WebService.pinboardAction(id, 'getById').subscribe(resultpinboardData => {
			this.spinner.hide();
			if (resultpinboardData.statusCode == 200) {
				this.pinboard = new Pinboard(resultpinboardData.responsePacket)
				if (this.pinboard.subscriptionPlanId == 1 || this.pinboard.subscriptionPlanId == 2) {

				} else {

					this.pinboard.subscriptionPlanId = null
				}
				console.log(this.pinboard);
				this.pinboard.type = parseInt(resultpinboardData.responsePacket.type)
				this.openingTime = JSON.parse(resultpinboardData.responsePacket.businessInformation.openingHours);
				this.pinboard.user = '';
				this.pinboard.user = JSON.parse(resultpinboardData.responsePacket.user);
				this.pinboard.userNew = JSON.parse(resultpinboardData.responsePacket.user);
				if (resultpinboardData.responsePacket.competingCategoriesListObj) {
					this.pinboard.competingCategoriesList = JSON.parse(resultpinboardData.responsePacket.competingCategoriesListObj);
				} else {
					this.pinboard.competingCategoriesList = [];
				}
				this.pinobardPreviousStatus = this.pinboard.pinboardStatus;
				this.pinobardPreviousType = this.pinboard.type;
				if (resultpinboardData.responsePacket.isPaid) {
					this.paidPinboard = true;
				} else {
					this.paidPinboard = false;
				}
				this.pinboard.latitude = resultpinboardData.responsePacket.latitude;
				this.pinboard.longitude = resultpinboardData.responsePacket.longitude;
				this.htmlOptions.categoryImage = resultpinboardData.responsePacket.categoryImage
				this.htmlOptions.logo = resultpinboardData.responsePacket.logo
				this.htmlOptions.backgroundImage = resultpinboardData.responsePacket.backgroundImage;
				this.oldExpiryDate = this.pinboard.expiryDate;
				var newVarDate = this.pinboard.expiryDate.toString();
				var newDateArr = newVarDate.split("-");
				var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
				this.pinboard.expiryDate = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]));

				let self = this;
				/* if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function (position) {
						self.pinboard.userLatitude = position.coords.latitude;
						self.pinboard.userLongitude = position.coords.longitude;
					}, function (error) {
					}, { timeout: 5000 });
				} */
				this.initLocationPickerEdit();
			} else {
				localStorage.setItem('statusCode', resultpinboardData.message);
			}
		},
			error => {

			})

	}
	//fucntion is called when user click on add button from listing page
	renewPinboard(id) {
		this.htmlOptions.newupload = true;
		this.spinner.show();
		this.htmlOptions.button = true;
		this.htmlOptions.editMode = true;
		this.WebService.pinboardAction(id, 'getById').subscribe(resultpinboardData => {
			this.spinner.hide();

			if (resultpinboardData.statusCode == 200) {
				this.pinboard = new Pinboard(resultpinboardData.responsePacket)
				if (this.pinboard.pinboardStatus == 3) {
					this.pinboard.isPaid = false;
				}
				this.pinboard.type = parseInt(resultpinboardData.responsePacket.type)
				this.openingTime = JSON.parse(resultpinboardData.responsePacket.businessInformation.openingHours);
				this.pinboard.user = '';
				this.pinboard.user = JSON.parse(resultpinboardData.responsePacket.user);

				if (resultpinboardData.responsePacket.competingCategoriesListObj) {
					this.pinboard.competingCategoriesList = JSON.parse(resultpinboardData.responsePacket.competingCategoriesListObj);
				} else {
					this.pinboard.competingCategoriesList = [];
				}

				/* this.pinboard.competingCategoriesList = JSON.parse(resultpinboardData.responsePacket.competingCategoriesListObj); */

				if (resultpinboardData.responsePacket.isPaid) {
					this.paidPinboard = true;
				} else {
					this.paidPinboard = false;
				}
				this.paidPinboard = false;
				this.pinboard.pinboardStatus = 3
				this.pinboard.type = null
				this.htmlOptions.categoryImage = resultpinboardData.responsePacket.categoryImage
				this.htmlOptions.logo = resultpinboardData.responsePacket.logo
				this.htmlOptions.backgroundImage = resultpinboardData.responsePacket.backgroundImage
				var newVarDate = this.pinboard.expiryDate.toString();
				var newDateArr = newVarDate.split("-");
				var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
				//this.pinboard.expiryDate = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]));

			} else {
				localStorage.setItem('statusCode', resultpinboardData.message);
			}
		},
			error => {

			})

	}
	//funcntion is called when user click on save/update button from form
	onSubmit() {
		/* onSubmit(f: any) { */
		/* if (!f.valid) {
			document.getElementById("error_box").scrollIntoView();

			this.isValid = true;
			var that = this;

			setTimeout(() => {
				that.isValid = false;
			}, 5000);
			return;
		} */
		console.log(this.pinboard);
		this.pinboard.userId = this.userId
		this.pinboard.user = [];
		this.pinboard.user = this.newPinUser
		if (!this.pinboard.userNew){
			this.pinboard.userNew = this.newPinUser
		}
		//this.pinboard.userNew = this.newPinUser
		if (!this.pinboard.type) {
			return;
		}
		this.spinner.show();
		var self = this;
		this.multipleProductImageUploadl(
			function (error) {
				self.spinner.hide();
				if (this.htmlOptions.editMode) {
					this.submitWithoutImage()
				} else {
					alert(error)
				}
			},
			function (success) {
				self.spinner.hide();
				if (success == '') {
					self.categoryImage = self.pinboard.categoryImage
					self.logo = self.pinboard.logo
					self.backgroundImage = self.pinboard.backgroundImage
				} else {

					for (let i = 0; i < success.length; i++) {

						if (success[i].ImageType == "logo") {
							self.pinboard.logo = success[i].ImagePath
						}
						if (success[i].ImageType == "categoryImage") {
							self.pinboard.categoryImage = success[i].ImagePath
						}

						if (success[i].ImageType == "backgroundImage") {
							self.pinboard.backgroundImage = success[i].ImagePath
						}
					}

				}

				var newExpDate = new Date();
				var myJSON = JSON.stringify(self.openingTime);
				self.pinboard.businessInformation.openingHours = myJSON;
				self.pinboardCatArray = []
				self.pinboardCatArrayObj = []
				self.pinboardCatArrayOld = self.pinboard.competingCategoriesList
				if (self.pinboard.competingCategoriesList) {
					for (let i = 0; i < self.pinboard.competingCategoriesList.length; i++) {
						self.pinboardCatArray.push(self.pinboard.competingCategoriesList[i]['categoryName']);
					}
				}

				if (self.pinboard.competingCategoriesList) {
					for (let i = 0; i < self.pinboard.competingCategoriesList.length; i++) {

						self.pinboardCatArrayObj.push({ "categoryId": self.pinboard.competingCategoriesList[i]['categoryId'], "categoryName": self.pinboard.competingCategoriesList[i]['categoryName'] });
					}
				}

				if (self.pinboardCatArray) {

					self.pinboard.competingCategoriesList = self.pinboardCatArray;
				} else {
					self.pinboard.competingCategoriesList = [];

				}
				/* if (self.pinboard.userNew && self.pinboard.userNew.length > 0) {
				}  */
				var user = JSON.stringify(self.pinboard.user);
					self.pinboard.user = '';
					self.pinboard.user = user;
				if (self.pinboardCatArrayObj) {
					self.pinboard.competingCategoriesListObj = JSON.stringify(self.pinboardCatArrayObj);
				} else {
					self.pinboard.competingCategoriesListObj = [];
				}

				if (self.oldExpiryDate != '') {
					self.pinboard.expiryDate = self.oldExpiryDate;
					newExpDate = self.oldExpiryDate;
				}

				self.spinner.show();
				self.WebService.addPinboardData(self.pinboard, newExpDate).subscribe(result => {
					self.spinner.hide();
					document.getElementById('openSpine').click();
					if (result['statusCode'] == 201) {
						alert(result['message']);
					} else if (result['statusCode'] == 200) {
						var temp = self.htmlOptions.button;
						if (self.isPublishedClicked) {
							/* self.pinboard.id = result['responsePacket']['id'] */
							self.pinboard = new Pinboard(result['responsePacket'])
							if (self.pinboard.subscriptionPlanId == 2) {
								document.getElementById('openSpine').click();
								self.isPublishedClicked = false;
								self.planAmount = self.pinboard.months * self.MonthlyCharges
								self.credits = self.pinboard.months * 1
								document.getElementById("openPaymentPopup").click();
								return
							} else if (self.pinboard.subscriptionPlanId == 1) {
								document.getElementById('openSpine').click();
								self.isPublishedClicked = false;
								self.planAmount = self.yearlyCharges;
								self.credits = 10
								document.getElementById("openPaymentPopup").click();
								return
							} else {
								window.location.reload();
								alert('Record saved successfully');
							}
						} else {
							window.location.reload();
							alert('Record saved successfully');
							/* setTimeout(function () {
								if (temp) {
									self.toastr.success('Record updated successfully', 'Success');
								} else {
									self.toastr.success('Record saved successfully', 'Success');
								}
								self.ngOnInit();
							}, 500); */
						}

					} else {

						if (self.pinboard.competingCategoriesListObj) {
							self.pinboard.competingCategoriesList = JSON.parse(self.pinboard.competingCategoriesListObj);
						} else {
							self.pinboard.competingCategoriesList = [];
						}
						/* alert(result['message']); */
						/* self.toastr.error(result['message']); */
					}
				},
					error => {
						self.spinner.hide();
					})



			})
		console.log('helloe');
	}
	spinerClick() {
		this.spinner.hide();
	}
	buy() {
		let self = this;
		this.subscription.planId = this.pinboard.subscriptionPlanId;
		this.subscription.subscriptionPlanType = 1;
		this.subscription.discountAmout = this.couponDiscountValue;

		this.subscription.planAmount = this.planAmount;
		this.subscription.credits = this.credits;
		this.subscription.amountPaid = this.planAmount;;

		let user = {
			"userId": this.userId
		}
		this.spinner.show();
		this.subscription.user = user;
		this.stripeService
			.createToken(this.card.getCard(), { name })
			.subscribe(result => {
				if (result.token) {
					this.subscription.token = result.token.id;
					var element = <HTMLInputElement>document.getElementById("buttonDisabled");
					element.disabled = true;
					self.WebService.subscribePlan(self.subscription, 'subscribePlan').subscribe(resultSubscriptionData => {
						var element = <HTMLInputElement>document.getElementById("buttonDisabled");

						if (resultSubscriptionData['statusCode'] == 200) {
							self.savePinboardAfterPayment(resultSubscriptionData['responsePacket']['userSubscriptionId'])
							element.disabled = false;
						} else {
							self.spinner.hide();
							self.toastr.error(resultSubscriptionData['message']);
							element.disabled = false;
						}
					},
						error => {

						})
				} else if (result.error) {
					// Error creating the token
					this.spinner.hide();
					this.toastr.error(result.error.message);
				}
			});
	}
	savePinboardAfterPayment(userSubscriptionId) {
		document.getElementById("closePaymentPopup").click();
		this.spinner.show();
		this.pinboard.userId = this.userId
		this.pinboard.user = this.pinUser
		var user = JSON.stringify(this.pinboard.user);
		this.pinboard.user = '';
		this.pinboard.user = user;
		this.pinboard.isPaid = true;
		this.pinboard.adminUserId = 0;
		this.pinboard.pinboardStatus = 2;
		this.pinboard.autoRecurring = true;
		this.pinboard.paymentId = userSubscriptionId;

		let self = this;
		/* this.multipleProductImageUploadl( */
		/* function (error) {
			if (this.htmlOptions.editMode) {
				this.submitWithoutImage()
			} else {
				alert(error)
			}
		},
		function (success) {
			if (success == '') {
				self.categoryImage = self.pinboard.categoryImage
				self.logo = self.pinboard.logo
				self.backgroundImage = self.pinboard.backgroundImage

			} else {

				for (let i = 0; i < success.length; i++) {

					if (success[i].ImageType == "logo") {
						self.pinboard.logo = success[i].ImagePath
					}
					if (success[i].ImageType == "categoryImage") {
						self.pinboard.categoryImage = success[i].ImagePath
					}

					if (success[i].ImageType == "backgroundImage") {
						self.pinboard.backgroundImage = success[i].ImagePath
					}
				}

			} */
		var myJSON = JSON.stringify(self.openingTime);
		self.pinboard.businessInformation.openingHours = myJSON;
		var newExpDate = new Date();
		if (self.oldExpiryDate != '') {
			self.pinboard.expiryDate = self.oldExpiryDate;
			newExpDate = self.oldExpiryDate;
		}

		self.WebService.addPinboardData(self.pinboard, newExpDate).subscribe(result => {
			document.getElementById('openSpine').click();
			if (result['statusCode'] == 200) {
				let self = this
				setTimeout(function () {
					window.location.reload();
					/* self.ngOnInit(); */
					self.toastr.success('We have received payment and Record added successfully', 'Success');
				}, 500);

			} else {
				if (self.pinboard.competingCategoriesListObj) {
					self.pinboard.competingCategoriesList = JSON.parse(self.pinboard.competingCategoriesListObj);
				} else {
					self.pinboard.competingCategoriesList = [];
				}
				self.toastr.error(result['message']);
			}
		},
			error => {

			})

		/* }) */
	}
	//funcntion is called when user upload image from form
	fileEvent(fileInput: any, fileType) {
		console.log(fileInput);
		this.htmlOptions.newupload = false;
		var files = fileInput.target.files;
		var file = files[0];

		if (file.size > 5242880) {
			this.pinboard.backgroundImage = "";
			this.pinboard.logo = "";
			this.pinboard.categoryImage = "";
			alert("Image size should be less than 5MB");
			this.globalFilesArray = [];
			return false;
		} else if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {
			var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

			if (pos != -1) {
				if (file == undefined) {
					this.globalFilesArray.splice(pos, 1);
				} else {
					this.globalFilesArray[pos].fileType = fileType
					this.globalFilesArray[pos].file = file
				}
			} else {
				this.globalFilesArray.push({ fileType: fileType, file: file })
			}
			if (fileType == 'logo') {
				this.htmlOptions.logo = ""
			}
			if (fileType == 'categoryImage') {
				this.htmlOptions.categoryImage = ""
			}
			if (fileType == 'backgroundImage') {
				this.htmlOptions.backgroundImage = ""
			}

		} else {
			if (fileType == 'logo') {
				this.pinboard.logo = ""
			}
			if (fileType == 'categoryImage') {
				this.pinboard.categoryImage = ""
			}
			if (fileType == 'backgroundImage') {
				this.pinboard.backgroundImage = ""
			}
			/* this.pinboard.backgroundImage = "";
			this.pinboard.logo = "";
			this.pinboard.categoryImage = ""; */
			alert("Only jpg, png and jpeg file format allowed");
			this.globalFilesArray = [];
			return false;
		}

	}

	//funcntion is user for display opening hrs
	/* populate() {
		var hours, ampm;
		var hours1, ampm1;
		this.fromTimeArray.push({ hours: + "12 " + " AM" })
		for (var i = 1; i <= 11; i++) {

			this.fromTimeArray.push({ hours: i + ":00 " + "AM" })
			this.fromTimeArray.push({ hours: i + ":30 " + "AM" })
		}

		for (var j = 1; j <= 11; j++) {
			this.toTimeArray.push({ hours: j + ":00 " + 'PM' })
			this.toTimeArray.push({ hours: j + ":30 " + 'PM' })
		}
		this.toTimeArray.push({ hours: + "12 " + " PM" })
	} */

	populate() {
		var hours, ampm;
		var hours1, ampm1;
		/* this.fromTimeArray.push({ hours: + "12" }) */
		for (var i = 0; i <= 23; i++) {

			this.fromTimeArray.push({ hours: i + ":00" })
			this.fromTimeArray.push({ hours: i + ":30" })
		}

		for (var j = 0; j <= 23; j++) {
			this.toTimeArray.push({ hours: j + ":00" })
			this.toTimeArray.push({ hours: j + ":30" })
		}
		/* this.toTimeArray.push({ hours: + "12" }) */
	}

	//funcntion is called when user click on action buttons like active, delete
	pinboardAction(id, action) {
		let self = this;
		if (action == "delete") {
			var r = confirm("Are you sure? You will not be able to recover this in future!");
			if (r == true) {
				this.spinner.show();
				this.WebService.pinboardAction(id, action).subscribe(resultPinBoardActionData => {
					this.spinner.hide();
					if (resultPinBoardActionData.statusCode == 200) {
						setTimeout(function () {
							if (action == "active") {
								self.toastr.success('Record has been activated successfully', 'Activated');
							} else if (action == "inactive") {
								self.toastr.success('Record has been deactivated successfully', 'Deactivated');

							} else if (action == "delete") {
								self.toastr.success('Record has been deleted successfully', 'Deleted');
							}
						}, 1000);
						this.pinboardData = resultPinBoardActionData.responsePacket;
						this.ngOnInit();
					} else {
						this.toastr.error(resultPinBoardActionData.message);
					}
				},
					error => {

					})
			}
		} else {
			this.spinner.show();
			this.WebService.pinboardAction(id, action).subscribe(resultPinBoardActionData => {
				this.spinner.hide();

				if (resultPinBoardActionData.statusCode == 200) {
					setTimeout(function () {
						if (action == "active") {
							self.toastr.success('Record has been activated successfully', 'Activated');
						} else if (action == "inactive") {
							self.toastr.success('Record has been deactivated successfully', 'Deactivated');

						} else if (action == "delete") {
							self.toastr.success('Record has been deleted successfully', 'Deleted');
						}
					}, 1000);
					this.pinboardData = resultPinBoardActionData.responsePacket
					this.ngOnInit();
				} else {
					this.toastr.error(resultPinBoardActionData.message);
				}
			},
				error => {

				})
		}

	}

	fromChange(event: any) {
		console.log(event);

	}
	//funcntion is called user select
	handleAddressChange(event: any) {
		if (this.htmlOptions.editMode) {
			this.pinboard.isLocationChange = true;
		}
		this.pinboard.businessInformation.location = event.formatted_address;
		this.WebService.getlatlng(event.formatted_address).subscribe(resultpinboardData => {
			if (resultpinboardData.status == "OK") {
				this.pinboard.latitude = resultpinboardData.results[0].geometry.location.lat;
				this.pinboard.longitude = resultpinboardData.results[0].geometry.location.lng;
				this.spinner.hide();
				this.pinboardData = resultpinboardData['responsePacket']
			} else {
				this.spinner.hide();
				this.pinboard.latitude = '';
				this.pinboard.longitude = '';
				localStorage.setItem('statusCode', resultpinboardData['message']);
			}
		},
			error => {

			})
	}

	//category listing to add and edit form
	getCategories() {
		this.WebService.getCategories('getCategory').subscribe(resultcategoryData => {
			if (resultcategoryData['statusCode'] == 200) {
				this.categorydata = resultcategoryData['responsePacket'];
				this.dropdownList = resultcategoryData['responsePacket'];
			}
		},
			error => {

			})
	}

	//category listing
	getOfferCategory() {
		this.WebService.getOfferCategory('getOfferCategory').subscribe(resultcategoryData => {
			if (resultcategoryData['statusCode'] == 200) {

				//this.dropdownList = resultcategoryData['responsePacket'];
			}
		},
			error => {

			})
	}
	//funcntion used to list all user's pinboards.
	myPinboardList() {
		this.WebService.getMyPinboardList(this.userId, 'getMyPinboardList').subscribe(resultMyPinboardData => {
			this.spinner.hide();
			if (resultMyPinboardData['statusCode'] == 200) {
				this.pinboardArr = resultMyPinboardData['responsePacket'];
				console.log(this.pinboardArr);
			}
		},
			error => {

			})
	}
	changeModule(module: string) {
		this.ngOnInit();
	}
	dateFormate(expiryDate) {
		var newDateArr = expiryDate.split("-");
		var lastChar = newDateArr[2].substring(newDateArr[2].length - 2);
		var newExpDate = newDateArr[0] + "-" + newDateArr[1] + "-" + lastChar;

		return newExpDate;
	}
	//funcntion is called when user click clone icon, it will copy the record that selected
	clonePinboard(id) {
		this.WebService.pinboardAction(id, 'clonePinboard').subscribe(resultofferData => {
			if (resultofferData.statusCode == 200) {
				this.ngOnInit();
			}
		},
			error => {

			})
	}
	//funcntion is called when user click on view pinboard button
	viewPinboard(pinboardId) {
		if (pinboardId) {
			localStorage.setItem('pinboardId', pinboardId);
		} else {
			localStorage.setItem('pinboardId', '');
		}
		this.router.navigate(['/pinboard']);
	}

	buyPlan(planId, planAmount, credits) {

		if (localStorage.getItem('isLoggedIn') != "true") {
			this.router.navigate(['/login']);
			return false;
		}

		this.subscription.planId = planId;
		this.planAmount = planAmount;
		this.subscription.credits = credits;
		this.subscription.subscriptionPlanType = 2;
		this.subscription.discountAmout = 0;
		this.subscription.amountPaid = planAmount;

		let user = {
			"userId": this.userId
		}
		this.subscription.user = user;
		document.getElementById("openPaymentPopup").click();

	}
	haveApplyCode() {
		this.htmlOptions['haveApplyCode'] = false;
		this.htmlOptions['applyCode'] = true;
		this.htmlOptions['appliedCode'] = false;

	}
	cancelPromoCode() {
		this.promoCode.promoCodeText = "";
		this.htmlOptions['haveApplyCode'] = true;
		this.htmlOptions['applyCode'] = false;
		this.htmlOptions['appliedCode'] = false;

	}
	applyPromoCode() {
		if (!this.promoCode.promoCodeText) {
			this.toastr.error('Please enter valid promo code');
			return;
		}
		let user = {
			"userId": this.userId
		}
		let promoCode = {
			user: user,
			couponCode: this.promoCode.promoCodeText
		}
		this.oldPrice = this.planAmount;
		var elementButton = <HTMLInputElement>document.getElementById("applyButtonDisabled");
		elementButton.classList.add("disabled");
		this.WebService.checkCouponValidity(promoCode, 'checkCouponValidity').subscribe(resultPromoData => {
			if (resultPromoData['statusCode'] == 200) {
				elementButton.classList.remove("disabled");
				this.couponData = resultPromoData['responsePacket'];
				if (this.couponData.couponType == 2) {
					let discountPerAmout = (this.couponData.couponValue / 100) * this.planAmount
					this.planAmount = this.planAmount - discountPerAmout;
					this.couponDiscountValue = discountPerAmout;
				} else {
					if (this.couponData.couponValue <= this.planAmount) {
						this.planAmount = this.planAmount - this.couponData.couponValue;
						this.couponDiscountValue = this.couponData.couponValue;
					} else {
						this.couponDiscountValue = this.planAmount;
						this.planAmount = 0;
					}
				}

				this.htmlOptions['haveApplyCode'] = false;
				this.htmlOptions['applyCode'] = false;
				this.htmlOptions['appliedCode'] = true;
				this.htmlOptions.appliedCodeText = this.couponData.couponCode;
			} else {
				this.toastr.error(resultPromoData['message']);
			}
		},
			error => {

			})
	}
	removePromoCode() {
		this.planAmount = this.oldPrice;
		/* this.htmlOptions = {
			haveApplyCode: true,
			applyCode: false,
			appliedCode: false
		} */
		this.htmlOptions['haveApplyCode'] = true;
		this.htmlOptions['applyCode'] = false;
		this.htmlOptions['appliedCode'] = false;
	}
	pinboardPlan(e: any) {
		this.planId = e;
		if (e == 1) {
			this.planAmount = this.yearlyCharges;
			this.oldPlanPrice = this.yearlyCharges
			this.credits = 10
			this.oldPlanCredit = 10
			this.monthDrowdown = false
			this.subscription.months = 1;
			this.pinboard.months = 1
		} else if (e == 2) {
			this.planAmount = this.MonthlyCharges
			this.oldPlanPrice = this.MonthlyCharges
			this.credits = 1
			this.oldPlanCredit = 1
			this.monthDrowdown = true
			this.subscription.months = 1;
			this.pinboard.months = 1
		} else {
			this.planAmount = this.MonthlyCharges
			this.oldPlanPrice = this.MonthlyCharges
			this.credits = 1
			this.oldPlanCredit = 1
			this.monthDrowdown = false
			this.subscription.months = 1;
			this.pinboard.months = 1
		}
	}
	pinboardPlanMonth(e2: any) {
		this.subscription.months = e2;
		this.planAmount = this.subscription.months * this.oldPlanPrice
		this.credits = this.subscription.months * this.oldPlanCredit
	}
	onSelectAll(items: any) {
		console.log(items);
	}
	onItemSelect(item: any) {

	}
	pinboardPlanList() {
		this.WebService.pinboardplan('get').subscribe(resultSubscriptionData => {
			this.spinner.hide();
			if (resultSubscriptionData['statusCode'] == 200) {
				console.log(resultSubscriptionData['responsePacket']);
				this.subscriptionArr = resultSubscriptionData['responsePacket'];
				this.yearlyCharges = this.subscriptionArr[0]['planAmount']
				this.MonthlyCharges = this.subscriptionArr[1]['planAmount']
			} else {
				/* this.toastr.error(resultSubscriptionData['message']); */
			}
		},
			error => {

			})
	}

	fileChangeEvent(event: any, fileType): void {

		this.isUploaded = true;
		if (fileType == 'logo') {
			this.cropImageObj.isUploadedL = true;
			this.htmlOptions.logo = ""
			this.isLogo = true
			this.imageChangedEventLogo = event;
		}
		if (fileType == 'backgroundImage') {
			this.cropImageObj.isUploadedB = true;
			this.htmlOptions.backgroundImage = ""
			this.isBackground = true
			this.imageChangedEventBackground = event;
		}
		this.imageCropper.autoCrop = true;
		let element: any = document.getElementsByClassName('source-image');
		let fileReader = new FileReader();
		fileReader.onload = (e) => {

			if (fileType == 'logo') {

				this.cropImageObj.tampImageWithoutCropL = fileReader.result.toString();
			}
			if (fileType == 'backgroundImage') {

				this.cropImageObj.tampImageWithoutCropB = fileReader.result.toString();
			}
			//this.tampImageWithoutCrop = fileReader.result.toString();
		}
		fileReader.readAsDataURL(event.target.files[0]);
		this.detectFiles(event, fileType)

	}
	imageCropped(event: ImageCroppedEvent, fileType) {

		if (fileType == 'logo') {

			this.cropImageObj.croppedImageL = event.base64;
			this.cropImageObj.tampImageWithCropL = event.base64;
			const base64Data = new Buffer(event.base64.replace(/^data:image\/\w+;base64,/, ""), 'base64')
			var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)
			const type = event.base64.split(';')[0].split('/')[1]
			if (pos != -1) {
				if (event.base64 == undefined) {
					this.globalFilesArray.splice(pos, 1);
				} else {
					this.globalFilesArray[pos].fileType = fileType
					this.globalFilesArray[pos].file = base64Data
				}
			} else {
				this.globalFilesArray.push({ fileType: fileType, file: event.base64, type: type })
			}
		}
		if (fileType == 'backgroundImage') {

			this.cropImageObj.croppedImageB = event.base64;
			this.cropImageObj.tampImageWithCropB = event.base64;
			const base64Data = new Buffer(event.base64.replace(/^data:image\/\w+;base64,/, ""), 'base64')
			const type = event.base64.split(';')[0].split('/')[1]
			var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

			if (pos != -1) {
				if (event.base64 == undefined) {
					this.globalFilesArray.splice(pos, 1);
				} else {
					this.globalFilesArray[pos].fileType = fileType
					this.globalFilesArray[pos].file = base64Data
				}
			} else {
				this.globalFilesArray.push({ fileType: fileType, file: event.base64, type: type })
			}
		}
		console.log(this.globalFilesArray)
	}


	cropImageLogo() {
		this.cropImageObj.IsLogoCropedImage = true
		this.cropImageObj.IsCropedImageL = true;
		this.cropImageObj.showCropperL = true;
		this.imageCropper.autoCrop = true;
		this.cropImageObj.showL = true;
	}
	cropImageBackground() {
		this.cropImageObj.IsBackgroundCropedImage = true
		this.cropImageObj.IsCropedImageB = true;
		this.cropImageObj.showCropperB = true;
		this.imageCropper.autoCrop = true;
		this.cropImageObj.showB = true;
	}

	CancelL() {
		this.cropImageObj.IsLogoCropedImage = false;
		this.cropImageObj.IsCropedImageL = false;
		this.cropImageObj.showL = false;
		this.imageCropper.resetCropperPosition();
		this.imageCropper.autoCrop = true;
		this.cropImageObj.showCropperL = true;
	}
	CancelB() {
		this.cropImageObj.IsBackgroundCropedImage = false;
		this.cropImageObj.IsCropedImageB = false;
		this.cropImageObj.showB = false;
		this.imageCropper.resetCropperPosition();
		this.imageCropper.autoCrop = true;
		this.cropImageObj.showCropperB = true;
	}


	base64MimeType(encoded) {
		var result = null;

		if (typeof encoded !== 'string') {
			return result;
		}

		var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

		if (mime && mime.length) {
			result = mime[1];
		}

		return result;
	}

	compressAndUploadFile(f: any) {
		if (!f.valid) {
			document.getElementById("error_box").scrollIntoView();

			this.isValid = true;
			var that = this;

			setTimeout(() => {
				that.isValid = false;
			}, 5000);
			return;
		}
		this.spinner.show();
		//if (this.isUploaded) {
		if (false) {
			this.globalFilesArray = [];
			var imageL: any
			if (this.isLogo) {
				if (this.cropImageObj.IsCropedImageL == true) {
					imageL = this.cropImageObj.tampImageWithCropL;
				} else {
					imageL = this.cropImageObj.tampImageWithoutCropL;
				}
			}
			var imageB: any
			if (this.isBackground) {
				if (this.cropImageObj.IsCropedImageB == true) {
					imageB = this.cropImageObj.tampImageWithCropB;
				} else {
					imageB = this.cropImageObj.tampImageWithoutCropB;
				}
			}
			//  this.imageCompress.uploadFile().then(({image, orientation}) => {
			var ImageTypeL = this.base64MimeType(imageL);
			var ImageTypeB = this.base64MimeType(imageB);
			//L
			if (this.imageCompress.byteCount(imageL) > 150 && this.IsCropedImage == false && this.isLogo) {
				var quality;
				var size;
				if (ImageTypeL == "image/png") {
					quality = 30;
					/* size = 10; */

				} else {
					/* size = 50; */
					quality = 30;
				}
				this.imageCompress.compressFile(imageL, this.cropImageObj.orientationLogoImage, size, quality).then(
					result => {
						this.cropImageObj.croppedImageL = result;

						const base64Data = new Buffer(result.replace(/^data:image\/\w+;base64,/, ""), 'base64')
						const type = result.split(';')[0].split('/')[1]
						this.globalFilesArray.push({ fileType: 'logo', file: base64Data })
						//this.UploadToS3L(result)
						console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
					}
				);
			} else {
				this.globalFilesArray.push({ fileType: 'logo', file: imageL })
				//this.UploadToS3L(imageL)
			}
			//B
			if (this.imageCompress.byteCount(imageB) > 150 && this.IsCropedImage == false && this.isBackground) {
				var quality;
				var size;
				if (ImageTypeB == "image/png") {
					quality = 30;
					/* size = 10; */

				} else {
					/* size = 50; */
					quality = 30;
				}
				this.imageCompress.compressFile(imageB, this.cropImageObj.orientationBackgroundImage, size, quality).then(
					result => {
						this.cropImageObj.croppedImageB = result;
						//this.UploadToS3B(result)
						const base64Data = new Buffer(result.replace(/^data:image\/\w+;base64,/, ""), 'base64')
						const type = result.split(';')[0].split('/')[1]
						this.globalFilesArray.push({ fileType: 'background', file: base64Data })
						console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
					}
				);
			} else {
				//this.UploadToS3B(imageB)
				this.globalFilesArray.push({ fileType: 'background', file: imageB })
			}
			//this.multipleProductImageUploadl();
		} else {
			this.onSubmit()
		}
		//});
	}

	UploadToS3L(image: any) {
		console.log('imageL');
		console.log(image);
		const base64Data = new Buffer(image.replace(/^data:image\/\w+;base64,/, ""), 'base64')
		const type = image.split(';')[0].split('/')[1]
		AWS.config.update({
			region: environment.aws_region,
			credentials: new AWS.CognitoIdentityCredentials({
				IdentityPoolId: environment.aws_IdentityPoolId
			})
		})
		const s3 = new AWS.S3({
			apiVersion: environment.aws_apiVersion,
			params: { Bucket: environment.aws_bucketName }
		})
		var params = {
			Bucket: environment.aws_bucketName,
			Key: '_cmsImages/' + new Date().getTime(),
			Body: base64Data,
			ACL: 'public-read',
			ContentEncoding: 'base64', // required
			ContentType: "image/" + type // required. Notice the back ticks
		};
		var self = this
		s3.upload(params, function (err, data) {
			if (err) { return console.log(err) }
			console.log('data', data.Location)

			self.pinboard.logo = data.Location;

			//self.onSubmit()
		})
	}
	UploadToS3B(image: any) {
		console.log('imageB');
		console.log(image);
		const base64Data = new Buffer(image.replace(/^data:image\/\w+;base64,/, ""), 'base64')
		const type = image.split(';')[0].split('/')[1]

		AWS.config.update({
			region: environment.aws_region,
			credentials: new AWS.CognitoIdentityCredentials({
				IdentityPoolId: environment.aws_IdentityPoolId
			})
		})
		const s3 = new AWS.S3({
			apiVersion: environment.aws_apiVersion,
			params: { Bucket: environment.aws_bucketName }
		})
		var params = {
			Bucket: environment.aws_bucketName,
			Key: '_cmsImages/' + new Date().getTime(),
			Body: base64Data,
			ACL: 'public-read',
			ContentEncoding: 'base64', // required
			ContentType: "image/" + type // required. Notice the back ticks
		};
		var self = this
		s3.upload(params, function (err, data) {
			if (err) { return console.log(err) }
			self.pinboard.backgroundImage = data.Location
			//self.onSubmit()
		})
	}
	//image uploald to S3 bucket before form submit
	multipleProductImageUploadl(callbackError, callbackSuccess, ) {

		var _that = this
		AWS.config.update({
			region: environment.aws_region,
			credentials: new AWS.CognitoIdentityCredentials({
				IdentityPoolId: environment.aws_IdentityPoolId
			})
		})

		const s3 = new AWS.S3({
			apiVersion: environment.aws_apiVersion,
			params: { Bucket: environment.aws_bucketName }
		})
		var ImagesUrls = []
		if (this.globalFilesArray.length > 0) {

			this.globalFilesArray.map((item) => {

				var params = {
					Bucket: environment.aws_bucketName,
					Key: '_cmsImages/' + new Date().getTime(),
					Body: item.file,
					ACL: 'public-read',
					ContentEncoding: 'base64', // required
					ContentType: item.type,
				};

				s3.upload(params, function (err, data) {
					let fType = data.key.split("__")[1]
					if (err) { callbackError(err) }
					ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: item.fileType })
					if (ImagesUrls.length == _that.globalFilesArray.length) {
						callbackSuccess(ImagesUrls)
					}
					/* if (fType == 'logo') {
						console.log("logo");
						_that.pinboard.logo = data.Location;
					}
					if (fType == 'background') {
						console.log("background");
						_that.pinboard.backgroundImage = data.Location
					} */
				})
			})
		} else {
			callbackSuccess("")
		}
	}
	detectFiles(event, fileType) {
		var file = event.target.files[0];
		this.getOrientation(file, (orientation) => {
			if (fileType == 'logo') {
				this.cropImageObj.orientationLogoImage = orientation;
			}
			if (fileType == 'backgroundImage') {
				this.cropImageObj.orientationBackgroundImage = orientation;
			}
		});
		if (this.isUploaded) {
			//this.globalFilesArray = [];
			var imageL: any
			if (this.isLogo) {
				if (this.cropImageObj.IsCropedImageL == true) {
					imageL = this.cropImageObj.tampImageWithCropL;
				} else {
					imageL = this.cropImageObj.tampImageWithoutCropL;
				}
			}
			var imageB: any
			if (this.isBackground) {
				if (this.cropImageObj.IsCropedImageB == true) {
					imageB = this.cropImageObj.tampImageWithCropB;
				} else {
					imageB = this.cropImageObj.tampImageWithoutCropB;
				}
			}
			//  this.imageCompress.uploadFile().then(({image, orientation}) => {
			var ImageTypeL = this.base64MimeType(imageL);
			var ImageTypeB = this.base64MimeType(imageB);
			//L
			if (this.imageCompress.byteCount(imageL) > 150 && this.IsCropedImage == false && this.isLogo) {
				var quality;
				var size;
				if (ImageTypeL == "image/png") {
					quality = 30;
					/* size = 10; */

				} else {
					/* size = 50; */
					quality = 30;
				}
				this.imageCompress.compressFile(imageL, this.cropImageObj.orientationLogoImage, size, quality).then(
					result => {
						this.cropImageObj.croppedImageL = result;

						const base64Data = new Buffer(result.replace(/^data:image\/\w+;base64,/, ""), 'base64')
						const type = result.split(';')[0].split('/')[1]
						/* this.globalFilesArray.push({ fileType: 'logo', file: base64Data }) */
						var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

						if (pos != -1) {
							if (file == undefined) {
								this.globalFilesArray.splice(pos, 1);
							} else {
								this.globalFilesArray[pos].fileType = fileType
								this.globalFilesArray[pos].file = file
							}
						} else {
							this.globalFilesArray.push({ fileType: fileType, file: file, type: file.type })
						}

					}
				);
			} else {
				var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

				if (pos != -1) {
					if (file == undefined) {
						this.globalFilesArray.splice(pos, 1);
					} else {
						this.globalFilesArray[pos].fileType = fileType
						this.globalFilesArray[pos].file = file
					}
				} else {
					this.globalFilesArray.push({ fileType: fileType, file: file, type: file.type })
				}
				//this.globalFilesArray.push({ fileType: 'logo', file: imageL })
				//this.UploadToS3L(imageL)
			}
			//B
			if (this.imageCompress.byteCount(imageB) > 150 && this.IsCropedImage == false && this.isBackground) {
				var quality;
				var size;
				if (ImageTypeB == "image/png") {
					quality = 30;
					/* size = 10; */

				} else {
					/* size = 50; */
					quality = 30;
				}
				this.imageCompress.compressFile(imageB, this.cropImageObj.orientationBackgroundImage, size, quality).then(
					result => {
						this.cropImageObj.croppedImageB = result;
						//this.UploadToS3B(result)
						const base64Data = new Buffer(result.replace(/^data:image\/\w+;base64,/, ""), 'base64')
						const type = result.split(';')[0].split('/')[1]
						var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

						if (pos != -1) {
							if (file == undefined) {
								this.globalFilesArray.splice(pos, 1);
							} else {
								this.globalFilesArray[pos].fileType = fileType
								this.globalFilesArray[pos].file = file
							}
						} else {
							this.globalFilesArray.push({ fileType: fileType, file: file, type: file.type })
						}
						/* this.globalFilesArray.push({ fileType: 'background', file: base64Data })
						console.warn('Size in bytes is now:', this.imageCompress.byteCount(result)); */
					}
				);
			} else {
				//this.UploadToS3B(imageB)
				/* this.globalFilesArray.push({ fileType: 'background', file: imageB }) */
				var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

				if (pos != -1) {
					if (file == undefined) {
						this.globalFilesArray.splice(pos, 1);
					} else {
						this.globalFilesArray[pos].fileType = fileType
						this.globalFilesArray[pos].file = file
					}
				} else {
					this.globalFilesArray.push({ fileType: fileType, file: file, type: file.type })
				}
			}
			console.log(this.globalFilesArray);
		}
	}

	getOrientation(file, callback) {

		var reader: any,
			target: EventTarget;
		reader = new FileReader();
		reader.onload = (event) => {

			var view = new DataView(event.target.result);

			if (view.getUint16(0, false) != 0xFFD8) return callback(-2);

			var length = view.byteLength,
				offset = 2;

			while (offset < length) {
				var marker = view.getUint16(offset, false);
				offset += 2;

				if (marker == 0xFFE1) {
					if (view.getUint32(offset += 2, false) != 0x45786966) {
						return callback(-1);
					}
					var little = view.getUint16(offset += 6, false) == 0x4949;
					offset += view.getUint32(offset + 4, little);
					var tags = view.getUint16(offset, little);
					offset += 2;

					for (var i = 0; i < tags; i++)
						if (view.getUint16(offset + (i * 12), little) == 0x0112)
							return callback(view.getUint16(offset + (i * 12) + 8, little));
				}
				else if ((marker & 0xFF00) != 0xFF00) break;
				else offset += view.getUint16(offset, false);
			}
			return callback(-1);
		};

		reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
	};

}
