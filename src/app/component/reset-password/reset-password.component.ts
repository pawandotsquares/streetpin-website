import { Component, OnInit } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  public user: any = {};
  htmlOptions: any = {};
  token: any;
  message: any;
  returnUrl: string;
  constructor(
    private Router: Router,
    private WebService: WebService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.token = params['id'];
    });
  }
  onSubmit() {
    if (this.user.password != this.htmlOptions.cpassword){
      return;
    }
    this.spinner.show();
    this.WebService.resetPassword(this.token, this.user.password, 'userForgotPassword').subscribe(result => {
      this.spinner.hide();
      if (result['statusCode'] == 200) {
        this.toastr.success('Password has been changed successfully', 'Success');
        let self = this;
        setTimeout(function () {
          self.router.navigate(['/']);
        }, 1500);

      } else {
        this.toastr.error(result['message']);
      }
    },
      error => {

      })
  }
}
