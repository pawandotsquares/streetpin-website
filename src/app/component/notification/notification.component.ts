import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  userId: any;
  subscription: any = {};
  notificationData: any;

  /* public Profile: Subscription = new Subscription({}); */
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    this.getUserNotification();
    this.updateUserNotificationStatus();
    //this.getProfileContent();
  }
/*   userNotificationData() {
    this.WebService.getSubscriptionPlanList('getSubscriptionPlanList').subscribe(resultNotificationData => {
      this.spinner.hide();

      if (resultNotificationData['statusCode'] == 200) {
        this.notificationData = resultNotificationData['responsePacket'];
      } else {
        this.toastr.error(resultNotificationData['message']);
      }
    },
      error => {

      })
  } */
  getProfileContent() {
    this.spinner.hide();
    this.WebService.profileData(this.userId, 'getUserProgile').subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        if (resultProfileData.isInstantAlerts) {
          this.getUserNotification();
        }
      }
    },
      error => {

      })
  }
  getUserNotification() {
    this.WebService.getUserNotification(this.userId, 'getUserNotification').subscribe(resultNotificationCount => {
      this.spinner.hide();
      if (resultNotificationCount['statusCode'] == 200) {
        this.notificationData = resultNotificationCount['responsePacket']['userNotificationList'];
        console.log(this.notificationData);
      }
    },
      error => {

      })
  }
  updateUserNotificationStatus() {
    this.WebService.getUserNotification(this.userId, 'updateUserNotificationStatus').subscribe(resultNotificationCount => {
      if (resultNotificationCount['statusCode'] == 200) {

      }
    },
      error => {

      })
  }
  dateToUK(postDate: any) {
    var newDate = postDate.split(" ");
    var newDateArr = newDate[0].split("-");
    var newTimeArr = newDate[1].split(":");
    var date1 = new Date(parseInt(newDateArr[0]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[2]), parseInt(newTimeArr[0]) + 1, parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));

    var curr_date = date1.getDate();
    var curr_month = date1.getMonth() + 1; //Months are zero based
    var curr_year = date1.getFullYear();
    var curr_year = date1.getFullYear();
    var curr_hrs = date1.getHours();
    var curr_mnt = date1.getMinutes();
    var curr_sec = date1.getSeconds();
    var newConvertedDate = curr_date + "-" + curr_month + "-" + curr_year + " " + curr_hrs + ":" + curr_mnt + ":" + curr_sec;
    //return date1.toLocaleString('en-GB', { timeZone: 'UTC' });
    return newConvertedDate;
  }

}
