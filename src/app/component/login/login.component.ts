import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/models/profile';
import { WebService } from '../../service/web.service'
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import * as hello from 'hellojs/dist/hello.all.js'
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: any = {};
  public userEmailnUsername: any = {};
  socialLoginData: any = {};
  returnUrl: string;
  webLoggedIn: any;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  token: any;
  htmlOptions: any = {};
  message: any;
  public Profile: Profile = new Profile({});
  constructor(
    private WebService: WebService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authService: AuthService,

  ) { }


  ngOnInit() {
    this.returnUrl = localStorage.getItem('returnUrl') || '/profile'
    this.route.params.subscribe(params => {
      this.token = params['id'];

    });
    if (this.token){
      this.verifyEmail();
    }else{
      if (localStorage.getItem('isLoggedIn') == "true") {
        this.router.navigate([this.returnUrl]);
       //this.router.navigate(['/profile']);
        return false;
      }
    }
    let userName = localStorage.getItem('remUserName');
    let password = localStorage.getItem('remPass');
    let rememberMe = localStorage.getItem('rememberMe');

    if (rememberMe){
      this.user.rememberMe = rememberMe
      this.user.userName = userName
      this.user.password = password
    }
    this.socialSignIn();
  }
   public socialSignIn() {
    hello.init({
      facebook: environment.facebook_client_id, //eg. Facebook ID
      google: environment.google_client_id,
      twitter: environment.twitter_in_client_id
    }, {
        redirect_uri: environment.site_url});
  }
  socialLoginPopup(network2){
    if (localStorage.getItem('network')) {
      this.helloLogout(localStorage.getItem('network'), network2);
    }else{
      this.socialLogin(network2)
    }
  }

  socialLogin(network) {
    this.socialLoginData = {}
    console.log('%%%%%%%%%%%%%%', localStorage.getItem('network'));
    hello(network).logout().then(function () {
      console.log('Signed out');
    }, function (e) {
      console.log('Signed out error: ' + e.error.message);
    });
    this.userEmailnUsername.network = network;
    var self = this;
    try {
      let client_id: any;
      hello(network).login({
        scope: 'email,basic',
        redirect_uri: environment.site_url,
        response_type: 'code',
        force: false
      }).then(function (data) {
        console.log('data1')
        console.log(data)

      }, function (error) {
          console.log('error1')
          console.log(JSON.stringify(error))
          /* this.toastr.error('Error in ' + network + 'login'); */

      });

      hello.on('auth.login', function (auth) {
        console.log(auth.network);

        hello(auth.network).api('me').then(function (data) {

          console.log('data2')
          console.log(data)
          self.socialLoginData['socialId'] = data['id'];
          if (data['firstName']){
            self.socialLoginData['firstName'] = data['firstName'];
          }else{
            self.socialLoginData['firstName'] = data['name'];
          }
          if (data['lastName']){
            self.socialLoginData['lastName'] = data['lastName'];
          }
          self.socialLoginData['email'] = data['email'];

          if (network == 'facebook'){
            self.socialLoginData['socialType'] = 1;
          } else if (network == 'twitter'){
            self.socialLoginData['socialType'] = 2;
          } else if (network == 'google'){
            self.socialLoginData['socialType'] = 3;
          }
          console.log('socialLoginData');
          console.log(self.socialLoginData);
          self.socialLoginSave(network);
        },function (error) {
          console.log('%%%%%%',error);
        })
      });
    }
    catch (err) {
      console.log(err);
      this.toastr.error('Error in ' + network + 'login');
    }
  }

  socialLoginSave(network) {
    var self = this;
    self.WebService.userSignUp(self.socialLoginData, 'socialUserLogin').subscribe(result => {
      self.spinner.hide();
      console.log('result array');
      console.log(result);
      if (result['statusCode'] == 200) {
        this.Profile = new Profile(result['responsePacket'])
        this.WebService.setCurrentUser(this.Profile);
        self.toastr.success('login successfully', 'Success');
        localStorage.setItem('role', "web");
        localStorage.setItem('isLoggedIn', "true");
        localStorage.setItem('webLoggedIn', "true");
        localStorage.setItem('webEmail', result['responsePacket']['email']);
        localStorage.setItem('userId', result['responsePacket']['userId']);
        localStorage.setItem('userName', result['responsePacket']['userName']);
        localStorage.setItem('network', network);
        self.setLoggedInTime();
        console.log('profile tt');
        document.getElementById("redirectToProfile").click();
        return;
      } else if (result['statusCode'] == 403) {
        console.log('403');
        self.helloLogout2();
        document.getElementById("openExtraSetailsPopup").click();
      } else if (result['statusCode'] == 405) {
        console.log('405 A');
        self.helloLogout2();
        self.toastr.success('A verification link has been sent to your registered email, Please verify your account to login', 'Account created');
      }else{
        console.log('else message');
        self.helloLogout2();
        self.toastr.success('A verification link has been sent to your registered email, Please verify your account to login', 'Account created');
      }
    },
      error => {

      })
  }
  helloLogout(network, network2){
    var self = this;
    hello(network).logout().then(function () {
      localStorage.setItem('network', '');
      console.log('logout');
      self.socialLogin(network2)
    }, function (e) {
        console.log('Not logout' + e.error.message);
    })
  }
  helloLogout2(){
    var self = this;
    if (localStorage.getItem('network')){
      hello(localStorage.getItem('network')).logout().then(function () {
        localStorage.setItem('network', '');
        console.log('logged our 2');
      }, function (e) {
          console.log('Not logout' + e.error.message);
      })
    }
  }
  socialUserLogin() {
    this.socialLoginData['email'] = this.userEmailnUsername.newEmail;
    this.socialLoginData['userName'] = this.userEmailnUsername.newUserName;
    this.WebService.userSignUp(this.socialLoginData, 'socialUserLogin').subscribe(result => {
      this.spinner.hide();
      console.log('Session result');
      console.log(result);
      if (result['statusCode'] == 200) {

        this.Profile = new Profile(result['responsePacket'])
        this.WebService.setCurrentUser(this.Profile);

        this.toastr.success('login successfully', 'Success');
        localStorage.setItem('role', "web");
        localStorage.setItem('isLoggedIn', "true");
        localStorage.setItem('webLoggedIn', "true");
        localStorage.setItem('userId', result['responsePacket']['userId']);
        localStorage.setItem('userName', result['responsePacket']['userName']);
        localStorage.setItem('webEmail', result['responsePacket']['email']);
        localStorage.setItem('network', this.userEmailnUsername.network);
        this.setLoggedInTime();
        console.log('profile1');
        document.getElementById("redirectToProfile").click();
        return;
      } else if (result['statusCode'] == 405) {
        console.log("after email");
        this.helloLogout2();
        this.toastr.success('A verification link has been sent to your registered email, Please verify your account to login', 'Account created');
      } else {
        this.helloLogout2();
        console.log("Second function");
        this.toastr.error(result['message']);
      }
    },
      error => {

      })
  }

 //fucntion called when user click on email verification link
  verifyEmail(){
    this.logout()
    this.spinner.show();
    this.WebService.verifyEmail(this.token, 'userActivation').subscribe(result => {
      this.spinner.hide();
      if (result['statusCode'] == 200) {
        this.toastr.success('Account verified successfully, Please login to continue', 'Success');
      } else {
        this.toastr.error(result['message']);
      }
    },
      error => {

      })

  }
   //fucntion called when form submitted: login form
  onSubmit() {
    this.spinner.show();
    this.WebService.userlogin(this.user.userName, this.user.password, 'userAuth').subscribe(result => {

      this.spinner.hide();
      if (result['statusCode'] == 200) {
        this.Profile = new Profile(result['responsePacket'])
        this.WebService.setCurrentUser(this.Profile);
        console.log(result)
        if (this.user.rememberMe) {
            localStorage.setItem('rememberMe', this.user.rememberMe);
            localStorage.setItem('remUserName', this.user.userName);
            localStorage.setItem('remPass', this.user.password);
          } else {
           localStorage.setItem('rememberMe', '');
          localStorage.setItem('remUserName', '');
           localStorage.setItem('remPass', '');
          }
        localStorage.setItem('WebUserProfileImage', result['responsePacket']['profileImage']);
        localStorage.setItem('firstName', result['responsePacket']['firstName']);
        localStorage.setItem('userName', result['responsePacket']['userName']);
        localStorage.setItem('webEmail', result['responsePacket']['email']);

        localStorage.setItem('role', "web");
        localStorage.setItem('isLoggedIn', "true");
        localStorage.setItem('webLoggedIn', "true");
        localStorage.setItem('WebUserSessionData', result.responsePacket);
        localStorage.setItem('userId', result.responsePacket.userId);
        localStorage.setItem('userName', result.responsePacket.userName);
        this.setLoggedInTime();
        //this.returnUrl = '/profile';
        this.router.navigate([this.returnUrl]);
      } else {
        this.toastr.error(result['message']);
      }
    },
      error => {

      })
  }

  setLoggedInTime(){
    var loginTime = new Date().getTime();
    localStorage.setItem("LoggedInTime", loginTime.toString())
  }

  logout(): void {
    this.authService.logout();
  }
}
