import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { WebService } from 'src/app/service/web.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: any;
  latitude: any;
  longitude: any;
  public navActive:boolean=false;
  constructor(
    private authService: AuthService,
    private router: Router,
    private WebService: WebService
    ) { }

  ngOnInit() {
    this.isLoggedIn = localStorage.getItem('isLoggedIn');

    if (this.isLoggedIn == 'true') {
      this.isLoggedIn = 'true';
    } else {
      this.isLoggedIn = 'false';
    }
  }
  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }


}
