/// <reference types="@types/googlemaps" />
import { Component, OnInit } from '@angular/core';
import OverlappingMarkerSpiderfier from 'overlapping-marker-spiderfier';


@Component({
  selector: 'app-googlemap',
  templateUrl: './googlemap.component.html',
  styleUrls: ['./googlemap.component.css']
})
export class GooglemapComponent implements OnInit {

  constructor() { }
  mapData : any = []
  ngOnInit() {

    this.mapData = [{ lat: 26.894117, lng: 75.830052, text: 'XYZ' }, { lat: 26.894117, lng: 75.830052, text: 'XYZ' }]
    var mapElement = document.getElementById('map_element');
    var map = new google.maps.Map(mapElement, { center: new google.maps.LatLng(26.9124, 75.7873), zoom: 12 });


    var self = this;

    google.maps.event.addListenerOnce(map, 'idle', function () {
      var iw = new google.maps.InfoWindow();
      var oms = new OverlappingMarkerSpiderfier(map, {
        markersWontMove: true,
        markersWontHide: true,
        basicFormatEvents: true
      });

      oms.addListener('format', function (marker, status) {
        var iconURL = status == OverlappingMarkerSpiderfier.markerStatus.SPIDERFIED ? 'assets/images/location_city_marker.png' :
          status == OverlappingMarkerSpiderfier.markerStatus.SPIDERFIABLE ? 'assets/images/pin-flats-estates.png' :
            status == OverlappingMarkerSpiderfier.markerStatus.UNSPIDERFIABLE ? 'assets/images/pin-meetup.png' :
              null;
        var iconSize = new google.maps.Size(23, 32);
        marker.setIcon({
          url: iconURL,
          size: iconSize,
          scaledSize: iconSize  // makes SVG icons work in IE
        });
      });

       for (var i = 0, len = self.mapData.length; i < len; i++) {
        (function () {  // make a closure over the marker and marker data
         // var markerData = { lat: 26.894117, lng: 75.830052, text: 'XYZ' }
          var markerData = self.mapData[i];  // e.g. { lat: 50.123, lng: 0.123, text: 'XYZ' }
          var marker = new google.maps.Marker({ position: markerData, map: map });  // markerData works here as a LatLngLiteral
          google.maps.event.addListener(marker, 'spider_click', function (e) {  // 'spider_click', not plain 'click'
            iw.setContent(markerData.text);
            iw.open(map, marker);
          });

          console.log(marker);
          oms.addMarker(marker);  // adds the marker to the spiderfier _and_ the map
        })();
      }

    });


  }



}
