import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Profile } from 'src/app/models/profile';
import { WebService } from 'src/app/service/web.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';
import { DatePipe } from '@angular/common'
/* import * as AWS from 'aws-sdk' */
import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public moduleChanged = new EventEmitter<string>();
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  p: number = 1;
  userId: any;
  publicProfileData: object;
  profileActivityData: object;
  ProfileData: object;
  htmlOptions: any = {};
  membershipDate: any;
  message: any;
  globalFilesArray: any = []
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private authService: AuthService,
    public datepipe: DatePipe
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.getUserActivity(params['id'])
        localStorage.setItem('returnUrl', '/user/' + params['id']);
        this.htmlOptions = {
          editMode: false,
          publicProfileMode: true,
          privateProfileMode: false
        }
      } else {
        this.toastr.error('No user detail found');
        this.router.navigate(['/']);
      }

    });
  }
  getUserActivity(userName: any) {
    this.WebService.publicProfileData(userName, 'getUserActivity').subscribe(resultProfileData => {
      this.spinner.hide();
      if (resultProfileData['statusCode'] == 200) {
        this.publicProfileData = resultProfileData['responsePacket']
        this.profileActivityData = this.publicProfileData['userActivityList'];
        console.log(this.publicProfileData)
        this.membershipDate = this.dateFormate(resultProfileData['responsePacket'].user.membershipDate);
       /*  this.membershipDate = this.datepipe.transform(resultProfileData['responsePacket'].user.membershipDate, 'dd-MM-yyyy'); */
      } else {
        this.message = resultProfileData['message'];
      }
    },
      error => {

      })
  }
  addhttp(url) {
    if (!/^(f|ht)tps?:\/\//i.test(url)) {
      url = "http://" + url;
    }
    return url;
  }
  dateFormate(postDate: any) {
    var newDate = postDate.split(" ");
    var newDateArr = newDate[0].split("-");
    var newTimeArr = newDate[1].split(":");
    var date1 = new Date(parseInt(newDateArr[0]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[2]), parseInt(newTimeArr[0]) + 1, parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));

    var curr_date = date1.getDate();
    var curr_month = date1.getMonth() + 1; //Months are zero based
    var curr_year = date1.getFullYear();
    var curr_year = date1.getFullYear();
    var curr_hrs = date1.getHours();
    var curr_mnt = date1.getMinutes();
    var curr_sec = date1.getSeconds();
    var newConvertedDate = curr_date + "-" + curr_month + "-" + curr_year + " " + curr_hrs + ":" + curr_mnt + ":" + curr_sec;
    //return date1.toLocaleString('en-GB', { timeZone: 'UTC' });
    return newConvertedDate;
  }

  dateTimeCalculation(postDate: any) {
    var newDate = postDate.split(" ");
    var newDateArr = newDate[0].split("-");
    var newTimeArr = newDate[1].split(":");
    var date1 = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]), parseInt(newTimeArr[0]), parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));
    /* var date1 = new Date(date1 + ' UTC'); */
   // var date2 = new Date().toUTCString();
    var d1 = new Date();
    d1.toUTCString();
    var date2 = new Date(d1.getUTCFullYear(), d1.getUTCMonth(), d1.getUTCDate(), d1.getUTCHours(), d1.getUTCMinutes(), d1.getUTCSeconds());
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffSec = Math.ceil(timeDiff / 1000);
    return this.secondsToDhms(diffSec);
  }

  secondsToDhms(seconds) {

    var d = Math.floor(seconds / (3600 * 24));
    var h = Math.floor(seconds % (3600 * 24) / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 60);

    var dDisplay = d > 0 ? d + (d == 1 ? " day " : " days ") : "";
    var hDisplay = h > 0 ? h + (h == 1 ? " hour " : " hours ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute " : " minutes ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";

    if (d > 0) {
      return dDisplay
    } else if (h > 0) {
      return hDisplay
    } else if (m > 0) {
      return mDisplay
    } else {
      return sDisplay
    }

  }
}
