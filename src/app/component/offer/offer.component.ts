import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { Offer } from 'src/app/models/Offer';
import { Pinboard } from 'src/app/models/pinboard';
import * as AWS from 'aws-sdk'
import { AuthService } from 'src/app/service/auth.service';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { NgxImageCompressService } from 'ngx-image-compress';
import { DOC_ORIENTATION } from 'ngx-image-compress/lib/image-compress';

@Component({
	selector: 'app-offer',
	templateUrl: './offer.component.html',
	styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {
	@ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent;
	@Output('changeModule')
	public orientation: number = -1;
	imageChangedEvent: any = '';
	isUploaded: any = '';
	croppedImage: any = '';
	showCropper = true;
	public show: boolean = false;
	public IsCropedImage: boolean = false;
	public tampImageWithoutCrop: string = "";
	public tampImageWithCrop: string = "";
	public moduleChanged = new EventEmitter<string>();
	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};
	po: number = 1;
	currDate: any;
	userId: any;
	userName: any;
	firstName: any;
	pinUser: any = [];
	minDate: Date;
	oldExpDate: Date;
	oldStartDate: Date;
	maxDate: Date;
	htmlOptions: any = {};
	logo: string;
	backgroundImage: string;
	pinboardArr: any;
	imageUplaodOption: boolean;
	categorydata: any;
	globalFilesArray: any = []
	offerData: object;
	userData: object;
	dropdownSettings = {};
	dropdownList = [];
	pinboardArray = [];
	offerLocationData: object;
	staticPageData: any;
	offerIcon: string;
	offerMedia: string;
	radiusInMete: any;
	offerCatData: any;
	noPinboard: boolean;
	totalCredits: any;
	myPinboardArray = [];
	myPinboardIds = [];
	pinboardCount: number
	daysCount: number
	selectedIds: number
	totalChargedCredit: number
	alreadyPurchsed = []
	exitingPinboardCount: number
	isClicked: boolean
	isValid: boolean
	fileReader: any;
	rotation = {
		1: 'rotate(0deg)',
		3: 'rotate(180deg)',
		6: 'rotate(90deg)',
		8: 'rotate(270deg)'
	};

	users: any = [];

	public Offers: Offer = {} as Offer;
	public pinboard: Pinboard = new Pinboard({});
	imgResultBeforeCompress: string;
	imgResultAfterCompress: string;
	aspectRatio: string;
	constructor(
		private WebService: WebService,
		private router: Router,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
		private authService: AuthService,
		private imageCompress: NgxImageCompressService,
	) { }

	ngOnInit() {
		this.isUploaded = false;
		localStorage.setItem('returnUrl', '/offers');

		this.isClicked = false;
		this.totalChargedCredit = 0;
		this.htmlOptions.alreadyPurchsedcount = 0;
		this.daysCount = 0;
		this.oldStartDate = new Date();
		this.oldExpDate = new Date();
		this.pinboardCount = 0;
		this.radiusInMete = 50000;
		this.noPinboard = false;
		this.pinboard.isLocationChange = false
		this.minDate = new Date();
		this.spinner.show();
		this.userId = localStorage.getItem('userId');
		this.userName = localStorage.getItem('userName');
		this.firstName = localStorage.getItem('firstName');
		this.pinUser.push({ userId: this.userId, firstName: this.firstName });
		this.getMyofferList();

		this.getCategories();
		this.getPinboardContent();
		this.getUserCredits();
		this.myPinboardList();
		this.dropdownSettings = {
			singleSelection: false,
			enableCheckAll: false,
			idField: 'id',
			textField: 'title',
			selectAllText: 'Select All',
			unSelectAllText: 'Deselect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};
		this.setHtmlOptions();
		if (localStorage.getItem('offerReturnUrl')) {
			localStorage.removeItem('offerReturnUrl')
		}
	}
	setHtmlOptions() {
		this.htmlOptions = {
			editMode: false,
			viewMode: false
		}
	}

	//hide the form and display listing page
	cancelButton() {
		this.isUploaded = false;
		this.totalChargedCredit = 0;
		this.htmlOptions.alreadyPurchsedcount = 0;
		this.daysCount = 0;
		this.pinboardCount = 0;
		this.oldStartDate = new Date();
		this.oldExpDate = new Date();
		this.htmlOptions = {
			editMode: false
		}
	}
	viewPinboard(id) {
		this.spinner.show();
		this.htmlOptions.logo = "";
		this.htmlOptions.backgroundImage = "";
		this.htmlOptions.button = false;
		this.htmlOptions.editMode = false;
		this.htmlOptions.viewMode = true;
		this.WebService.offersAction(id, 'getById').subscribe(resultofferData => {
			this.spinner.hide();
			if (resultofferData.statusCode == 200) {
				this.Offers = resultofferData.responsePacket
			}
		},
			error => {

			})
	}

	//clone the selected offer .
	cloneOffer(id) {
		this.WebService.offersAction(id, 'cloneOffer').subscribe(resultofferData => {
			if (resultofferData.statusCode == 200) {
				this.ngOnInit();
			} else {
				this.toastr.error(resultofferData['message']);
			}
		},
			error => {

			})
	}

	//blank offer form will open to add new offer
	addOffer() {
		this.isUploaded = false;
		this.totalChargedCredit = 0;
		this.htmlOptions.alreadyPurchsedcount = 0;
		this.daysCount = 0;
		this.oldStartDate = new Date();
		this.oldExpDate = new Date();
		this.htmlOptions.logo = "";
		this.htmlOptions.backgroundImage = "";
		this.htmlOptions.button = false;
		this.htmlOptions.editMode = true;
		this.htmlOptions.editOfferMode = false;
		this.htmlOptions.viewMode = false;
		this.Offers = {} as Offer;
		this.Offers.categoryId = null;
	}

	//edit offer form will open with data of selected records
	editOffer(id) {
		this.isUploaded = false;
		this.totalChargedCredit = 0;
		this.htmlOptions.alreadyPurchsedcount = 0;
		this.daysCount = 0;
		this.oldStartDate = new Date();
		this.oldExpDate = new Date();
		this.htmlOptions.newupload = true;
		this.spinner.show();
		this.htmlOptions.button = true;
		this.htmlOptions.editOfferMode = true;
		this.htmlOptions.editMode = true;
		this.htmlOptions.viewMode = false;
		this.WebService.offersAction(id, 'getById').subscribe(resultofferData => {
			this.spinner.hide();
			if (resultofferData.statusCode == 200) {

				this.Offers = resultofferData.responsePacket
				this.Offers.latitude = resultofferData.responsePacket.latitude
				this.Offers.longitude = resultofferData.responsePacket.longitude
				this.getNearByLocationPinBoardList();

				this.htmlOptions.offerIcon = resultofferData.responsePacket.offerIcon
				this.htmlOptions.offerMedia = resultofferData.responsePacket.offerMedia
				this.Offers.offerMedia = resultofferData.responsePacket.offerMedia
				this.alreadyPurchsed = resultofferData.responsePacket.pinboardList
				if (!this.alreadyPurchsed) {
					this.alreadyPurchsed = []
				}
				if (this.myPinboardArray) {
					this.exitingPinboardCount = 0;
					for (let i = 0; i < this.myPinboardArray.length; i++) {
						if (this.alreadyPurchsed.includes(this.myPinboardArray[i]['id'])) {
							this.exitingPinboardCount = this.exitingPinboardCount + 1;
						}
					}
				} else {
					this.exitingPinboardCount = this.alreadyPurchsed.length
				}
				var newVarSDate = this.Offers.startDate.toString();
				var newSDateArr = newVarSDate.split("-");
				var newStartDate = newSDateArr[2] + "-" + newSDateArr[1] + "-" + newSDateArr[0];
				/* this.Offers.startDate = new Date(newStartDate); */
				this.Offers.startDate = new Date(parseInt(newSDateArr[2]), parseInt(newSDateArr[1]) - 1, parseInt(newSDateArr[0]));
				var newVarDate = this.Offers.expiryDate.toString();
				var newDateArr = newVarDate.split("-");
				/* var newExpDate = newDateArr[2] + "," + newDateArr[1] + "," + newDateArr[0]+ ", 00, 00, 00, 00"; */
				this.Offers.expiryDate = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]));

				this.oldStartDate = this.Offers.startDate;
				this.maxDate = this.Offers.startDate;
				this.oldExpDate = this.Offers.expiryDate;

			} else {
				localStorage.setItem('statusCode', resultofferData.message);
			}
		},
			error => {

			})

	}
	//category listing
	getCategories() {
		this.WebService.getCategories('getCategory').subscribe(resultcategoryData => {
			if (resultcategoryData['statusCode'] == 200) {
				this.categorydata = resultcategoryData['responsePacket'];
			}
		},
			error => {

			})
	}

	/* getCategories() {
		this.WebService.getOfferCategory('getOfferCategory').subscribe(resultcategoryData => {
			if (resultcategoryData['statusCode'] == 200) {
				this.categorydata = resultcategoryData['responsePacket'];
			}
		},
		error => {

		})
	} */
	//image uploading from form
	fileEvent(fileInput: any, fileType) {

		/* var os = window.navigator.platform.toLowerCase();
		if (os.indexOf('mac') != -1 || os.indexOf('ios') != -1 || os.indexOf('iphone') != -1) {
			var that = this;
			var orientation = function (file, callback) {
				that.fileReader = new FileReader();
				that.fileReader.onloadend = function () {
					var base64img = "data:" + file.type + ";base64," + that._arrayBufferToBase64(that.fileReader.result);

					var scanner = new DataView(that.fileReader.result);
					var idx = 0;
					var value = 1; // Non-rotated is the default
					if (that.fileReader.result.length < 2 || scanner.getUint16(idx) != 0xFFD8) {

						if (callback) {
							callback(base64img, value);
						}
						return;
					}
					idx += 2;
					var maxBytes = scanner.byteLength;
					while (idx < maxBytes - 2) {
						var uint16 = scanner.getUint16(idx);
						idx += 2;
						switch (uint16) {
							case 0xFFE1: // Start of EXIF
								var exifLength = scanner.getUint16(idx);
								maxBytes = exifLength - idx;
								idx += 2;
								break;
							case 0x0112: // Orientation tag
								value = scanner.getUint16(idx + 6, false);
								maxBytes = 0; // Stop scanning
								break;
						}
					}
					if (callback) {
						callback(base64img, value);
					}
				}
				that.fileReader.readAsArrayBuffer(file);
			};
			var files = fileInput.target.files;
			var file = files[0];
			if (file) {
				orientation(file, function (base64img, value) {
					if (value == 3 || value == 6 || value == 8) {
						if (confirm("Landscape view works best for posts and offers.")) {
							if (fileType == 'offerIcon') {
								that.Offers.offerIcon = ""
							}
							if (fileType == 'offerMedia') {
								that.Offers.offerMedia = ""
							}
							that.globalFilesArray = [];
							return false;
						}
					}

				});
			}
		} */

		this.htmlOptions.newupload = false;
		var files = fileInput.target.files;
		var file = files[0];

		if (file.size > 5242880) {
			if (fileType == 'offerIcon') {
				this.Offers.offerIcon = ""
			}
			if (fileType == 'offerMedia') {
				this.Offers.offerMedia = ""
			}
			alert("Image size should be less than 5MB");
			this.globalFilesArray = [];
			return false;
		} else if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {
			var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

			if (pos != -1) {
				if (file == undefined) {
					this.globalFilesArray.splice(pos, 1);
				} else {
					this.globalFilesArray[pos].fileType = fileType
					this.globalFilesArray[pos].file = file
				}
			} else {
				this.globalFilesArray.push({ fileType: fileType, file: file })
			}

			if (fileType == 'offerIcon') {
				this.htmlOptions.offerIcon = ""
			}
			if (fileType == 'offerMedia') {
				this.htmlOptions.offerMedia = ""
			}
		} else {

			/* this.Offers.offerIcon = "";
			this.Offers.offerMedia = ""; */

			if (fileType == 'offerIcon') {
				this.Offers.offerIcon = ""
			}
			if (fileType == 'offerMedia') {
				this.Offers.offerMedia = ""
			}
			alert("Only jpg, png and jpeg file format allowed");
			this.globalFilesArray = [];
			return false;
		}

	}
	_arrayBufferToBase64(buffer) {
		var binary = ''
		var bytes = new Uint8Array(buffer)
		var len = bytes.byteLength;
		for (var i = 0; i < len; i++) {
			binary += String.fromCharCode(bytes[i])
		}
		return window.btoa(binary);
	}
	//called this function to upload image before form submit
	multipleProductImageUploadl(callbackError, callbackSuccess, ) {

		var _that = this
		AWS.config.update({
			region: environment.aws_region,
			credentials: new AWS.CognitoIdentityCredentials({
				IdentityPoolId: environment.aws_IdentityPoolId
			})
		})

		const s3 = new AWS.S3({
			apiVersion: environment.aws_apiVersion,
			params: { Bucket: environment.aws_bucketName }
		})
		//Key: '' + new Date().getTime() + "__" + item.fileType + "__" + item.file.name,
		var ImagesUrls = []
		if (this.globalFilesArray.length > 0) {
			this.globalFilesArray.map((item) => {
				var params = {
					Bucket: environment.aws_bucketName,
					Key: '_cmsImages/' + new Date().getTime() + "__" + item.fileType + "__" + item.file.name,
					Body: item.file,
					ACL: 'public-read'
				};
				s3.upload(params, function (err, data) {
					if (err) {
						callbackError(err)
					} else {
						let fType = data.key.split("__")[1]
						ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: fType })
						if (ImagesUrls.length == _that.globalFilesArray.length) {
							callbackSuccess(ImagesUrls)
						}
					}
				})
			})
		} else {
			callbackSuccess("")
		}
	}
	backgroundColor(event: any) {
		this.Offers.backgroundColor = event;
	}


	fromChange(event: any) {
		console.log(event);

	}
	colorCode(event: any) {
		this.Offers.backgroundColor = event;
	}

	//to save lat long we get request while user select address from form
	handleAddressChange(event: any) {
		this.Offers.location = event.formatted_address;
		this.WebService.getlatlng(event.formatted_address).subscribe(resultOffersData => {
			if (resultOffersData.status == "OK") {
				this.Offers.latitude = resultOffersData.results[0].geometry.location.lat;
				this.Offers.longitude = resultOffersData.results[0].geometry.location.lng;
				this.spinner.hide();
				this.offerLocationData = resultOffersData['responsePacket']
				this.Offers.categoryId = null;
				this.getNearByLocationPinBoardList();
			} else {
				this.spinner.hide();
				this.Offers.latitude = '';
				this.Offers.longitude = '';
				localStorage.setItem('statusCode', resultOffersData['message']);
			}
		},
			error => {

			})
	}
	//fatching the list of pinboard for targeting
	getPinboardContent() {
		this.WebService.pinboardContent(this.pinboard, 'getAll').subscribe(resultpinboardData => {
			if (resultpinboardData['statusCode'] == 200) {
				this.dropdownList = resultpinboardData['responsePacket']
			} else {

				localStorage.setItem('statusCode', resultpinboardData['message']);
			}
		},
			error => {

			})
	}
	getPinboardContentCat(catText) {
		this.pinboard.categoryName = catText;
		this.pinboard.userId = this.userId;
		this.WebService.pinboardContent(this.pinboard, 'getAll').subscribe(resultpinboardData => {
			console.log(resultpinboardData);
			if (resultpinboardData['statusCode'] == 200) {
				this.dropdownList = resultpinboardData['responsePacket']
			} else {

				localStorage.setItem('statusCode', resultpinboardData['message']);
			}
		},
			error => {

			})
	}
	//fatching the list of users offr list
	getMyofferList() {
		this.WebService.getMyofferList(this.userId, 'getMyOffersList').subscribe(resultofferData => {
			this.spinner.hide();
			if (resultofferData['statusCode'] == 200) {
				this.offerData = resultofferData['responsePacket']
			} else {
			}
		},
			error => {

			})
	}

	/* onSubmit(f: any) { */
	onSubmit() {
		/* if (!f.valid) {
			document.getElementById("error_box").scrollIntoView();

			this.isValid = true;
			var that = this;

			setTimeout(() => {
				that.isValid = false;
			}, 5000);
			return;
		} */
		if (this.Offers.pinboardObjList) {
			for (let i = 0; i < this.Offers.pinboardObjList.length; i++) {
				this.pinboardArray.push(this.Offers.pinboardObjList[i]['id']);
			}
		}
		this.Offers.pinboardList = this.pinboardArray;
		this.spinner.show();
		let self = this;
	/* 	this.multipleProductImageUploadl( */
			/* function (error) {
				if (this.htmlOptions.editMode) {
					this.submitWithoutImage()
				} else {
					alert(error)
				}
			},
			function (success) {
				if (success == '') {
				} else {
					for (let i = 0; i < success.length; i++) {
						if (success[i].ImageType == "offerMedia") {
							self.Offers.offerMedia = success[i].ImagePath
						}
					}

				} */

				var s = self.Offers.startDate;
				if (typeof s != 'object') {
					var newVarDate = self.Offers.startDate.toString();
					var newDateArr = newVarDate.split("-");
					var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
					s = new Date(newExpDate);
				}

				var curr_date = s.getDate();
				var curr_month = s.getMonth() + 1; //Months are zero based
				var curr_year = s.getFullYear();
				var newStartDate = curr_date + "-" + curr_month + "-" + curr_year;

				var e = self.Offers.expiryDate;
				if (typeof e != 'object') {
					var newVarDate = self.Offers.expiryDate.toString();
					var newDateArr = newVarDate.split("-");
					var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
					e = new Date(newExpDate);
				}
				var curr_date = e.getDate();
				var curr_month = e.getMonth() + 1; //Months are zero based
				var curr_year = e.getFullYear();
				var newExpDate = curr_date + "-" + curr_month + "-" + curr_year;

				self.Offers.userId = self.userId;
				self.Offers.userName = self.userName;
				self.Offers.totalChargedCredit = self.totalChargedCredit;
				self.WebService.addOfferData(self.Offers, 'add', newExpDate, newStartDate).subscribe(result => {
					if (result['statusCode'] == 200) {
						self.spinner.hide();
						var temp = self.htmlOptions.button;
						setTimeout(function () {
							if (temp) {
								self.toastr.success('Record updated successfully', 'Success');
							} else {
								self.toastr.success('Record saved successfully', 'Success');
							}
							window.location.reload();

						}, 500);

					} else {
						self.toastr.error(result['message']);
						self.router.navigate(["credits"]);
					}
				},
					error => {

					})
		/* 	}) */
	}

	//to active inactive and delete we called this function from form
	offerAction(id, action) {
		if (action == "delete") {
			var r = confirm("Are you sure? You will not be able to recover this in future!");
			if (r == true) {
				this.spinner.show();
				let self = this;
				this.WebService.offersAction(id, action).subscribe(resultVoucherActionData => {
					this.spinner.hide();
					if (resultVoucherActionData.statusCode == 200) {
						setTimeout(function () {
							if (action == "active") {
								self.toastr.success('Record has been activated successfully', 'Success');
							} else if (action == "inactive") {
								self.toastr.success('Record has been deactivated successfully', 'Success');

							} else if (action == "delete") {
								self.toastr.success('Record has been deleted successfully', 'Success');
							}
						}, 1000);
						this.offerData = resultVoucherActionData.responsePacket
						this.ngOnInit();
					} else {
						localStorage.setItem('statusCode', resultVoucherActionData.message);
					}
				},
					error => {

					})
			}
		} else {
			this.spinner.show();
			this.WebService.offersAction(id, action).subscribe(resultVoucherActionData => {
				this.spinner.hide();
				let self = this;
				if (resultVoucherActionData.statusCode == 200) {
					setTimeout(function () {
						if (action == "active") {
							self.toastr.success('Record has been activated successfully', 'Success');
						} else if (action == "inactive") {
							self.toastr.success('Record has been deactivated successfully', 'Success');

						} else if (action == "delete") {
							self.toastr.success('Record has been deleted successfully', 'Success');
						}
					}, 1000);
					this.offerData = resultVoucherActionData.responsePacket
					this.ngOnInit();
				} else {
					localStorage.setItem('statusCode', resultVoucherActionData.message);
				}
			},
				error => {

				})
		}

	}
	//display the nearbuy pinboards
	getNearByLocationPinBoardList() {
		if (!this.Offers.latitude) {
			this.getPinboardContent();
			/* this.toastr.error("Please select valid location");
			return; */
		} else {
			this.WebService.getNearByLocationPinBoardListWithUser(this.userId, this.Offers.latitude, this.Offers.longitude, this.radiusInMete, 'getNearByLocationPinBoardList').subscribe(resultofferData => {
				if (resultofferData['statusCode'] == 200) {
					this.noPinboard = false;
					this.dropdownList = resultofferData['responsePacket']

				} else {
					this.noPinboard = true;
					this.dropdownList = [];
					/* this.toastr.error("We do not have any pinboard on this location right now."); */
				}
			},
				error => {

				})
		}
	}
	getUserCredits() {
		this.WebService.getUserCredits(this.userId, 'getUserCredits').subscribe(resultCreditsData => {
			if (resultCreditsData['statusCode'] == 200) {
				this.totalCredits = resultCreditsData['responsePacket']['totalCredits'];
			}
		},
			error => {

			})
	}
	myPinboardList() {
		this.WebService.getMyPinboardList(this.userId, 'getMyPinboardList').subscribe(resultMyPinboardData => {
			if (resultMyPinboardData['statusCode'] == 200) {
				this.myPinboardArray = resultMyPinboardData['responsePacket'];
				for (let i = 0; i < this.myPinboardArray.length; i++) {
					if (this.myPinboardArray[i]['type'] == 2) {
						this.myPinboardIds.push(this.myPinboardArray[i]['id']);
					}
				}
			}
		},
			error => {

			})
	}
	onItemSelect(event: any) {
		if (event.id) {
			this.isClicked = true
			if (!this.myPinboardIds.includes(event.id)) {
				if (!this.alreadyPurchsed.includes(event.id)) {
					this.pinboardCount = this.pinboardCount + 1;
				} else {
					this.exitingPinboardCount = this.exitingPinboardCount + 1;
				}
			}
		}
		if (this.Offers.expiryDate && this.Offers.expiryDate.setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0)) {
			this.totalCharged();
		}
	}
	onDeSelect(event: any) {
		this.Offers.startDate = ''
		this.Offers.expiryDate = ''
		if (event.id) {
			this.isClicked = true
			if (!this.myPinboardIds.includes(event.id)) {
				if (!this.alreadyPurchsed.includes(event.id)) {
					this.pinboardCount = this.pinboardCount - 1;
				} else {
					this.exitingPinboardCount = this.exitingPinboardCount - 1;
				}
			}
		}
		this.totalCharged();
	}
	onSelectAll(event: any) {

		if (event) {
			this.pinboardCount = event.length;
		}
		if (this.myPinboardIds.length) {
			if (this.pinboardCount > this.myPinboardIds.length) {
				this.pinboardCount = this.pinboardCount - this.myPinboardIds.length
			}
			if (this.alreadyPurchsed) {
				if (this.alreadyPurchsed.length > this.pinboardCount) {
					this.pinboardCount = this.pinboardCount - this.alreadyPurchsed.length;
				}
			}
		}
		this.totalCharged();
	}
	onDeSelectAll(event: any) {

		if (event) {
			this.pinboardCount = 0;
		}
		this.totalCharged();
	}
	onFromDateSelect(event: any) {
		/* if (!this.isClicked){
			if (this.myPinboardIds.length) {
				if (this.alreadyPurchsed) {
					if (this.alreadyPurchsed.length > this.myPinboardIds.length) {
						this.exitingPinboardCount = this.alreadyPurchsed.length - this.myPinboardIds.length;
					}else{
						this.exitingPinboardCount = this.alreadyPurchsed.length;
					}
				}
			}
		} */
		this.maxDate = event;
		if (this.Offers.expiryDate) {
			this.Offers.expiryDate = this.maxDate;
			this.daysCount = 1;
		}
		this.totalCharged();

	}
	onEndsDateSelect(event: any) {
		if (this.Offers.startDate) {
			var date2 = new Date(this.Offers.expiryDate);
			var date1 = new Date(this.Offers.startDate);
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
			this.daysCount = this.daysCount + 1; //same day it will count by 1 day
		} else {
			this.daysCount = 0
		}
		this.totalCharged();

	}
	totalCharged() {
		this.totalChargedCredit = 0;
		if (this.htmlOptions.editOfferMode) {
			/* if (this.oldStartDate === this.Offers.startDate){
				console.log(this.oldStartDate+" == "+this.Offers.startDate+ "Yes")
			}else{
				console.log(this.oldStartDate + " == " + this.Offers.startDate+ "no")
			} */
			if (this.Offers.expiryDate > this.oldExpDate && this.pinboardCount == 0 && this.oldStartDate.setHours(0, 0, 0, 0) == this.Offers.startDate.setHours(0, 0, 0, 0)) {
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.oldExpDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.htmlOptions.alreadyPurchsedcount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(this.alreadyPurchsed.length)
			} else if (this.pinboardCount == 0 && this.oldStartDate < this.Offers.startDate && this.Offers.startDate > this.oldExpDate) {
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.htmlOptions.alreadyPurchsedcount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(this.alreadyPurchsed.length)
			} else if (this.pinboardCount != 0 && this.oldStartDate.setHours(0, 0, 0, 0) == this.Offers.startDate.setHours(0, 0, 0, 0) && this.oldExpDate.setHours(0, 0, 0, 0) == this.Offers.expiryDate.setHours(0, 0, 0, 0)) {
				//26 feb
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)

			} else if (this.Offers.startDate < this.oldStartDate && this.Offers.expiryDate > this.oldExpDate) {
				//19 Feb 2019
				var date2 = new Date(this.oldStartDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				var daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));

				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.oldExpDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + daysCount + 1; //same day it will count by 1 day
				let pinboardCount1 = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(pinboardCount1)

				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate > this.oldExpDate && this.pinboardCount > 0 && this.oldStartDate.setHours(0, 0, 0, 0) == this.Offers.startDate.setHours(0, 0, 0, 0) && (this.Offers.expiryDate.setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0))) {
				//18 Feb
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.oldExpDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				let pinboardCount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(pinboardCount)

				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date();
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)

			} else if (this.Offers.expiryDate > this.oldExpDate && this.pinboardCount > 0 && this.oldStartDate.setHours(0, 0, 0, 0) == this.Offers.startDate.setHours(0, 0, 0, 0) && (this.Offers.expiryDate.setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0))) {
				//18 Feb pawan
				/*var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date();
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				let pinboardCount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(pinboardCount)

				 var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount) */

			} else if (this.Offers.startDate > this.oldExpDate && this.pinboardCount == 0) {
				//26 Feb 2019
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.htmlOptions.alreadyPurchsedcount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(this.alreadyPurchsed.length)

				var date2 = new Date(this.oldStartDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)

			} else if (this.Offers.startDate >= this.oldStartDate && this.Offers.expiryDate <= this.oldExpDate) {
				//26 Feb 2019
				var date2 = new Date(this.Offers.startDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate.setHours(0, 0, 0, 0) == this.oldExpDate.setHours(0, 0, 0, 0) && this.oldStartDate.setHours(0, 0, 0, 0) == this.Offers.startDate.setHours(0, 0, 0, 0)) {
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate > this.oldExpDate && this.oldStartDate.setHours(0, 0, 0, 0) == this.Offers.startDate.setHours(0, 0, 0, 0)) {
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.oldExpDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate < this.oldExpDate && this.oldStartDate == this.Offers.startDate) {
				var date2 = new Date(this.oldExpDate);
				var date1 = new Date(this.Offers.expiryDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate > this.oldExpDate && this.oldStartDate < this.Offers.startDate) {
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)

				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.htmlOptions.alreadyPurchsedcount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(this.alreadyPurchsed.length)

			} else if (this.Offers.expiryDate.setHours(0, 0, 0, 0) == this.oldExpDate.setHours(0, 0, 0, 0) && this.oldStartDate.setHours(0, 0, 0, 0) == this.Offers.startDate.setHours(0, 0, 0, 0)) {
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate.setHours(0, 0, 0, 0) == this.oldExpDate.setHours(0, 0, 0, 0) && this.oldStartDate > this.Offers.startDate) {
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.oldStartDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate.setHours(0, 0, 0, 0) == this.oldExpDate.setHours(0, 0, 0, 0) && this.oldStartDate < this.Offers.startDate) {
				var date2 = new Date(this.oldExpDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate < this.oldExpDate && this.oldStartDate > this.Offers.startDate) {
				//26 Feb
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				let pinboardCount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(pinboardCount)

				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.oldStartDate > this.Offers.startDate) {
				//18 Feb
				var date2 = new Date(this.oldStartDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				let pinboardCount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(pinboardCount)
				var date2 = new Date(this.oldStartDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.Offers.expiryDate > this.oldExpDate && this.oldStartDate > this.Offers.startDate) {
				//18 Feb //19feb
				var date2 = new Date(this.oldStartDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				let pinboardCount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(pinboardCount)

				var date2 = new Date(this.oldStartDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else if (this.pinboardCount > 0 && this.Offers.startDate > this.oldExpDate) {
				//18 Feb
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.oldExpDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				let pinboardCount = this.alreadyPurchsed.length;
				this.exisitngChargesCalculation(pinboardCount)

				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)

			} else if (this.pinboardCount > 0 && this.oldStartDate < this.Offers.startDate) {
				var date2 = new Date(this.Offers.expiryDate);
				var date1 = new Date(this.Offers.startDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				this.daysCount = Math.ceil(timeDiff / (1000 * 3600 * 24));
				this.daysCount = this.daysCount + 1; //same day it will count by 1 day
				this.editChargesCalculation(this.pinboardCount)
			} else {
				this.daysCount = 0
			}


		}

		if (this.daysCount > 0 && this.pinboardCount > 0 && !this.htmlOptions.editOfferMode) {
			if (this.daysCount < 7) {
				var week = 1
			} else {
				week = Math.ceil(this.daysCount / 7);
			}
			this.totalChargedCredit = this.pinboardCount * week;
		}
	}
	editChargesCalculation(pinboardCount) {
		if (this.daysCount < 7) {
			var week = 1
		} else {
			week = Math.ceil(this.daysCount / 7);
		}

		this.totalChargedCredit = this.totalChargedCredit + (pinboardCount * week);


	}
	exisitngChargesCalculation(pinboardCount) {
		if (this.daysCount < 7) {
			var week = 1
		} else {
			week = Math.ceil(this.daysCount / 7);
		}
		if (this.exitingPinboardCount) {
			pinboardCount = pinboardCount - this.exitingPinboardCount
			this.totalChargedCredit = this.totalChargedCredit + (pinboardCount * week);
		}

	}
	changeModule(module: string) {
		this.ngOnInit();
	}
	dateFormate(event: any) {
		var newVarDate = event.toString();
		var newDateArr = newVarDate.split("-");
		var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
		var expireDate = new Date(newDateArr[2], newDateArr[1] - 1, newDateArr[0], 23, 59, 59);

		if (newDateArr[1].length < 2) newDateArr[1] = '0' + newDateArr[1];
		if (newDateArr[0].length < 2) newDateArr[0] = '0' + newDateArr[0];

		var newdate = [newDateArr[2], newDateArr[1], newDateArr[0]].join('-');

		newdate = newdate + " 23:59:59"

		return newdate;
	}
	onCategoryChange(e: any) {
		let selectElementText = e.target['options']
		[e.target['options'].selectedIndex].text;
		this.Offers.category = selectElementText;
		this.Offers.pinboardObjList = [];
		this.getPinboardContentCat(selectElementText);

	}
	onSubmitCheck(test) {
		alert(test)
	}
	buyCredit() {

		localStorage.setItem('offerReturnUrl', '/offers');

		this.router.navigate(['/my-offer-credits']);
	}

	fileChangeEvent(event: any): void {
		this.htmlOptions.offerMedia = ""
		this.isUploaded = true;
		this.imageChangedEvent = event;
		this.imageCropper.autoCrop = true;
		let element: any = document.getElementsByClassName('source-image');
		let fileReader = new FileReader();
		fileReader.onload = (e) => {
			this.tampImageWithoutCrop = fileReader.result.toString();
		}
		fileReader.readAsDataURL(event.target.files[0]);
		this.detectFiles(event)

	}

	imageCropped(event: ImageCroppedEvent) {
		this.croppedImage = event.base64;
		this.tampImageWithCrop = event.base64;
		this.detectFiles(event)
	}
	imageLoaded() {
		// show cropper
	}
	cropperReady() {
		// cropper ready
	}
	loadImageFailed() {
		// show message
	}
	cropImage() {
		this.IsCropedImage = true;
		this.showCropper = true;
		this.imageCropper.autoCrop = true;
		this.show = true;
	}
	fitToScreen() {
		this.IsCropedImage = true;
		this.show = true;
		this.imageCropper.autoCrop = false;
		this.showCropper = false;
	}
	Cancel() {
		this.IsCropedImage = false;
		this.show = false;
		this.imageCropper.resetCropperPosition();
		this.imageCropper.autoCrop = true;
		this.showCropper = true;
	}


	base64MimeType(encoded) {
		var result = null;

		if (typeof encoded !== 'string') {
			return result;
		}

		var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

		if (mime && mime.length) {
			result = mime[1];
		}

		return result;
	}

	compressAndUploadFile(f: any) {
	 if (!f.valid) {
		document.getElementById("error_box").scrollIntoView();

		this.isValid = true;
		var that = this;

		setTimeout(() => {
			that.isValid = false;
		}, 5000);
		return;
		}
		this.spinner.show();
		if (this.isUploaded){

		var image: any
		if (this.IsCropedImage == true) {
			image = this.tampImageWithCrop;
		} else {
			image = this.tampImageWithoutCrop;
		}
		//  this.imageCompress.uploadFile().then(({image, orientation}) => {
		var ImageType = this.base64MimeType(image);

			if (this.imageCompress.byteCount(image) > 150 && this.IsCropedImage==false)
			{
			var quality;
			var size;
			if (ImageType == "image/png") {
				quality = 30;
				/* size = 10; */

			} else {
				/* size = 50; */
				quality = 30;
			}
				this.imageCompress.compressFile(image, this.orientation, size, quality).then(
				result => {
					this.croppedImage = result;
					this.UploadToS3(result)
					console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
				}
			);
		} else {
			this.UploadToS3(image)
		}

		}else{
			this.onSubmit()
		}
		//});
	}

	UploadToS3(image: any) {

		const base64Data = new Buffer(image.replace(/^data:image\/\w+;base64,/, ""), 'base64')
		const type = image.split(';')[0].split('/')[1]
		AWS.config.update({
			region: environment.aws_region,
			credentials: new AWS.CognitoIdentityCredentials({
				IdentityPoolId: environment.aws_IdentityPoolId
			})
		})
		const s3 = new AWS.S3({
			apiVersion: environment.aws_apiVersion,
			params: { Bucket: environment.aws_bucketName }
		})
		var params = {
			Bucket: environment.aws_bucketName,
			Key: '_cmsImages/' + new Date().getTime(),
			Body: base64Data,
			ACL: 'public-read',
			ContentEncoding: 'base64', // required
			ContentType: "image/" + type // required. Notice the back ticks
		};
		var self = this
		s3.upload(params, function (err, data) {
			if (err) { return console.log(err) }
			console.log('data', data.Location)
			self.Offers.offerMedia = data.Location;
			self.onSubmit()
		})
	}
	detectFiles(event) {
		var file = event.target.files[0];
		this.getOrientation(file, (orientation) => {
			this.orientation = orientation;
			console.log('orientation', orientation)
		});
	}

	getOrientation(file, callback) {

		var reader: any,
			target: EventTarget;
		reader = new FileReader();
		reader.onload = (event) => {

			var view = new DataView(event.target.result);

			if (view.getUint16(0, false) != 0xFFD8) return callback(-2);

			var length = view.byteLength,
				offset = 2;

			while (offset < length) {
				var marker = view.getUint16(offset, false);
				offset += 2;

				if (marker == 0xFFE1) {
					if (view.getUint32(offset += 2, false) != 0x45786966) {
						return callback(-1);
					}
					var little = view.getUint16(offset += 6, false) == 0x4949;
					offset += view.getUint32(offset + 4, little);
					var tags = view.getUint16(offset, little);
					offset += 2;

					for (var i = 0; i < tags; i++)
						if (view.getUint16(offset + (i * 12), little) == 0x0112)
							return callback(view.getUint16(offset + (i * 12) + 8, little));
				}
				else if ((marker & 0xFF00) != 0xFF00) break;
				else offset += view.getUint16(offset, false);
			}
			return callback(-1);
		};

		reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
	};
}
