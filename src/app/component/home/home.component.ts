
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { ToastrService, ToastrComponentlessModule } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';
import { MouseEvent } from '@agm/core';
import { getCurrencySymbol } from '@angular/common';
import { get } from 'scriptjs';
import { environment } from '../../../environments/environment';
import { FacebookService, UIParams, UIResponse, InitParams } from 'ngx-facebook'
declare var klokantech;

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#20c997',
		type: 'square-jelly-box',
		loadigText: '...fetching your hot-off-the-press, local posts and offers...'
	};
	user: any = {};
	selectedIndex: any;
	onBoarding: any = {};
	public navActive: boolean = false;
	public mapnavActive: boolean = false;
	public filterBoxOpen: boolean = false;
	// google maps zoom level
	zoom: number = 16;
	categorydata: any;
	// initial center position for the map
	postForShare: any;
	ShareOfferData: any;
	currentLocation: boolean;
	lat: number;
	lng: number;
	latC: number;
	lngC: number;
	userName: any;
	webLoggedIn: any;
	imagePath: any;
	postComments: any[] = [];
	commentPost: any = {};
	radiusInMeter: number;
	unViewedCount: number;
	userId: any;
	htmlOptions: any = {}
	currDate: any
	markerss: any[];
	pinboardId: any;
	pinboardArr: any;
	categoryName: any;
	searchKeyWord: any;
	postArr: any;
	offerArr: any;
	WebUserProfileImage: any;
	finalOfferPostArr: any;
	mapDetailsArray: any = []
	isHome: boolean;
	isFirst: boolean;
	noPostNOffer: boolean;
	public searchActive: boolean = true;
	private changeLat: number;
	private changeLng: number;
	private changeLatTemp: number;
	private changeLngTemp: number;

	constructor(
		private WebService: WebService,
		private router: Router,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
		private authService: AuthService,
		private fb: FacebookService,
	) {
		let initParams: InitParams = {
			appId: '361946667869904',
			xfbml: true,
			version: 'v2.8'
		};

		fb.init(initParams);
	}

	ngOnInit() {
		this.spinner.show();
		localStorage.setItem('returnUrl', '/home');
		//this.imagePath = "https://adminstreetpin.s3.amazonaws.com/" + environment.aws_folderName;
		this.imagePath = environment.aws_bucketURL + environment.aws_folderName;
		this.checkLogout();
		this.selectedIndex = 1;
		this.noPostNOffer = false;
		this.isFirst = false;
		this.isHome = true;
		this.WebUserProfileImage = localStorage.getItem('WebUserProfileImage');
		this.userId = localStorage.getItem('userId');
		this.currDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
		this.mapDetailsArray = [];
		this.searchKeyWord = null;
		this.categoryName = null;
		this.radiusInMeter = 20000;
		this.lat = 51.3716652;
		this.lng = -0.0935713;

			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition((position) => {
					this.lat = position.coords.latitude;
					this.lng = position.coords.longitude;
					this.latC = position.coords.latitude;
					this.lngC = position.coords.longitude;
					this.getCurrentLocationPinboard(this.lat, this.lng);
				},
				error => {
					this.getCurrentLocationPinboard(this.lat, this.lng);
				}
			);
		}

		this.getCategories();
		this.setHtmlOptions();

		if (localStorage.getItem('userOnboardingSkip') == 'false' || !localStorage.getItem('userOnboardingSkip')) {
			document.getElementById('onBoardingClick').click();
		}

		this.webLoggedIn = localStorage.getItem('webLoggedIn');
		this.WebUserProfileImage = localStorage.getItem('WebUserProfileImage');
		if (this.webLoggedIn == 'true') {
			this.webLoggedIn = 'true';
		} else {
			this.webLoggedIn = 'false';
		}
		if (localStorage.getItem("findMe") == '5'){
			this.filterBoxOpen = !this.filterBoxOpen
			localStorage.setItem("findMe", '0')
		}

	}
	checkLogout() {
		this.webLoggedIn = localStorage.getItem('webLoggedIn');
		if (this.webLoggedIn == 'true') {
			this.webLoggedIn = 'true';
			if (localStorage.getItem("LoggedInTime")) {
				var t0 = Number(localStorage.getItem("LoggedInTime"));
				if (isNaN(t0)) t0 = 0;
				var t1 = new Date().getTime();
				var duration = t1 - t0;
				if (duration < 60 * 60 * 1000) {
					var loginTime = new Date().getTime();
					localStorage.setItem("LoggedInTime", loginTime.toString());
				} else {
					this.logout();
				}
			}
		} else {
			this.webLoggedIn = 'false';
		}
	}
	logout(): void {
		localStorage.removeItem('userId')
		localStorage.removeItem('isLoggedIn')
		this.authService.logout();
	}
	setHtmlOptions() {
		this.user.displayType = 0;
		this.htmlOptions = {
			offerBox: true,
			postBox: true,
		}
	}

	idle() {
		if (this.isFirst) {
			this.isFirst = false;

			var count = this.distance(this.changeLat, this.changeLng, this.changeLatTemp, this.changeLngTemp,'K')

			if (count>8){
				this.changeLatTemp = this.changeLat;
				this.changeLngTemp = this.changeLng;
				this.getCurrentLocationPinboard(this.changeLat, this.changeLng);
			}else{
				this.isFirst = true;
			}
		}
	}
    distance(lat1, lon1, lat2, lon2, unit) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		var radlat1 = Math.PI * lat1 / 180;
		var radlat2 = Math.PI * lat2 / 180;
		var theta = lon1 - lon2;
		var radtheta = Math.PI * theta / 180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180 / Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit == "K") { dist = dist * 1.609344 }
		if (unit == "N") { dist = dist * 0.8684 }
		console.log(dist);
		return dist;
	}
}
	centerChange(event: any) {
		if (this.changeLat){
			this.changeLatTemp = this.changeLat;
			this.changeLngTemp = this.changeLng;
		}
		if (event) {
			this.changeLat = event.lat;
			this.changeLng = event.lng;
		}
	}


	getCategories() {
		this.WebService.getCategories('getCategory').subscribe(resultcategoryData => {
			if (resultcategoryData['statusCode'] == 200) {
				this.categorydata = resultcategoryData['responsePacket'];
			}
		},
			error => {

			})
	}

	dateFormate(event: any) {
		var newVarDate = event.toString();
		var newDateArr = newVarDate.split("-");
		var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];

		var expireDate = new Date(newDateArr[2], newDateArr[1] - 1, newDateArr[0], 23, 59, 59);

		var expireDate = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]), 23, 59, 59);
		var newdate = expireDate
		/* if (newDateArr[1].length < 2) newDateArr[1] = '0' + newDateArr[1];
		if (newDateArr[0].length < 2) newDateArr[0] = '0' + newDateArr[0];

		var newdate = [newDateArr[2], newDateArr[1], newDateArr[0]].join('-');

		newdate = newdate + " 23:59:59" */

		return newdate;
	}

	diffCalculation(expireDate) {

		var timeDiff = Math.abs(expireDate.getTime() - new Date().getTime());
		// Time calculations for days, hours, minutes and seconds
		var days = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
		var hours = Math.floor((timeDiff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((timeDiff % (1000 * 60)) / 1000);

		// Output the result in an element with id="demo"
		var finaldata = days + ":" + hours + ":"
			+ minutes + ":" + seconds;

		return finaldata;
	}
	getCurrentLocationPinboard(lat, lng) {
		this.spinner.show();
		if (lat && lng) {
			this.mapDetailsArray = [];
			this.finalOfferPostArr = [];
			this.markerss = [];
			this.WebService.getNearByPinBoard(lat, lng, this.radiusInMeter, this.categoryName, this.searchKeyWord, "getNearByPinBoard").subscribe(resultSubscriptionData => {

				this.isFirst = true;
				var self = this;
				setTimeout(() => {
					self.spinner.hide();
				}, 1000);
				if (resultSubscriptionData['statusCode'] == 200) {
					console.log(resultSubscriptionData['responsePacket']);
					this.noPostNOffer = false;
					for (let i = 0; i < resultSubscriptionData['responsePacket']['pinboardList'].length; i++) {
						let imageTempUrl
						if (resultSubscriptionData['responsePacket']['pinboardList'][i]['isPaid']){

							imageTempUrl = 'assets/images/category_pins/pin-' + resultSubscriptionData['responsePacket']['pinboardList'][i]['categoryName'] + '.png';

							var pinboardUrl = "/pinboard/" + resultSubscriptionData['responsePacket']['pinboardList'][i]['pinboardName'];

						}else{

							imageTempUrl = 'assets/images/category_pins/pin-'+resultSubscriptionData['responsePacket']['pinboardList'][i]['categoryName'] + '.png';

							pinboardUrl = "/pinboard/" + resultSubscriptionData['responsePacket']['pinboardList'][i]['categoryName'] +"/"+ resultSubscriptionData['responsePacket']['pinboardList'][i]['pinboardName'];
						}
						let logo = resultSubscriptionData['responsePacket']['pinboardList'][i]['logo']
						this.mapDetailsArray.push({
							id: resultSubscriptionData['responsePacket']['pinboardList'][i]['pinboardName'],
							lat: resultSubscriptionData['responsePacket']['pinboardList'][i]['latitude'],
							lng: resultSubscriptionData['responsePacket']['pinboardList'][i]['longitude'],
							label: resultSubscriptionData['responsePacket']['pinboardList'][i]['title'],category: resultSubscriptionData['responsePacket']['pinboardList'][i]['categoryName'],
							logo: logo,
							draggable: false,
							type: 1,
							pinboardUrl: pinboardUrl,
							iconUrl: imageTempUrl,
							width: 48,
							height: 62,
						})
					}
					if (resultSubscriptionData['responsePacket']['townPinboardList']){

						for (let i = 0; i < resultSubscriptionData['responsePacket']['townPinboardList'].length; i++) {

							var pinboardUrl = "/pinboard/" + resultSubscriptionData['responsePacket']['townPinboardList'][i]['pinboardName'];


							this.mapDetailsArray.push({
								id: resultSubscriptionData['responsePacket']['townPinboardList'][i]['pinboardName'],
								lat: resultSubscriptionData['responsePacket']['townPinboardList'][i]['latitude'],
								lng: resultSubscriptionData['responsePacket']['townPinboardList'][i]['longitude'],
								label: resultSubscriptionData['responsePacket']['townPinboardList'][i]['title'],
								category: resultSubscriptionData['responsePacket']['townPinboardList'][i]['categoryName'],
								logo: resultSubscriptionData['responsePacket']['townPinboardList'][i]['logo'],
								type: 2,
								pinboardUrl: pinboardUrl,
								draggable: false,
								iconUrl: 'assets/images/location_city_marker.png',
								width: 68,
								height: 82,
							})
						}
					}

					this.pinboardArr = resultSubscriptionData['responsePacket'];
					this.offerArr = resultSubscriptionData['responsePacket']['offersList'];
					this.postArr = resultSubscriptionData['responsePacket']['postList'];
					let self = this;
					if (this.postArr){
						for (let i = 0; i < this.postArr.length; i++) {
							this.imageExists(this.imagePath + '/' + this.postArr[i]
							['userId'], function (exists) {
								if (exists) {
									self.postArr[i]['src'] = self.imagePath + '/' + self.postArr[i]['userId'];
								} else {
									self.postArr[i]['src'] = '';
								}
							})
						}
					}
					if (this.offerArr) {
					for (let i = 0; i < this.offerArr.length; i++) {
						this.imageExists(this.imagePath + '/' + this.offerArr[i]
						['userId'], function (exists) {
							if (exists) {
								self.offerArr[i]['src'] = self.imagePath + '/' + self.offerArr[i]['userId'];
							} else {
								self.offerArr[i]['src'] = '';
							}
						})
					}
				}
					/* this.finalOfferPostArr = this.postArr.concat(this.offerArr);
					this.finalOfferPostArr = this.shuffle(this.finalOfferPostArr) */
					this.finalOfferPostArr = [];
					if (this.offerArr || this.postArr){
						if (this.offerArr.length > 0 || this.postArr.length > 0) {
							this.noPostNOffer = false;
							this.finalOfferPostArr = this.postArr.concat(this.offerArr);
							//this.finalOfferPostArr = this.shuffle(this.finalOfferPostArr)
						} else {
							this.noPostNOffer = true;
						}
					} else {
						this.noPostNOffer = true;
					}

				} else {
					this.noPostNOffer = true;
					//this.toastr.error(resultSubscriptionData['message'])
					/* this.ngOnInit(); */
				}
			},
				error => {

				})
			this.markerss = this.mapDetailsArray;
		}
	}
	imageExists(url, callback) {
		var img = new Image();
		img.onload = function () { callback(true); };
		img.onerror = function () { callback(false); };
		img.src = url;
	}
	shuffle(array) {
		var currentIndex = array.length, temporaryValue, randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}


	clickedMarker(label: string, index: number) {
		this.router.navigate(['/pinboard/' + label]);
	}

	showType(event: any) {
		if (event == 1) {
			this.htmlOptions = {
				offerBox: false,
				postBox: true,
			}
		} else if (event == 2) {
			this.htmlOptions = {
				offerBox: true,
				postBox: false,
			}
		} else {
			this.htmlOptions = {
				offerBox: true,
				postBox: true,
			}
		}
	}
	underDevelopment() {
		this.toastr.warning('Under Development');
	}
	handleAddressChange(event: any) {
		this.mapDetailsArray = [];
		this.searchKeyWord = null;
		this.categoryName = null;
		this.radiusInMeter = 20000;
		this.isFirst = false;
		this.WebService.getlatlng(event.formatted_address).subscribe(resultpinboardData => {
			if (resultpinboardData.status == "OK") {
				this.lat = resultpinboardData.results[0].geometry.location.lat;
				this.lng = resultpinboardData.results[0].geometry.location.lng;
			} else {
				this.lat = 51.3716652;
				this.lng = -0.0935713;
			}

			this.getCurrentLocationPinboard(this.lat, this.lng);
		},
			error => {

			})
	}

	saveRadiusInMeter(event: any) {
		if (event.target.value > 9500) {
			this.radiusInMeter = 20000;
		} else if (event.target.value) {
			this.radiusInMeter = event.target.value;
		}
	}
	onChangeCate(event: any) {

		if (event.target.value != "Select Category") {
			this.categoryName = event.target.value;
		} else {
			//this.ngOnInit()
			this.categoryName = null;
			this.getCurrentLocationPinboard(this.lat, this.lng);
		}
	}
	onKey(event: any) {
		if (event.target.value) {
			this.searchKeyWord = event.target.value;
		} else {
			this.searchKeyWord = null;
		}
		if(event.keyCode==13){
			document.getElementById('searchButton').click();
		}
	}
	seachBycatAndRadius() {
		this.getCurrentLocationPinboard(this.lat, this.lng);
	}

	mapLoad(map) {
		this.renderGeolocationControl(map);
	}

	renderGeolocationControl(map) {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition((position) => {
				this.lat = position.coords.latitude;
				this.lng = position.coords.longitude;
			});
		}
	}
	closePopup() {
		this.noPostNOffer = false;
	}
	onScroll() {
		this.getCurrentLocationPinboard(this.lat, this.lng);
	}
	offerLatLong(postOffer: any){
		/* this.lat = postOffer.latitude
		this.lng = postOffer.longitude */
		this.lat = parseFloat(postOffer.latitude)
		this.lng = parseFloat(postOffer.longitude)
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
		//this.getCurrentLocationPinboard(this.lat, this.lng);
	}
	getCurrentLocationPinboardOfferLatLong(lat, lng) {
		if (lat && lng) {
			this.radiusInMeter = 1000;
			this.mapDetailsArray = [];
			this.finalOfferPostArr = [];
			this.markerss = [];
			this.WebService.getNearByPinBoard(lat, lng, this.radiusInMeter, this.categoryName, this.searchKeyWord, "getNearByPinBoard").subscribe(resultSubscriptionData => {
				this.spinner.hide();
				this.isFirst = true;
				if (resultSubscriptionData['statusCode'] == 200) {

					this.noPostNOffer = false;
					for (let i = 0; i < resultSubscriptionData['responsePacket']['pinboardList'].length; i++) {
						let imageTempUrl
						if (resultSubscriptionData['responsePacket']['pinboardList'][i]['isPaid']) {
							imageTempUrl = resultSubscriptionData['responsePacket']['pinboardList'][i]['categoryImageUrl']
						} else {

							imageTempUrl = 'assets/images/category_pins/pin-' + resultSubscriptionData['responsePacket']['pinboardList'][i]['categoryName'] + '.png';
						}
						this.mapDetailsArray.push({
							id: resultSubscriptionData['responsePacket']['pinboardList'][i]['pinboardName'],
							lat: resultSubscriptionData['responsePacket']['pinboardList'][i]['latitude'],
							lng: resultSubscriptionData['responsePacket']['pinboardList'][i]['longitude'],
							label: resultSubscriptionData['responsePacket']['pinboardList'][i]['title'],
							draggable: false,
							iconUrl: imageTempUrl,
						})
					}
					if (resultSubscriptionData['responsePacket']['townPinboardList']) {

						for (let i = 0; i < resultSubscriptionData['responsePacket']['townPinboardList'].length; i++) {
							this.mapDetailsArray.push({
								id: resultSubscriptionData['responsePacket']['townPinboardList'][i]['pinboardName'],
								lat: resultSubscriptionData['responsePacket']['townPinboardList'][i]['latitude'],
								lng: resultSubscriptionData['responsePacket']['townPinboardList'][i]['longitude'],
								label: resultSubscriptionData['responsePacket']['townPinboardList'][i]['title'],
								draggable: false,
								iconUrl: 'assets/images/location_city_marker.png',
							})
						}
					}

					this.pinboardArr = resultSubscriptionData['responsePacket'];
					console.log(this.pinboardArr);
					this.offerArr = resultSubscriptionData['responsePacket']['offersList'];
					this.postArr = resultSubscriptionData['responsePacket']['postList'];
					/* this.finalOfferPostArr = this.postArr.concat(this.offerArr);
					this.finalOfferPostArr = this.shuffle(this.finalOfferPostArr) */
					if (this.offerArr || this.postArr) {
						if (this.offerArr.length > 0 || this.postArr.length > 0) {
							this.noPostNOffer = false;
							this.finalOfferPostArr = this.postArr.concat(this.offerArr);
							//this.finalOfferPostArr = this.shuffle(this.finalOfferPostArr)
						} else {
							this.noPostNOffer = true;
						}
					} else {
						this.noPostNOffer = true;
					}

				} else {
					this.noPostNOffer = true;

				}
			},
				error => {

				})
			this.markerss = this.mapDetailsArray;
		}
	}
	setPostId(postId) {
		this.commentPost.postId = postId;
	}
	addComment(postId, idx) {
		if (!this.userId){
			this.toastr.error('Please log in to create a post or reply');
			return
		}
		if (!this.commentPost[idx]) {
			this.toastr.error('Please fill the text in comment box');
			return
		}
		this.spinner.show();
		let commentData = {
			"postId": postId,
			"userId": this.userId,
			"userName": this.userName,
			"commentText": this.commentPost[idx]
		}
		this.WebService.addComment(commentData, 'add-comment').subscribe(result => {
			if (result['statusCode'] == 200) {
				this.spinner.hide();
				var temp = this.htmlOptions.button;
				this.toastr.success('Comment added successfully', 'Success');
				this.commentPost[idx] = ''
				/* this.ngOnInit(); */
				/* this.getCurrentLocationPinboard(this.lat, this.lng); */
				this.getComments(postId, idx)

			} else {
				this.toastr.error(result['message']);
				this.ngOnInit();
			}
		},
			error => {

			})
	}
	getComments(postId, idx) {
		this.WebService.getCommentByPostId(postId, 'getCommentByPostId').subscribe(result => {
			console.log(result['responsePacket']);
			if (result['statusCode'] == 200) {
				this.postComments[postId] = result['responsePacket']
				var self = this;

				for (let i = 0; i < this.postComments[postId].length; i++) {
					this.imageExists(this.imagePath + '/' + this.postComments[postId][i]['userId'], function (exists) {
						if (exists) {
							self.postComments[postId][i]['src'] = self.imagePath + '/' + self.postComments[postId][i]['userId'];
						} else {
							self.postComments[postId][i]['src'] = '';
						}
					})
				}

				self.postComments[postId].sort(function (x, y) {
					var xUpdatedDate = self.changeDate(x.updatedDate);
					var yUpdatedDate = self.changeDate(y.updatedDate);
					if (xUpdatedDate < yUpdatedDate) {
						return -1;
					}
					if (xUpdatedDate > yUpdatedDate) {
						return 1;
					}
					return 0;
				});
				this.finalOfferPostArr[idx].commentCount = result['responsePacket'].length
			}
		},
			error => {

			})
	}
	changeDate(reqDate: any) {
		var newDate = reqDate.split(" ");
		var newDateArr = newDate[0].split("-");
		var newTimeArr = newDate[1].split(":");
		var date = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]), parseInt(newTimeArr[0]), parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));
		return date.getTime();
	}
	dateTimeCalculation(postDate: any) {
		var newDate = postDate.split(" ");
		var newDateArr = newDate[0].split("-");
		var newTimeArr = newDate[1].split(":");
		var date1 = new Date(parseInt(newDateArr[0]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[2]), parseInt(newTimeArr[0]), parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));
		//var date1 = new Date(date1 + ' UTC');
		var d1 = new Date();
		d1.toUTCString();
		var date2 = new Date(d1.getUTCFullYear(), d1.getUTCMonth(), d1.getUTCDate(), d1.getUTCHours(), d1.getUTCMinutes(), d1.getUTCSeconds());
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffSec = Math.ceil(timeDiff / 1000);
		return this.secondsToDhms(diffSec);
	}

	secondsToDhms(seconds) {

		var d = Math.floor(seconds / (3600 * 24));
		var h = Math.floor(seconds % (3600 * 24) / 3600);
		var m = Math.floor(seconds % 3600 / 60);
		var s = Math.floor(seconds % 60);

		var dDisplay = d > 0 ? d + (d == 1 ? " day ago" : " days ago") : "";
		var hDisplay = h > 0 ? h + (h == 1 ? " hour ago" : " hours ago") : "";
		var mDisplay = m > 0 ? m + (m == 1 ? " minute ago" : " minutes ago") : "";
		var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";

		if (d > 0) {
			return dDisplay
		} else if (h > 0) {
			return hDisplay
		} else if (m > 0) {
			return mDisplay
		} else {
			return 'Just Now'
		}

	}

	postReport(ev: any) {
		this.spinner.show();
		this.WebService.postAction(ev, 'reportActionOnPost').subscribe(resultNotificationCount => {
			this.spinner.hide();
			if (resultNotificationCount['statusCode'] == 200) {
				this.toastr.success('your request has been sent', 'Success');
				this.ngOnInit();
			} else {
				this.toastr.error(resultNotificationCount['message']);
			}
		},
			error => {

			})
	}
	pinboardAction(id, action) {
		var r = confirm("Are you sure? You will not be able to recover this in future!");
		if (r == true) {
			this.spinner.show();
			this.WebService.postAction(id, action).subscribe(resultPinBoardActionData => {
				this.spinner.hide();
				if (resultPinBoardActionData.statusCode == 200) {
					setTimeout(function () {
						this.toastr.success('Post has been deleted successfully', 'Deleted');
					}, 1000);
					this.ngOnInit();
				} else {
					this.toastr.error(resultPinBoardActionData.message);
				}
			},
				error => {

				})
		}
	}

	clusterEvent(event){
		console.log("Clicked 12" + event);
	}

	openSharePopup(pinboardName: any, category: any, offerData: any) {

		this.postForShare = environment.site_url + 'pinboard/' + category + '/' + pinboardName + '/' + offerData.id;
		this.ShareOfferData = offerData;
		console.log(this.postForShare);

		document.getElementById("openExtraSetailsPopup").click();
	}

	share(url: string) {
		/* url = url+'/' */
		let params: UIParams = {
			href: url,
			method: 'share'
		};
		console.log(url)
		this.fb.ui(params)
			.then((res: UIResponse) => console.log(res))
			.catch((e: any) => console.error(e));

	}
	twitterShare(url: any, ShareOfferData: any) {
		console.log(url);
		// Opens a pop-up with twitter sharing dialog
		var shareURL = "http://twitter.com/share?"; //url base
		//params
		var params = {
			url: url,
			text: ShareOfferData.title,
			via: "Streetpin",
			hashtags: ShareOfferData.servicesHashtags
		}
		for (var prop in params) shareURL += '&' + prop + '=' + encodeURIComponent(params[prop]);
		window.open(shareURL, '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
	}
	splitString(category: any){
		if (category){
			var cat = category
			if (category == "club-society"){
				cat = "club society";
			} else if (category == "flats-estates"){
				cat = "flats & estates ";
			} else if (category == "food-drink"){
				cat = "food & drink ";
			} else if (category == "travel-agent"){
				cat = "travel agent ";
			}
			return cat;
			//return category.replace('-', " ");
			//return catName.charAt(0).toUpperCase() + catName.slice(1)
		}
	}
	showDiv(index) {
		console.log(index)
		this.selectedIndex = index;
	}
	onFilterChange(event: any) {
		console.log(event);
		if (event == 'A') {
			localStorage.setItem('userOnboardingSkip', 'true')
		} else {
			localStorage.setItem('userOnboardingSkip', 'false')
		}

	}
	substr(str : any){
		if (str){
			if (str.length > 150){
				return str.substr(0, 150)+"...";
			}else{
				return str.substr(0, 150);
			}
		}
	}
	addhttp(url) {
		if (!/^(f|ht)tps?:\/\//i.test(url)) {
			url = "http://" + url;
		}
		return url;
	}
	findMeClick(findMe: any) {
		this.filterBoxOpen = !this.filterBoxOpen
	 	this.navActive = !this.navActive
		/* if (findMe){
			document.getElementById('findMeBox').click();
		} */
	}
}
