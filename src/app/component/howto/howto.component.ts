import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-howto',
  templateUrl: './howto.component.html',
  styleUrls: ['./howto.component.css']
})
export class HowtoComponent implements OnInit {
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  userId: any;
  subscription: any = {};
  PageData: any;

  /* public Profile: Subscription = new Subscription({}); */
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    localStorage.setItem('returnUrl','/howto');
    this.getPageContent();
  }
  getPageContent() {
    this.WebService.getPageContent(3).subscribe(resultPagedata => {
      this.spinner.hide();
      if (resultPagedata['statusCode'] == 200) {
        this.PageData = resultPagedata['responsePacket'];
        console.log(this.PageData)
      }
    },
      error => {

      })
  }

}
