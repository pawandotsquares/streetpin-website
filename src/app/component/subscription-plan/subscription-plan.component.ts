import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
/* import { Subscription } from 'src/app/models/subscription'; */
import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from "ngx-stripe";
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';

@Component({
	selector: 'app-subscription-plan',
	templateUrl: './subscription-plan.component.html',
	styleUrls: ['./subscription-plan.component.css']
})
export class SubscriptionPlanComponent implements OnInit {
	@ViewChild(StripeCardComponent) card: StripeCardComponent;
	user: any = {};
	promoCode: any = {};
	htmlOptions: any = {};
	couponData: any;
	couponDiscountValue: any;
	oldPrice: any;
	totalCredits: any;
	cardOptions: ElementOptions = {
		hidePostalCode: true,
		style: {
			base: {
				iconColor: 'rgb(45, 48, 51)',
				color: 'rgb(114, 114, 114)',
				lineHeight: '40px',
				// fontWeight: 300,
				// fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSize: '16px',
				'::placeholder': {
					color: 'rgb(114, 114, 114)'
				}
			}
		}
	};
	elementsOptions: ElementsOptions = {
		locale: 'en'
	};
	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};
	userId: any;
	webLoggedIn: any;
	subscription: any = {};
	subscriptionArr: any;

	/* public Profile: Subscription = new Subscription({}); */
	constructor(
		private WebService: WebService,
		private router: Router,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
		private authService: AuthService,
		private stripeService: StripeService,
	) { }

	ngOnInit() {
		this.webLoggedIn = localStorage.getItem('webLoggedIn');
		console.log(this.webLoggedIn)
		localStorage.setItem('returnUrl', '/my-offer-credits');
		this.htmlOptions = {
			haveApplyCode: true,
			applyCode: false,
			appliedCode: false
		}

		this.spinner.show();
		this.userId = localStorage.getItem('userId');
		this.subscriptionList();
		if (this.webLoggedIn){
			this.getUserCredits();
		}

	}
	getUserCredits() {
		this.WebService.getUserCredits(this.userId, 'getUserCredits').subscribe(resultCreditsData => {
			console.log(resultCreditsData);
			this.spinner.hide();
			if (resultCreditsData['statusCode'] == 200) {
				this.totalCredits = resultCreditsData['responsePacket']['totalCredits'];
			} else {
				/* this.toastr.error(resultCreditsData['message']); */
			}
		},
			error => {

			})
	}
	subscriptionList() {
		this.WebService.getSubscriptionPlanList('getSubscriptionPlanList').subscribe(resultSubscriptionData => {
			this.spinner.hide();
			if (resultSubscriptionData['statusCode'] == 200) {
				this.subscriptionArr = resultSubscriptionData['responsePacket'];
			} else {
				/* this.toastr.error(resultSubscriptionData['message']); */
			}
		},
			error => {

			})
	}
	buyPlan(planId, planAmount, credits) {
		if (localStorage.getItem('isLoggedIn') != "true") {
			this.router.navigate(['/login']);
			return false;
		}
		this.cancelPromoCode();

		this.subscription.planId = planId;
		this.subscription.planAmount = planAmount;
		this.subscription.credits = credits;
		this.subscription.subscriptionPlanType = 2;
		this.subscription.discountAmout = 0;
		this.subscription.amountPaid = planAmount;

		let user = {
			"userId": this.userId
		}
		this.subscription.user = user;
		document.getElementById("openPaymentPopup").click();

	}

	buy() {
		let self = this;
		this.stripeService
			.createToken(this.card.getCard(), { name })
			.subscribe(result => {
				if (result.token) {
					this.subscription.token = result.token.id;
					var element = <HTMLInputElement>document.getElementById("buttonDisabled");
					element.disabled = true;
					self.WebService.subscribePlan(self.subscription, 'subscribePlan').subscribe(resultSubscriptionData => {
						var element = <HTMLInputElement>document.getElementById("buttonDisabled");
						if (resultSubscriptionData['statusCode'] == 200) {
							self.toastr.success('Credit has been added to your account.', 'Payment Success');
							//window.location.reload();
							document.getElementById('closePaymentPopup').click();
							if (localStorage.getItem('offerReturnUrl')){
								this.router.navigate(['/offers']);
							}else{
								self.ngOnInit();
							}
							element.disabled = false;
						} else {
							self.toastr.error(resultSubscriptionData['message']);
							element.disabled = false;
						}
					},
						error => {

						})
				} else if (result.error) {
					// Error creating the token
					this.toastr.error(result.error.message);
				}
			});
	}
	haveApplyCode() {
		this.htmlOptions = {
			haveApplyCode: false,
			applyCode: true,
			appliedCode: false
		}
	}
	cancelPromoCode() {
		this.promoCode.promoCodeText = "";
		this.htmlOptions = {
			haveApplyCode: true,
			applyCode: false,
			appliedCode: false
		}
	}
	applyPromoCode() {
		if (!this.promoCode.promoCodeText){
			this.toastr.error('Please enter valid promo code');
			return;
		}
		let user = {
			"userId": this.userId
		}
		let promoCode = {
			user: user,
			couponCode: this.promoCode.promoCodeText
		}
		this.oldPrice = this.subscription.planAmount;
		var elementButton = <HTMLInputElement>document.getElementById("applyButtonDisabled");
		elementButton.classList.add("disabled");
		this.WebService.checkCouponValidity(promoCode, 'checkCouponValidity').subscribe(resultPromoData => {
			elementButton.classList.remove("disabled");
			if (resultPromoData['statusCode'] == 200) {
				this.couponData = resultPromoData['responsePacket'];
				if (this.couponData.couponType == 2) {
					let discountPerAmout = (this.couponData.couponValue / 100) * this.subscription.planAmount
					this.subscription.planAmount = this.subscription.planAmount - discountPerAmout;
					this.couponDiscountValue = discountPerAmout;
				} else {
					if (this.couponData.couponValue <= this.subscription.planAmount) {
						this.subscription.planAmount = this.subscription.planAmount - this.couponData.couponValue;
						this.couponDiscountValue = this.couponData.couponValue;
					} else {
						this.couponDiscountValue = this.subscription.planAmount;
						this.subscription.planAmount = 0;
					}
				}

				this.htmlOptions = {
						haveApplyCode: false,
						applyCode: false,
						appliedCode: true
					}
				this.htmlOptions.appliedCodeText = this.couponData.couponCode;
			} else {
				this.toastr.error(resultPromoData['message']);
			}
		},
			error => {

			})
	}
	removePromoCode() {
		this.subscription.planAmount = this.oldPrice;
		console.log(this.subscription.planAmount + " " + this.oldPrice)
		this.cancelPromoCode()
		this.htmlOptions = {
			haveApplyCode: true,
			applyCode: false,
			appliedCode: false
		}
	}
	payWithoutAmount(){
		this.subscription.amountPaid = this.subscription.planAmount;
		var element = <HTMLInputElement>document.getElementById("buttonDisabled");
		element.disabled = true;this.subscription
		this.WebService.subscribePlan(this.subscription, 'subscribePlan').subscribe(resultSubscriptionData => {
			var element = <HTMLInputElement>document.getElementById("buttonDisabled");
			if (resultSubscriptionData['statusCode'] == 200) {
				this.toastr.success('Credit has been added to your account.', 'Payment Success');
				//window.location.reload();
				document.getElementById('closePaymentPopup').click();
				if (localStorage.getItem('offerReturnUrl')) {
					this.router.navigate(['/offers']);
				} else {
					this.ngOnInit();
				}
				element.disabled = false;
			} else {
				this.toastr.error(resultSubscriptionData['message']);
				element.disabled = false;
			}
		},
			error => {

			})
	}
}
