import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/models/profile';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  htmlOptions: any = {};
  message: any;
  returnUrl: string;

  public Profile: Profile = new Profile({});

  constructor(
    private Router: Router,
    private WebService: WebService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService

  ) { }

  ngOnInit() {

  }

  onSubmit(){
    if (this.Profile.password != this.htmlOptions.cpassword){
      return;
    }
    this.spinner.show();
    this.WebService.userSignUp(this.Profile, 'userRegister').subscribe(result => {
      this.spinner.hide();
      if (result['statusCode'] == 200) {
        /* this.toastr.success('A verification link has been sent to your registered email, Please verify your account to login', 'Account created'); */

        alert('A verification link has been sent to your registered email, Please verify your account to login');

        this.returnUrl = '/login';
        this.Router.navigate([this.returnUrl]);
      } else {
        this.toastr.error(result['message']);
      }
    },
      error => {

      })
  }
}
