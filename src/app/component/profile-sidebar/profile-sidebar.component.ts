import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-sidebar',
  templateUrl: './profile-sidebar.component.html',
  styleUrls: ['./profile-sidebar.component.css']
})
export class ProfileSidebarComponent implements OnInit {
  @Output('changeModule')
  moduleChanged = new EventEmitter();
  WebUserProfileImage: any;
  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.WebUserProfileImage = localStorage.getItem('WebUserProfileImage');
  }
  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
  changeModule(module: string) {
    this.moduleChanged.emit(module);
  }

}
