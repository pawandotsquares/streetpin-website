import { Component, OnInit, Output,EventEmitter, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Post } from 'src/app/models/post';
import { AuthService } from 'src/app/service/auth.service';
import * as AWS from 'aws-sdk'
import { environment } from '../../../environments/environment';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { NgxImageCompressService } from 'ngx-image-compress';
import { FacebookService, UIParams, UIResponse, InitParams  } from 'ngx-facebook'


@Component({
  selector: 'app-pinboard',
  templateUrl: './pinboard.component.html',
  styleUrls: ['./pinboard.component.css']
})
export class PinboardComponent implements OnInit {
  @ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent;
  public orientation: number = -1;
  imageChangedEvent: any = '';
  isUploaded: any = '';
  croppedImage: any = '';
  showCropper = true;
  public show: boolean = false;
  public IsCropedImage: boolean = false;
  public tampImageWithoutCrop: string = "";
  public tampImageWithCrop: string = "";
  isClicked: boolean
  isValid: boolean
  shareObj = {
    href: "https://192.168.2.86:4200/pinboard/beauty/test_pinboard__25",
    hashtag: "#FACEBOOK-SHARE-HASGTAG"
  };

  user: any ={};
  fileReader : any;
  commentPost: any ={};
  postComments: any[] =[];
  postForShare: any;
  isTown: boolean;
  ShareOfferData: any;
  isLoggedIn: any;
  followingCount: number;
  noPostNOffer: boolean;
  globalFilesArray: any = []
  public navActivelessmore: boolean = false;
  public navPostShow: boolean = false;
  public moreActive: boolean = false;
  public navActive: boolean = false;
  status: boolean = false;
  moreOfferBox:any={};
  @Output('changeModule')
  moduleChanged = new EventEmitter();
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#20c997',
    type: 'square-jelly-box',
    loadigText: '...fetching your hot-off-the-press, local posts and offers...'
  };
  userId: any;
  pageRefreshCount: number = 0;
  userName: any;
  catId: any;
  shareOfferId: any;
  IsOfferFound: boolean;
  pinboardType: any;
  pinboardId: any;
  pinboardNewId: any;
  newPinboardId: any;
  pinboardTitle: any;
  unViewedCount: number;
  follow: boolean;
  webLoggedIn: any;
  imagePath: any;
  categorydata: any;
  pinboardArr: any;
  pinboardDetailsArr: any;
  WebUserProfileImage: any;
  postArr: any;
  offerArr: any;
  openingTime: any = {};
  finalOfferPostArr: any;
  htmlOptions: any = {};
  isFollowed: boolean;
  selfPinboard: boolean;
  pinboardUserId: number;
  rotation = {
  1: 'rotate(0deg)',
  3: 'rotate(180deg)',
  6: 'rotate(90deg)',
  8: 'rotate(270deg)'
};
  public Post: Post = {} as Post;

  constructor(
    private WebService: WebService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authService: AuthService,
    private fb: FacebookService,
    private imageCompress: NgxImageCompressService,
  ) {

    let initParams: InitParams = {
      /* appId: '1393148214161425', */
      appId: '361946667869904',
      xfbml: true,
      version: 'v2.8'
    };

    fb.init(initParams);
  }

  ngOnInit() {
    this.spinner.show();
    this.checkLogout()
    this.imagePath = environment.aws_bucketURL + environment.aws_folderName;
    //screen.orientation.lock("landscape");
    this.isTown = false;
    this.pinboardType = 1
    this.navActivelessmore = false;
    this.Post.expiryTimePeriod = 3;
    this.Post.categoryId = null;
    this.noPostNOffer = false;
    this.userId = localStorage.getItem('userId')
    this.userName = localStorage.getItem('userName')
    this.selfPinboard = false;
    this.WebUserProfileImage = localStorage.getItem('WebUserProfileImage');
    this.route.params.subscribe(params => {
      if (params['id']) {
        if (params['catId']){
          localStorage.setItem('returnUrl', '/pinboard/' + params['catId']+'/'+params['id']);
          this.catId = params['catId'];
          if (this.catId == 2){
            this.pinboardType = 2
          }
        }else{
          localStorage.setItem('returnUrl', '/pinboard/' + params['id']);
        }
        if (params['offer']) {
          this.shareOfferId = params['offer'];
          this.IsOfferFound = true
          this.pageRefreshCount = this.pageRefreshCount + 1
        }

        if (this.pageRefreshCount >= 1) {
          var self = this;
          setTimeout(function () {
            self.shareOfferId = '';
          }, 20000);
        }
        this.pinboardId = params['id'];
        this.getPinboardIdByName(this.pinboardId);
        this.getPinboardDetailsWithPostAndOffers(params['id'], this.pinboardType);

      }else{
        this.toastr.error('No pinboard details found');
        this.router.navigate(['/']);
      }
    });

    /* this.getUserNotification(); */
    this.isLoggedIn = localStorage.getItem('isLoggedIn');

    if (this.isLoggedIn == 'true') {
      this.isLoggedIn = 'true';
    } else {
      this.isLoggedIn = 'false';
    }
    this.setHtmlOptions();

    this.isLoggedIn = localStorage.getItem('isLoggedIn');

    if (this.isLoggedIn == 'true') {
      this.isLoggedIn = 'true';
    } else {
      this.isLoggedIn = 'false';
    }
    this.getPostCategory();

  }
  checkLogout() {
    this.webLoggedIn = localStorage.getItem('webLoggedIn');
    if (this.webLoggedIn == 'true') {
      this.webLoggedIn = 'true';
      if (localStorage.getItem("LoggedInTime")) {
        var t0 = Number(localStorage.getItem("LoggedInTime"));
        if (isNaN(t0)) t0 = 0;
        var t1 = new Date().getTime();
        var duration = t1 - t0;
        if (duration < 60 * 60 * 1000) {
          var loginTime = new Date().getTime();
          localStorage.setItem("LoggedInTime", loginTime.toString());
        } else {
          this.logout();
        }
      }
    } else {
      this.webLoggedIn = 'false';
    }
  }
  logout(): void {
    localStorage.removeItem('userId')
    localStorage.removeItem('isLoggedIn')
    this.authService.logout();
  }
  getPinboardIdByName(pinboardId : any) {
    this.WebService.getPinboardIdByName(pinboardId, 'getPinboardIdByName').subscribe(resultData => {
      if (resultData['statusCode'] == 200) {
        this.pinboardNewId = resultData['responsePacket']['id'];
        this.isFollowPinboard(this.pinboardNewId)
        this.getPinboardFollowerCount(this.pinboardNewId);
      }
    },
      error => {

      })
  }
  setHtmlOptions() {
    this.user.displayType = 0
    this.htmlOptions = {
      editMode: false,
      offerBox: true,
      postBox: true,
    }
  }
  dateFormate(event: any) {
    var newVarDate = event.toString();
    var newDateArr = newVarDate.split("-");
    var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
    var expireDate = new Date(newDateArr[2], newDateArr[1] - 1, newDateArr[0], 23, 59, 59);

    var expireDate = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]), 23, 59, 59);
    var newdate = expireDate

    return newdate;
  }
  getPostCategory() {
    this.WebService.getPostCategory('getPostCategory').subscribe(resultcategoryData => {
      if (resultcategoryData['statusCode'] == 200) {
        this.categorydata = resultcategoryData['responsePacket'];
      }
    },
      error => {

      })
  }
  getPinboardDetailsWithPostAndOffers(pinboardId, pinboardType){

    this.WebService.getPinboardDetailsWithPostAndOffersByName(pinboardId, pinboardType, 'getPinboardDetailsWithPostAndOffers').subscribe(resultSubscriptionData => {
      var self = this;
      setTimeout(() => {
        self.spinner.hide();
      }, 2000);
      if (resultSubscriptionData['statusCode'] == 200) {
        this.pinboardUserId = resultSubscriptionData['responsePacket']["user"]['userId'];

        /* if (!this.userId) {
          this.selfPinboard = false;
        } else  */
        if (this.pinboardUserId == this.userId) {
          this.selfPinboard = false;
        } else {
          this.selfPinboard = true;
        }
        this.pinboardArr = resultSubscriptionData['responsePacket'];
        console.log(this.pinboardArr);
        if (resultSubscriptionData['responsePacket']['townPinboard']){
          this.isTown = true;
          this.pinboardDetailsArr = resultSubscriptionData['responsePacket']['townPinboard'];
          this.openingTime = JSON.parse(resultSubscriptionData['responsePacket']['townPinboard']['businessInformation']['openingHours']);
          this.pinboardTitle = resultSubscriptionData['responsePacket']['townPinboard']['title'];
        }else{
          this.isTown = false;
          this.pinboardDetailsArr = resultSubscriptionData['responsePacket']['pinboard'];
          this.openingTime = JSON.parse(resultSubscriptionData['responsePacket']['pinboard']['businessInformation']['openingHours']);
          this.pinboardTitle = resultSubscriptionData['responsePacket']['pinboard']['title'];
        }
        this.newPinboardId = this.pinboardDetailsArr['id']
        /* this.openingTime = JSON.parse(resultSubscriptionData['responsePacket']['pinboard']['businessInformation']['openingHours']); */

        this.offerArr = resultSubscriptionData['responsePacket']['offersList'];
        this.postArr = resultSubscriptionData['responsePacket']['postList'];
        var self = this;
        if (this.postArr) {
        for (let i = 0; i < this.postArr.length; i++) {
          var url = this.imagePath + '/' + this.postArr[i]
          ['userId'];
          this.imageExists(this.imagePath + '/' +  this.postArr[i]
          ['userId'], function (exists) {
            if (exists) {
              self.postArr[i]['src'] = self.imagePath + '/' +  self.postArr[i]['userId'];
            } else {
              self.postArr[i]['src'] = '';
            }
              console.log(self.postArr)
          })
        }
      }
        if (this.offerArr) {
        for (let i = 0; i < this.offerArr.length; i++) {
          this.imageExists(this.imagePath + '/' +  this.offerArr[i]
          ['userId'], function (exists) {
            if (exists) {
              self.offerArr[i]['src'] = self.imagePath + '/' +  self.offerArr[i]['userId'];
            } else {
              self.offerArr[i]['src'] = '';
            }
          })
        }
      }

        if (this.offerArr.length > 0 || this.postArr.length > 0) {
          this.noPostNOffer = false;
          this.finalOfferPostArr = this.postArr.concat(this.offerArr);
          //this.finalOfferPostArr = this.shuffle(this.finalOfferPostArr)
        } else {
          this.noPostNOffer = true;
        }

      } else {

        this.toastr.error('Either Pinboard expired or No record found');
        this.router.navigate(['/']);
      }
    },
      error => {

      })
  }
  getPinboardFollowerCount(pinboardId) {
    this.WebService.getPinboardFollowerCount(pinboardId, 'getPinboardFollowerCount').subscribe(resultFollowerData => {
      if (resultFollowerData['statusCode'] == 200) {
        this.followingCount =  resultFollowerData['responsePacket']['followingCount'];
      }
    },
      error => {

      })
  }
  isFollowPinboard(pinboardId) {
    this.WebService.follow(this.userId, pinboardId, 'isFollowPinboard').subscribe(resultIsFollowerData => {
      if (resultIsFollowerData['statusCode'] == 200) {
        console.log(resultIsFollowerData);
        if (resultIsFollowerData['responsePacket']['isFollowPinboard']){
          this.isFollowed = true;
        }else{
          this.isFollowed = false;
        }

      }
    },
      error => {

      })
  }
  followPinboard(pinboardId) {
    if (!localStorage.getItem('userId')){
      this.toastr.error("Please login to follow this pinboard");
      this.spinner.hide();
    } else {
      this.spinner.show();
      this.WebService.follow(this.userId, pinboardId, 'FollowPinboard').subscribe(resultFollowerData => {
        this.spinner.hide();
        if (resultFollowerData['statusCode'] == 200) {
          this.ngOnInit();
        }else{
          this.toastr.error(resultFollowerData['message']);
        }
      },
        error => {

        })
    }

  }
  unFollowPinboard(pinboardId) {
    if (!localStorage.getItem('userId')) {
      this.toastr.error("Please login to unfollow this pinboard");
      this.spinner.hide();
    } else {
      this.spinner.show();
    this.WebService.follow(this.userId, pinboardId, 'UnfollowPinboard').subscribe(resultFollowerData => {
      this.spinner.hide();
      if (resultFollowerData['statusCode'] == 200) {
       this.ngOnInit();
      } else {
        this.toastr.error(resultFollowerData['message']);
      }
    },
      error => {

      })
    }
  }
  getUserNotification() {
    this.WebService.getUserNotification(this.userId, 'getUserNotification').subscribe(resultNotificationCount => {
      if (resultNotificationCount['statusCode'] == 200) {
        this.unViewedCount = resultNotificationCount['responsePacket']['unViewedCount'];

      }
    },
      error => {

      })
  }
  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
  onCategoryChange(e:any){
    let selectElementText = e.target['options']
    [e.target['options'].selectedIndex].text;
    this.Post.category = selectElementText;
  }
  showType(event:any){
    if(event==1){
      this.htmlOptions = {
        offerBox: false,
        postBox: true,
      }
    } else if (event == 2) {
      this.htmlOptions = {
        offerBox: true,
        postBox: false,
      }
    }else{
      this.htmlOptions = {
        offerBox: true,
        postBox: true,
      }
    }
  }
  underDevelopment(){
    this.toastr.warning('Under Development');
  }
  showInfoDiv(){
    this.status = !this.status;
  }

  test(idx)
  {
    this.moreOfferBox[idx]=false;
  }
  //image uploading from form
  fileEvent(fileInput: any, fileType) {
    /* var os = window.navigator.platform.toLowerCase();
    if (os.indexOf('mac') != -1 || os.indexOf('ios') != -1 || os.indexOf('iphone') != -1) {
      var that = this;
      var orientation = function (file, callback) {
        that.fileReader = new FileReader();
        that.fileReader.onloadend = function () {
      var base64img = "data:" + file.type + ";base64," + that._arrayBufferToBase64(that.fileReader.result);

          var scanner = new DataView(that.fileReader.result);
          var idx = 0;
          var value = 1; // Non-rotated is the default
          if (that.fileReader.result.length < 2 || scanner.getUint16(idx) != 0xFFD8) {

            if (callback) {
              callback(base64img, value);
            }
            return;
          }
          idx += 2;
          var maxBytes = scanner.byteLength;
          while (idx < maxBytes - 2) {
            var uint16 = scanner.getUint16(idx);
            idx += 2;
            switch (uint16) {
              case 0xFFE1: // Start of EXIF
                var exifLength = scanner.getUint16(idx);
                maxBytes = exifLength - idx;
                idx += 2;
                break;
              case 0x0112: // Orientation tag
                value = scanner.getUint16(idx + 6, false);
                maxBytes = 0; // Stop scanning
                break;
            }
          }
          if (callback) {
            callback(base64img, value);
          }
        }
        that.fileReader.readAsArrayBuffer(file);
      };
        var files = fileInput.target.files;
        var file = files[0];
        if (file) {
          orientation(file, function (base64img, value) {
            console.log(that.rotation[value]);
            if (value == 3 || value == 6 || value == 8){
              that.Post.imageAangle = 9

            }

          });
        }
    } */
    this.htmlOptions.newupload = false;
    var files = fileInput.target.files;
    var file = files[0];
    if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {
      var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

      if (pos != -1) {
        if (file == undefined) {
          this.globalFilesArray.splice(pos, 1);
        } else {
          this.globalFilesArray[pos].fileType = fileType
          this.globalFilesArray[pos].file = file
        }
      } else {
        this.globalFilesArray.push({ fileType: fileType, file: file })
      }

      if (fileType == 'image') {
        this.htmlOptions.image = ""
      }

    } else {

      this.Post.image = "";
      alert("Only jpg, png and jpeg file format allowed");
      this.globalFilesArray = [];
      return false;
    }

  }
  //called this function to upload image before form submit
  multipleProductImageUploadl(callbackError, callbackSuccess, ) {
    var _that = this
    AWS.config.update({
      region: environment.aws_region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: environment.aws_IdentityPoolId
      })
    })

    const s3 = new AWS.S3({
      apiVersion: environment.aws_apiVersion,
      params: { Bucket: environment.aws_bucketName }
    })

    var ImagesUrls = []
    if (this.globalFilesArray.length > 0) {
      this.globalFilesArray.map((item) => {
        var params = {
          Bucket: environment.aws_bucketName,
          Key: '_cmsImages/' + new Date().getTime() + "__" + item.fileType + "__" + item.file.name,
          Body: item.file,
          ACL: 'public-read'
        };
        s3.upload(params, function (err, data) {
          if (err) {
            callbackError(err)
          } else {
            let fType = data.key.split("__")[1]
            ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: fType })
            if (ImagesUrls.length == _that.globalFilesArray.length) {
              callbackSuccess(ImagesUrls)
            }
          }
        })
      })
    } else {
      callbackSuccess("")
    }
  }
  onSubmit() {

    /* if (!this.userId) {
      this.toastr.error('Please log in to create a post or reply');
      this.spinner.show();
      return
    } */

    this.spinner.show();
    let self = this;
    /* this.multipleProductImageUploadl(
      function (error) {
        if (this.htmlOptions.editMode) {
          this.submitWithoutImage()
        } else {
          alert(error)
        }
      },
      function (success) {
        if (success == '') {

        } else {

          for (let i = 0; i < success.length; i++) {

            if (success[i].ImageType == "image") {
              self.Post.image = success[i].ImagePath
            }
          }
        } */
        self.Post.pinboardId = self.newPinboardId
        self.Post.userId = self.userId
        self.Post.userName = self.userName
        self.Post.expiryDate = new Date();
        self.Post.pinboardTitle = self.pinboardTitle;
        self.WebService.addPostData(self.Post, 'add').subscribe(result => {
          if (result['statusCode'] == 200) {
            /* self.Post.description = '';
            self.Post.tags = ''; */
            self.spinner.hide();
            var temp = self.htmlOptions.button;
            setTimeout(function () {
              self.toastr.success('Post added successfully', 'Success');
              window.location.reload();
            }, 1000);
            //self.ngOnInit();

          } else {
            self.toastr.error(result['message']);
            self.ngOnInit();
          }
        },
          error => {

          })
      /* }) */
  }
  dateTimeCalculation(postDate: any){
    var newDate = postDate.split(" ");
    var newDateArr = newDate[0].split("-");
    var newTimeArr = newDate[1].split(":");
    var date1 = new Date(parseInt(newDateArr[0]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[2]), parseInt(newTimeArr[0]), parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));
    //var date1 = new Date(date1 + ' UTC');
    var d1 = new Date();
    d1.toUTCString();
    var date2 = new Date(d1.getUTCFullYear(), d1.getUTCMonth(), d1.getUTCDate(), d1.getUTCHours(), d1.getUTCMinutes(), d1.getUTCSeconds());
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffSec = Math.ceil(timeDiff / 1000);
    return this.secondsToDhms(diffSec);
  }

  secondsToDhms(seconds) {

    var d = Math.floor(seconds / (3600 * 24));
    var h = Math.floor(seconds % (3600 * 24) / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 60);

    var dDisplay = d > 0 ? d + (d == 1 ? " day ago" : " days ago") : "";
    var hDisplay = h > 0 ? h + (h == 1 ? " hour ago" : " hours ago") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute ago" : " minutes ago") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " Just Now" : " Just Now") : "";

    if (d>0){
      return dDisplay
    } else if (h > 0){
      return hDisplay
    } else if (m > 0) {
      return mDisplay
    } else {
      return 'Just Now'
    }

  }

  postReport(ev:any){
    this.spinner.show();
    this.WebService.postAction(ev, 'reportActionOnPost').subscribe(resultNotificationCount => {
      this.spinner.hide();
      if (resultNotificationCount['statusCode'] == 200) {
        this.toastr.success('your request has been sent', 'Success');
        this.ngOnInit();
      }else{
        this.toastr.error(resultNotificationCount['message']);
      }
    },
      error => {

      })
  }
  pinboardAction(id, action) {
      var r = confirm("Are you sure? You will not be able to recover this in future!");
      if (r == true) {
        this.spinner.show();
        var self = this;
        this.WebService.postAction(id, action).subscribe(resultPinBoardActionData => {
          this.spinner.hide();
          if (resultPinBoardActionData.statusCode == 200) {
            setTimeout(function () {
              self.toastr.success('Post has been deleted successfully', 'Deleted');
            }, 1000);
            window.location.reload();
          } else {
            this.toastr.error(resultPinBoardActionData.message);
          }
        },
          error => {

          })
    }
  }
  setPostId(postId){
    this.commentPost.postId = postId;
  }
  addComment(postId,idx){

    if (!this.userId) {
      this.toastr.error('Please log in to create a post or reply');
      return
    }
    if (!this.commentPost[idx]){
      this.toastr.error('Please fill the text in comment box');
      return
    }
    this.spinner.show();
    let commentData = {
      "postId": postId,
      "userId": this.userId,
      "userName": this.userName,
      "commentText": this.commentPost[idx]
    }
    this.WebService.addComment(commentData, 'add-comment').subscribe(result => {
      if (result['statusCode'] == 200) {
        this.spinner.hide();
        var temp = this.htmlOptions.button;
        this.toastr.success('Comment added successfully', 'Success');
        this.commentPost[idx] = '';
        //this.getPinboardDetailsWithPostAndOffers(this.pinboardId, this.pinboardType);

        this.getComments(postId, idx)

      } else {
        this.toastr.error(result['message']);
        this.ngOnInit();
      }
    },
      error => {

      })
  }
  getComments(postId, idx){
    this.WebService.getCommentByPostId(postId, 'getCommentByPostId').subscribe(result => {
      let self = this;

      if (result['statusCode'] == 200) {
        this.postComments[postId] = result['responsePacket']
        var tempUrl1
        for (let i = 0; i < this.postComments[postId].length; i++){
          this.imageExists(this.imagePath+ '/' + this.postComments[postId][i]['userId'], function (exists) {
            if (exists){
              self.postComments[postId][i]['src'] = self.imagePath + '/' +  self.postComments[postId][i]['userId'];
            }else{
              self.postComments[postId][i]['src'] = '';
            }
          })
        }


        self.postComments[postId].sort(function (x, y) {
          var xUpdatedDate = self.changeDate(x.updatedDate);
          var yUpdatedDate = self.changeDate(y.updatedDate);
          if (xUpdatedDate < yUpdatedDate) {
            return -1;
          }
          if (xUpdatedDate > yUpdatedDate) {
            return 1;
          }
          return 0;
        });

        this.finalOfferPostArr[idx].commentCount = result['responsePacket'].length
      }
    },
      error => {

      })
  }
  changeDate(reqDate: any){
    var newDate = reqDate.split(" ");
    var newDateArr = newDate[0].split("-");
    var newTimeArr = newDate[1].split(":");
    var date = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]), parseInt(newTimeArr[0]), parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));
    return date.getTime();
  }
  openSharePopup(pinboardName: any, category : any, offerData: any){

    this.postForShare = environment.site_url +'pinboard/'+ category + '/' + pinboardName + '/' + offerData.id;
    this.ShareOfferData = offerData;

    document.getElementById("openExtraSetailsPopup").click();
  }
  createFBShareLink(postOffer: any) {

    let FBVars = {
      fbAppId: "1393148214161425",
      fbShareImg: 'https://adminstreetpin.s3.amazonaws.com/_cmsImages/1550664442592__offerIcon__Koala.jpg',
      fbShareName: postOffer.title,
      fbShareCaption: postOffer.title,
      fbShareDesc: postOffer.offerDescription,
      baseURL: "https://d2c1vgyoekfl5p.cloudfront.net/pinboard/" + this.pinboardId + '/' + postOffer.id +'/',
      redirect_uri: "https://d2c1vgyoekfl5p.cloudfront.net/pinboard/" + this.pinboardId + '/' + postOffer.id +'/'
    };
    var url = 'http://www.facebook.com/dialog/share?app_id=' + FBVars.fbAppId +
      '&picture=' + FBVars.fbShareImg +
      '&name=' + encodeURIComponent(FBVars.fbShareName) +
      '&caption=' + encodeURIComponent(FBVars.fbShareCaption) +
      '&description=' + encodeURIComponent(FBVars.fbShareDesc) +
      '&redirect_uri=' + FBVars.redirect_uri +
      '&link=' + FBVars.redirect_uri +
      '&display=popup';


    window.open(url,
      'feedDialog',
      'toolbar=0,status=0,width=626,height=436'
    );
  }
  createFBFeedLink(postOffer: any) {

    let FBVars = {
      fbAppId: "1393148214161425",
      fbShareImg: 'https://adminstreetpin.s3.amazonaws.com/_cmsImages/1550664442592__offerIcon__Koala.jpg',
      fbShareName: postOffer.title,
      fbShareCaption: postOffer.title,
      fbShareDesc: postOffer.offerDescription,
      baseURL: "https://d2c1vgyoekfl5p.cloudfront.net/pinboard/" + this.pinboardId + '/' + postOffer.id + '/',
      redirect_uri: "https://d2c1vgyoekfl5p.cloudfront.net/pinboard/" + this.pinboardId + '/' + postOffer.id + '/'
    };
    var url = 'http://www.facebook.com/dialog/feed?app_id=' + FBVars.fbAppId +
      /* '&picture=' + FBVars.fbShareImg + */
      /* '&name=' + encodeURIComponent(FBVars.fbShareName) + */
      /* '&caption=' + encodeURIComponent(FBVars.fbShareCaption) + */
     /*  '&description=' + encodeURIComponent(FBVars.fbShareDesc) + */
      '&redirect_uri=' + FBVars.redirect_uri +
      /* '&link=' + FBVars.redirect_uri + */
      '&display=popup';


    window.open(url,
      'feedDialog',
      'toolbar=0,status=0,width=626,height=436'
    );
  }

  share(url: string) {
    /* url = url+'/' */
    let params: UIParams = {
      href: url,
      method: 'share'
    };
    console.log(url)
    this.fb.ui(params)
      .then((res: UIResponse) => console.log(res))
      .catch((e: any) => console.error(e));

  }
  twitterShare(url: any, ShareOfferData: any){
    console.log(url);
      // Opens a pop-up with twitter sharing dialog
      var shareURL = "http://twitter.com/share?"; //url base
      //params
      var params = {
        url: url,
        text: ShareOfferData.title,
        via: "Streetpin",
        hashtags: ShareOfferData.servicesHashtags
      }
      for (var prop in params) shareURL += '&' + prop + '=' + encodeURIComponent(params[prop]);
      window.open(shareURL, '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
    }
  linkedInShare(url: any, ShareOfferData: any){
    console.log(url);
    var url1 = url;
    var title = "Replace this with a title.";
    var text = "Replace this with your share copy.";
    window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(url1), '', 'left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

    }

  dateToUK(postDate: any) {
    var newDate = postDate.split(" ");
    var newDateArr = newDate[0].split("-");
    var newTimeArr = newDate[1].split(":");
    var date1 = new Date(parseInt(newDateArr[0]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[2]), parseInt(newTimeArr[0]) + 1, parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));

    var curr_date = date1.getDate();
    var curr_month = date1.getMonth() + 1; //Months are zero based
    var curr_year = date1.getFullYear();
    var curr_year = date1.getFullYear();
    var curr_hrs = date1.getHours();
    var curr_mnt = date1.getMinutes();
    var curr_sec = date1.getSeconds();
    var newConvertedDate = curr_date + "-" + curr_month + "-" + curr_year + " " + curr_hrs + ":" + curr_mnt + ":" + curr_sec;
    //return date1.toLocaleString('en-GB', { timeZone: 'UTC' });
    return newConvertedDate;
  }
  addhttp(url) {
  if (!/^(f|ht)tps?:\/\//i.test(url)) {
    url = "http://" + url;
  }
  return url;
}
  updateUrl(event:any){
    console.log("event"+event);
  }

  custom_sort(a, b) {
    return new Date(a.lastUpdated).getTime() - new Date(b.lastUpdated).getTime();
}


  // The "callback" argument is called with either true or false
// depending on whether the image at "url" exists or not.
 imageExists(url, callback) {
  var img = new Image();
  img.onload = function () { callback(true); };
  img.onerror = function () { callback(false); };
  img.src = url;
}

  _arrayBufferToBase64(buffer) {
    var binary = ''
    var bytes = new Uint8Array(buffer)
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i])
    }
    return window.btoa(binary);
  }
  splitString(category: any) {
    if (category) {
      var cat = category
      if (category == "club-society") {
        cat = "club society";
      } else if (category == "flats-estates") {
        cat = "flats & estates ";
      } else if (category == "food-drink") {
        cat = "food & drink ";
      } else if (category == "travel-agent") {
        cat = "travel agent ";
      }
      return cat;
    }
  }
  substr(str: any) {
    if (str) {
      if (str.length > 150) {
        return str.substr(0, 150) + "...";
      } else {
        return str.substr(0, 150);
      }
    }
  }

  fileChangeEvent(event: any): void {
    this.htmlOptions.offerMedia = ""
    this.isUploaded = true;
    this.imageChangedEvent = event;
    this.imageCropper.autoCrop = true;
    let element: any = document.getElementsByClassName('source-image');
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.tampImageWithoutCrop = fileReader.result.toString();
    }
    fileReader.readAsDataURL(event.target.files[0]);
    this.detectFiles(event)

  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.tampImageWithCrop = event.base64;
    this.detectFiles(event)
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  cropImage() {
    this.IsCropedImage = true;
    this.showCropper = true;
    this.imageCropper.autoCrop = true;
    this.show = true;
  }
  fitToScreen() {
    this.IsCropedImage = true;
    this.show = true;
    this.imageCropper.autoCrop = false;
    this.showCropper = false;
  }
  Cancel() {
    this.IsCropedImage = false;
    this.show = false;
    this.imageCropper.resetCropperPosition();
    this.imageCropper.autoCrop = true;
    this.showCropper = true;
  }


  base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
      return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
      result = mime[1];
    }

    return result;
  }

  compressAndUploadFile() {

    if (!this.userId) {
      this.spinner.show();
      this.toastr.error('Please login to create a post or reply');
      this.spinner.hide();
      return
    }
    /* if (!f.valid) {
      document.getElementById("error_box").scrollIntoView();

      this.isValid = true;
      var that = this;

      setTimeout(() => {
        that.isValid = false;
      }, 5000);
      return;
    } */
    this.spinner.show();
    if (this.isUploaded) {

      var image: any
      if (this.IsCropedImage == true) {
        image = this.tampImageWithCrop;
      } else {
        image = this.tampImageWithoutCrop;
      }
      //  this.imageCompress.uploadFile().then(({image, orientation}) => {
      var ImageType = this.base64MimeType(image);

      if (this.imageCompress.byteCount(image) > 150 && this.IsCropedImage == false) {
        var quality;
        var size;
        if (ImageType == "image/png") {
          quality = 30;
          /* size = 10; */

        } else {
          /* size = 50; */
          quality = 30;
        }
        this.imageCompress.compressFile(image, this.orientation, size, quality).then(
          result => {
            this.croppedImage = result;
            this.UploadToS3(result)
            console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
          }
        );
      } else {
        this.UploadToS3(image)
      }

    } else {
      this.onSubmit()
    }
    //});
  }

  UploadToS3(image: any) {

    const base64Data = new Buffer(image.replace(/^data:image\/\w+;base64,/, ""), 'base64')
    const type = image.split(';')[0].split('/')[1]
    AWS.config.update({
      region: environment.aws_region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: environment.aws_IdentityPoolId
      })
    })
    const s3 = new AWS.S3({
      apiVersion: environment.aws_apiVersion,
      params: { Bucket: environment.aws_bucketName }
    })
    var params = {
      Bucket: environment.aws_bucketName,
      Key: '_cmsImages/' + new Date().getTime(),
      Body: base64Data,
      ACL: 'public-read',
      ContentEncoding: 'base64', // required
      ContentType: "image/" + type // required. Notice the back ticks
    };
    var self = this
    s3.upload(params, function (err, data) {
      if (err) { return console.log(err) }
      console.log('data', data.Location)
      self.Post.image = data.Location;
      self.onSubmit()
    })
  }
  detectFiles(event) {
    var file = event.target.files[0];
    this.getOrientation(file, (orientation) => {
      this.orientation = orientation;
      console.log('orientation', orientation)
    });
  }

  getOrientation(file, callback) {

    var reader: any,
      target: EventTarget;
    reader = new FileReader();
    reader.onload = (event) => {

      var view = new DataView(event.target.result);

      if (view.getUint16(0, false) != 0xFFD8) return callback(-2);

      var length = view.byteLength,
        offset = 2;

      while (offset < length) {
        var marker = view.getUint16(offset, false);
        offset += 2;

        if (marker == 0xFFE1) {
          if (view.getUint32(offset += 2, false) != 0x45786966) {
            return callback(-1);
          }
          var little = view.getUint16(offset += 6, false) == 0x4949;
          offset += view.getUint32(offset + 4, little);
          var tags = view.getUint16(offset, little);
          offset += 2;

          for (var i = 0; i < tags; i++)
            if (view.getUint16(offset + (i * 12), little) == 0x0112)
              return callback(view.getUint16(offset + (i * 12) + 8, little));
        }
        else if ((marker & 0xFF00) != 0xFF00) break;
        else offset += view.getUint16(offset, false);
      }
      return callback(-1);
    };

    reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
  };

}
