import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from "ngx-stripe";
import { WebService } from '../../service/web.service'
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.css']
})
export class StripeComponent implements OnInit {
  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  user : any ={};
  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: 'rgb(45, 48, 51)',
        color: 'rgb(114, 114, 114)',
        lineHeight: '40px',
        // fontWeight: 300,
        // fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '16px',
        '::placeholder': {
          color: 'rgb(114, 114, 114)'
        }
      }
    }
  };
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  elementsOptions: ElementsOptions = {
    locale: 'en'
  };
  constructor(
    private stripeService: StripeService,
    private WebService: WebService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }
  stripeTest: FormGroup;
  ngOnInit() {
  }

  buy() {
    this.stripeService
      .createToken(this.card.getCard(), { name })
      .subscribe(result => {
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          console.log(result.token.id);
        } else if (result.error) {
          // Error creating the token
          this.toastr.error(result.error.message);
        }
      });
  }
}
