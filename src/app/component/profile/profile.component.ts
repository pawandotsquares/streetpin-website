import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Profile } from 'src/app/models/profile';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';
import * as AWS from 'aws-sdk'
import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  userId: any;
  selectedIndex : any;
  ProfileData: object;
  htmlOptions: any = {};
  onBoarding: any = {};
  profileActivityData: object;
  message: any;
  profileImage: string;
  globalFilesArray: any = []
  offerLocationData: object;
  publicProfileData: object;
  base64textString: any;
  base64textStringForSave: any;
  profileFileType: any;
  public Profile: Profile = new Profile({});
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    localStorage.setItem('returnUrl', '/posts');
    this.selectedIndex = 1;
    this.spinner.show();
    this.setHtmlOptions();
    this.getProfileContent()

  }
  setHtmlOptions() {
    this.userId = localStorage.getItem('userId');
    this.htmlOptions = {
      editMode: false,
      publicProfileMode: false,
      privateProfileMode: true
    }
  }
  cancelButton() {
    this.getProfileContent();
    this.htmlOptions = {
      editMode: false,
      publicProfileMode: false,
      privateProfileMode: true
    }
  }
  getProfileContent() {
    this.WebService.profileData(this.userId, 'getUserProgile').subscribe(resultProfileData => {

      if (resultProfileData['statusCode'] == 200) {
        this.Profile = new Profile(resultProfileData['responsePacket'])
        if (this.Profile.phoneNumber == 'null'){
          this.Profile.phoneNumber = '';
        }
        localStorage.setItem('WebUserProfileImage', resultProfileData['responsePacket'].profileImage);
        localStorage.setItem('firstName', resultProfileData['responsePacket']['firstName']);
        localStorage.setItem('userName', resultProfileData['responsePacket']['userName']);
       /*  if (localStorage.getItem('userOnboardingSkip') == 'false') {
          document.getElementById('onBoardingClick').click();
        } */
        this.getUserActivity(resultProfileData['responsePacket']['userName']);
      } else {
        this.spinner.hide();
        this.message = resultProfileData;
      }
    },
      error => {

      })
  }

  editProfile(profileId) {
    this.htmlOptions = {
      editMode: true,
      publicProfileMode: false,
      privateProfileMode: false
    }
  }

  fileEvent1(event,test) {
    console.log(event);
    var files = event.target.files;
    var file = files[0];
    this.profileFileType = files[0]['type'];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);

    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    /* this.base64textString = btoa(binaryString); */
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.base64textStringForSave = btoa(binaryString);
  }

  UploadToS3(callbackError, callbackSuccess, ) {
    AWS.config.update({
      region: environment.aws_region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: environment.aws_IdentityPoolId
      })
    })
    const s3 = new AWS.S3({
      apiVersion: environment.aws_apiVersion,
      params: { Bucket: environment.aws_bucketName }
    })
    var params = {
      Bucket: environment.aws_bucketName,
      Key: '_cmsImages/' + new Date().getTime(),
      Body: this.base64textStringForSave,
      ACL: 'public-read',
      ContentEncoding: 'base64', // required
      ContentType: this.profileFileType // required. Notice the back ticks
    };
    var self = this
    var ImagesUrls = []
    s3.upload(params, function (err, data) {

      if (err) {
            callbackError(err)
          } else {

        ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: this.profileFileType })

          callbackSuccess(ImagesUrls)

          }
    })
  }
  onSubmit() {
    this.spinner.show();
    this.Profile.userRole = "";
    let self = this;
    this.multipleProductImageUploadl(
      function (error) {
        if (this.htmlOptions.editMode) {
          this.submitWithoutImage()
        } else {
          alert(error)
        }
      },
      function (success) {
        if (success == '') {

        } else {
          console.log('in')
          self.Profile.profileImage = success[0].ImagePath
          localStorage.setItem('WebUserProfileImage', success[0].ImagePath);
        }
        self.WebService.profileUpdate(self.Profile, 'saveUserProgile').subscribe(result => {
          self.spinner.hide();
          if (result['statusCode'] == 200) {
            self.toastr.success('Profile updated successfully', 'Success');
            setTimeout(function () {
              window.location.reload();
            }, 1500);

          } else {
            self.toastr.error(result['message']);
          }
        },
          error => {

          })
      })
  }

  getUserActivity(userName: any) {
    this.spinner.hide();
    this.WebService.publicProfileData(userName, 'getUserActivity').subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        this.publicProfileData = resultProfileData['responsePacket']
        this.profileActivityData = this.publicProfileData['userActivityList'];
      }
    },
      error => {

      })
  }
  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
  handleAddressChange(event: any) {
    this.Profile.location = event.formatted_address;
    this.WebService.getlatlng(event.formatted_address).subscribe(resultProfileData => {
      if (resultProfileData.status == "OK") {
        this.Profile.latitude = resultProfileData.results[0].geometry.location.lat;
        this.Profile.longitude = resultProfileData.results[0].geometry.location.lng;
      } else {
        this.Profile.latitude = '';
        this.Profile.longitude = '';
      }
    },
      error => {

      })
  }
  fileEvent(fileInput: any, fileType) {
    this.htmlOptions.newupload = false;
    var files = fileInput.target.files;
    var file = files[0];
    if (file.size > 5242880) {
      this.Profile.profileImage = "";
      alert("Image size should be less than 5MB");
      this.globalFilesArray = [];
      return false;
    } else if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {
      var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

      if (pos != -1) {
        if (file == undefined) {
          this.globalFilesArray.splice(pos, 1);
        } else {
          this.globalFilesArray[0].fileType = fileType
          this.globalFilesArray[0].file = file
        }
      } else {
        this.globalFilesArray.push({ fileType: fileType, file: file })
      }

      if (fileType == 'profileImage') {
        this.htmlOptions.profileImage = ""
      }

    } else {

      this.Profile.profileImage = "";

      alert("Only jpg, png and jpeg file format allowed");
      this.globalFilesArray = [];
      return false;
    }

  }
  multipleProductImageUploadl(callbackError, callbackSuccess, ) {

    var _that = this
    AWS.config.update({
      region: environment.aws_region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: environment.aws_IdentityPoolId
      })
    })

    const s3 = new AWS.S3({
      apiVersion: environment.aws_apiVersion,
      params: { Bucket: environment.aws_bucketName }
    })

    var ImagesUrls = []
    if (this.globalFilesArray.length > 0) {
      this.globalFilesArray.map((item) => {
        var params = {
          Bucket: environment.aws_bucketName,
          Key: environment.aws_folderName +'/'+ this.userId,
          Body: item.file,
          ACL: 'public-read'
        };
        s3.upload(params, function (err, data) {
          if (err) {
            callbackError(err)
          } else {
            let fType = data.key.split("__")[1]
            ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: fType })
            if (ImagesUrls.length == _that.globalFilesArray.length) {
              callbackSuccess(ImagesUrls)
            }
          }
        })
      })
    } else {
      callbackSuccess("")
    }
  }

  /* uploadFile(callbackError, callbackSuccess, ) {
    console.log(this.globalFilesArray);
    const contentType = this.globalFilesArray[0]['file']['type'];
    const bucket = new AWS.S3(
      {
        accessKeyId: environment.AWSAccessKey,
        secretAccessKey: environment.SecretAccessKey,
        region: environment.region
      }
    );
    const params = {
      Bucket: environment.aws_bucketName,
      Key: environment.aws_folderName + '/' + this.userId,
      Body: this.globalFilesArray[0]['file'],
      ACL: 'public-read',
      ContentType: contentType
    };
    bucket.upload(params, function (err, data) {
      if (err) {
        callbackError(err)
        console.log('There was an error uploading your file: ', err);
        return false;
      }
      callbackSuccess(data)
      console.log('Successfully uploaded file.', data);
      return true;
    });

  } */
  /* uploadImage(){

        let bucket = new AWS.S3({
          accessKeyId: '',
          secretAccessKey: '',
          region: ''
        }
        );
        let encodedImage = JSON.parse("event.body").certificate;
        let decodedImage = Buffer.from(encodedImage, 'base64');

        const certparams = {
          Bucket: process.env['BucketName'],
          Key: process.env['CertName'],
          ACL: 'public-read',
          Body: decodedImage
        };

        bucket.upload(certparams, function (err, data) {
          if (err) {
            console.log(err);

          }
          let resp = {
            "file": certparams.Key
          }

        });
  } */
  changeModule(module: string) {
    this.ngOnInit();
  }

  dateTimeCalculation(postDate: any) {

    var newDate = postDate.split(" ");
    var newDateArr = newDate[0].split("-");
    var newTimeArr = newDate[1].split(":");
    var date1 = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]), parseInt(newTimeArr[0]), parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));
    /* var date1 = new Date(date1+' UTC');
    var date2 = new Date(); */
    var d1 = new Date();
    d1.toUTCString();
    var date2 = new Date(d1.getUTCFullYear(), d1.getUTCMonth(), d1.getUTCDate(), d1.getUTCHours(), d1.getUTCMinutes(), d1.getUTCSeconds());
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffSec = Math.ceil(timeDiff / 1000);
    return this.secondsToDhms(diffSec);
  }

  secondsToDhms(seconds) {

    var d = Math.floor(seconds / (3600 * 24));
    var h = Math.floor(seconds % (3600 * 24) / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 60);

    var dDisplay = d > 0 ? d + (d == 1 ? " day " : " days ") : "";
    var hDisplay = h > 0 ? h + (h == 1 ? " hour " : " hours ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute " : " minutes ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";

    if (d > 0) {
      return dDisplay
    } else if (h > 0) {
      return hDisplay
    } else if (m > 0) {
      return mDisplay
    } else {
      return sDisplay
    }

  }
  showDiv(index) {
    console.log(index)
    this.selectedIndex = index;
  }
  onFilterChange(event:any){
    console.log(event);
    if(event == 'A'){
      localStorage.setItem('userOnboardingSkip','true')
    }else{
      localStorage.setItem('userOnboardingSkip', 'false')
    }

  }
}
