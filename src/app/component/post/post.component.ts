import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post';
import { Pinboard } from 'src/app/models/pinboard';
import * as AWS from 'aws-sdk'
import { AuthService } from 'src/app/service/auth.service';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-post',
	templateUrl: './post.component.html',
	styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

	@Output('changeModule')

	public moduleChanged = new EventEmitter<string>();
	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};
	pp: number;
	userId: any;
	htmlOptions: any = {};
	image: string;
	imageUplaodOption: boolean;
	categorydata: any;
	globalFilesArray: any = []
	postData: object;
	userData: object;
	users: any = [];

	public Post: Post = {} as Post;
	public pinboard: Pinboard = new Pinboard({});

	constructor(
		private WebService: WebService,
		private router: Router,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
		private authService: AuthService,
	) { }

	ngOnInit() {
		localStorage.setItem('returnUrl', '/profile');
		this.pp = 1;
		this.spinner.show();
		this.userId = localStorage.getItem('userId');

		this.getMyPostList();
		this.setHtmlOptions();
		this.getPostCategory();

	}
	setHtmlOptions() {
		this.htmlOptions = {
			editMode: false,
			viewMode: false
		}
	}
	//hide the form and display listing page
	cancelButton() {
		this.htmlOptions = {
			editMode: false
		}
	}

	changeModule(module: string) {
		this.ngOnInit();
	}


	//blank offer form will open to add new offer
	addPost() {
		this.htmlOptions.image = "";
		this.htmlOptions.button = false;
		this.htmlOptions.editMode = true;
		this.htmlOptions.viewMode = false;
		this.Post = {} as Post;
	}

	//edit Post form will open with data of selected records
	editPost(id) {
		this.htmlOptions.newupload = true;
		this.spinner.show();
		this.htmlOptions.button = true;
		this.htmlOptions.editMode = true;
		this.htmlOptions.viewMode = false;
		this.WebService.postAction(id, 'getById').subscribe(resultpostData => {
			this.spinner.hide();
			console.log(resultpostData);
			if (resultpostData.statusCode == 200) {
				this.Post = resultpostData.responsePacket
				this.htmlOptions.image = resultpostData.responsePacket.image
			}
		},
			error => {

			})

	}
	//category listing
	getPostCategory() {
		this.WebService.getPostCategory('getPostCategory').subscribe(resultcategoryData => {
			if (resultcategoryData['statusCode'] == 200) {
				this.categorydata = resultcategoryData['responsePacket'];
			}
		},
			error => {

			})
	}
	//image uploading from form
	fileEvent(fileInput: any, fileType) {
		this.htmlOptions.newupload = false;
		var files = fileInput.target.files;
		var file = files[0];
		if (file.size > 5242880) {
			this.Post.image
			alert("Image size should be less than 5MB");
			this.globalFilesArray = [];
			return false;
		} else if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {
			var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

			if (pos != -1) {
				if (file == undefined) {
					this.globalFilesArray.splice(pos, 1);
				} else {
					this.globalFilesArray[pos].fileType = fileType
					this.globalFilesArray[pos].file = file
				}
			} else {
				this.globalFilesArray.push({ fileType: fileType, file: file })
			}

			if (fileType == 'image') {
				this.htmlOptions.image = ""
			}

		} else {

			this.Post.image = "";
			alert("Only jpg, png and jpeg file format allowed");
			this.globalFilesArray = [];
			return false;
		}

	}
	//called this function to upload image before form submit
	multipleProductImageUploadl(callbackError, callbackSuccess, ) {
		var _that = this
		AWS.config.update({
			region: environment.aws_region,
			credentials: new AWS.CognitoIdentityCredentials({
				IdentityPoolId: environment.aws_IdentityPoolId
			})
		})

		const s3 = new AWS.S3({
			apiVersion: environment.aws_apiVersion,
			params: { Bucket: environment.aws_bucketName }
		})

		var ImagesUrls = []
		if (this.globalFilesArray.length > 0) {
			this.globalFilesArray.map((item) => {
				var params = {
					Bucket: environment.aws_bucketName,
					Key: '_cmsImages/' + new Date().getTime() + "__" + item.fileType + "__" + item.file.name,
					Body: item.file,
					ACL: 'public-read'
				};
				s3.upload(params, function (err, data) {
					if (err) {
						callbackError(err)
					} else {
						let fType = data.key.split("__")[1]
						ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: fType })
						if (ImagesUrls.length == _that.globalFilesArray.length) {
							callbackSuccess(ImagesUrls)
						}
					}
				})
			})
		} else {
			callbackSuccess("")
		}
	}

	//fatching the list of users offr list
	getMyPostList() {
		this.WebService.getMyPostList(this.userId, 'getMyPostList').subscribe(resultpostData => {
			this.spinner.hide();
			if (resultpostData['statusCode'] == 200) {
				this.postData = resultpostData['responsePacket']
			} else {
			}
		},
			error => {

			})
	}
	dateFormate(e) {
		var curr_date: any;
		var curr_month: any;
		curr_date = e.getDate();
		if (curr_date < 10) curr_date = '0' + curr_date;
		curr_month = e.getMonth() + 1; //Months are zero based
		if (curr_month < 10) curr_month = '0' + curr_month;
		var curr_year = e.getFullYear();
		var newExpDate = curr_date + "-" + curr_month + "-" + curr_year;

		return newExpDate;
	}
	onSubmit() {
		this.spinner.show();
		let self = this;
		this.multipleProductImageUploadl(
			function (error) {
				if (this.htmlOptions.editMode) {
					this.submitWithoutImage()
				} else {
					alert(error)
				}
			},
			function (success) {
				if (success == '') {

				} else {

					for (let i = 0; i < success.length; i++) {

						if (success[i].ImageType == "image") {
							self.Post.image = success[i].ImagePath
						}


					}

				}
				self.WebService.addPostData(self.Post, 'add').subscribe(result => {
					if (result['statusCode'] == 200) {
						self.spinner.hide();
						var temp = self.htmlOptions.button;
						setTimeout(function () {
							if (temp) {
								self.toastr.success('Record updated successfully', 'Success');
							} else {
								self.toastr.success('Record saved successfully', 'Success');
							}
							window.location.reload();

						}, 500);

					} else {
						self.toastr.error(result['message']);
						self.router.navigate(["credits"]);
					}
				},
					error => {

					})
			})
	}

	//to active inactive and delete we called this function from form
	postAction(id, action) {
		if (action == "delete") {
			var r = confirm("Are you sure? You will not be able to recover this in future!");
			if (r == true) {
				this.spinner.show();
				let self = this;
				this.WebService.postAction(id, action).subscribe(resultVoucherActionData => {
					this.spinner.hide();
					if (resultVoucherActionData.statusCode == 200) {
						setTimeout(function () {
							if (action == "active") {
								self.toastr.success('Record has been activated successfully', 'Success');
							} else if (action == "inactive") {
								self.toastr.success('Record has been deactivated successfully', 'Success');

							} else if (action == "delete") {
								self.toastr.success('Record has been deleted successfully', 'Success');
							}
						}, 1000);
						this.postData = resultVoucherActionData.responsePacket
						this.ngOnInit();
					} else {
						localStorage.setItem('statusCode', resultVoucherActionData.message);
					}
				},
					error => {

					})
			}
		} else {
			this.spinner.show();
			this.WebService.postAction(id, action).subscribe(resultVoucherActionData => {
				this.spinner.hide();
				let self = this;
				if (resultVoucherActionData.statusCode == 200) {
					setTimeout(function () {
						if (action == "active") {
							self.toastr.success('Record has been activated successfully', 'Success');
						} else if (action == "inactive") {
							self.toastr.success('Record has been deactivated successfully', 'Success');

						} else if (action == "delete") {
							self.toastr.success('Record has been deleted successfully', 'Success');
						}
					}, 1000);
					this.postData = resultVoucherActionData.responsePacket
					this.ngOnInit();
				} else {
					localStorage.setItem('statusCode', resultVoucherActionData.message);
				}
			},
				error => {

				})
		}

	}
	onCategoryChange(e: any) {
		let selectElementText = e.target['options']
		[e.target['options'].selectedIndex].text;
		this.Post.category = selectElementText;
		this.Post.isChangeExpiryDate = true;
	}

	dateToUK(postDate: any) {
		var newDate = postDate.split(" ");
		var newDateArr = newDate[0].split("-");
		var newTimeArr = newDate[1].split(":");
		var date1 = new Date(parseInt(newDateArr[0]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[2]), parseInt(newTimeArr[0])+1, parseInt(newTimeArr[1]), parseInt(newTimeArr[2]));

		var curr_date = date1.getDate();
		var curr_month = date1.getMonth() + 1; //Months are zero based
		var curr_year = date1.getFullYear();
		var curr_year = date1.getFullYear();
		var curr_hrs = date1.getHours();
		var curr_mnt = date1.getMinutes();
		var curr_sec = date1.getSeconds();
		var newConvertedDate = curr_date + "-" + curr_month + "-" + curr_year + " " + curr_hrs + ":" + curr_mnt + ":" + curr_sec;
		//return date1.toLocaleString('en-GB', { timeZone: 'UTC' });
		return newConvertedDate;
	}
}
