import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WebService } from 'src/app/service/web.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-notification-count',
  templateUrl: './notification-count.component.html',
  styleUrls: ['./notification-count.component.css']
})
export class NotificationCountComponent implements OnInit {
  unViewedCount: number;
  userId: any;
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    this.getUserNotification();
    //this.getProfileContent();
  }

  getUserNotification() {
    this.WebService.getUserNotification(this.userId, 'getUserNotification').subscribe(resultNotificationCount => {
      if (resultNotificationCount['statusCode'] == 200) {
        this.unViewedCount = resultNotificationCount['responsePacket']['unViewedCount'];
      }
    },
      error => {

      })
  }
  getProfileContent() {
    this.WebService.profileData(this.userId, 'getUserProgile').subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        if (resultProfileData.isInstantAlerts){
          this.getUserNotification();
        }
      }
    },
      error => {

      })
  }
}
