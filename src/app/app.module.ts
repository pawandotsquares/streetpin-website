
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PinboardComponent } from './component/pinboard/pinboard.component';
import { TownpinboardComponent } from './component/townpinboard/townpinboard.component';
import { HomeComponent } from './component/home/home.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { MenuComponent } from './component/menu/menu.component';
import { AppRoutingModule } from './app-routing.module';
import { SignupComponent } from './component/signup/signup.component';
import { LoginComponent } from './component/login/login.component';
import { ProfileComponent } from './component/profile/profile.component';
import { ForgotPasswordComponent } from './component/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './component/reset-password/reset-password.component';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';

import { TermAndConditionComponent } from './component/term-and-condition/term-and-condition.component';
import { SubscriptionPlanComponent } from './component/subscription-plan/subscription-plan.component';
import { UserSubscriptionComponent } from './component/user-subscription/user-subscription.component';
import { ProfileSidebarComponent } from './component/profile-sidebar/profile-sidebar.component';
import { MypinboardComponent } from './component/mypinboard/mypinboard.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AccordionModule } from 'primeng/accordion';     //accordion and accordion tab
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'primeng/calendar';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { OfferComponent } from './component/offer/offer.component';
import { PostComponent } from './component/post/post.component';
import { NotificationComponent } from './component/notification/notification.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { UserComponent } from './component/user/user.component';
import { DatePipe } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
/* import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'; */
import { NgxPaginationModule } from 'ngx-pagination';
import { NotificationCountComponent } from './component/notification-count/notification-count.component';
import { AboutComponent } from './component/about/about.component';
import { HowtoComponent } from './component/howto/howto.component';
import { PrivacyComponent } from './component/privacy/privacy.component';
import { ContactComponent } from './component/contact/contact.component';
import { StaticMenuComponent } from './component/static-menu/static-menu.component';
import { NgxStripeModule } from 'ngx-stripe';
import { StripeComponent } from './component/stripe/stripe.component';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import { FacebookModule } from 'ngx-facebook';
import { GooglemapComponent } from './component/googlemap/googlemap.component';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { LocalHistoryComponent } from './component/local-history/local-history.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { PinboardPlansComponent } from './component/pinboard-plans/pinboard-plans.component';
import {NgxImageCompressService} from 'ngx-image-compress';
import { BlogComponent } from './component/blog/blog.component';
import { HttpConfigInterceptor } from './interceptor/httpconfig.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    PinboardComponent,
    TownpinboardComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    SignupComponent,
    LoginComponent,
    ProfileComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    TermAndConditionComponent,
    SubscriptionPlanComponent,
    UserSubscriptionComponent,
    ProfileSidebarComponent,
    MypinboardComponent,
    OfferComponent,
    PostComponent,
    NotificationComponent,
    UserComponent,
    NotificationCountComponent,
    AboutComponent,
    HowtoComponent,
    PrivacyComponent,
    ContactComponent,
    StaticMenuComponent,
    StripeComponent,
    GooglemapComponent,
    LocalHistoryComponent,
    PinboardPlansComponent,
    LocalHistoryComponent,
    BlogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AgmJsMarkerClustererModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCDt53lCO5hH0Ka6dYDLDdlRoeCIuGIq_M',
      libraries: ['places']
    }),
    NgxStripeModule.forRoot('pk_live_KiFvZrtyDKmPVvXkF5PikcwI'),
    GooglePlaceModule,
    HttpClientModule,
    HttpModule,
    NgxTrimDirectiveModule,
    NgxSpinnerModule,
    CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(
      {
        preventDuplicates: true,
      }
    ), // ToastrModule added
    AccordionModule,
    CalendarModule,
    CountdownTimerModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    InfiniteScrollModule,
    NgxPaginationModule,
    // add social buttons module to NgModule imports
    JwSocialButtonsModule,
    FacebookModule.forRoot(),
    OverlayPanelModule,
    ImageCropperModule,

  ],
  providers: [
    DatePipe,
    NgxImageCompressService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})


export class AppModule { }

/* platformBrowserDynamic().bootstrapModule(AppModule); */