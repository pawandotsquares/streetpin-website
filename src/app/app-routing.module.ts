import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { SignupComponent } from './component/signup/signup.component';
import { ForgotPasswordComponent } from './component/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './component/reset-password/reset-password.component';
import { ProfileComponent } from './component/profile/profile.component';
import { PinboardComponent } from './component/pinboard/pinboard.component';
import { TermAndConditionComponent } from './component/term-and-condition/term-and-condition.component';
import { SubscriptionPlanComponent } from './component/subscription-plan/subscription-plan.component';
import { UserSubscriptionComponent } from './component/user-subscription/user-subscription.component';
import { MypinboardComponent } from './component/mypinboard/mypinboard.component';
import { OfferComponent } from './component/offer/offer.component';
import { PostComponent } from './component/post/post.component';
import { NotificationComponent } from './component/notification/notification.component';
import { UserComponent } from './component/user/user.component';
import { HowtoComponent } from './component/howto/howto.component';
import { AboutComponent } from './component/about/about.component';
import { PrivacyComponent } from './component/privacy/privacy.component';
import { StripeComponent } from './component/stripe/stripe.component';
import { ContactComponent } from './component/contact/contact.component';
import { GooglemapComponent } from './component/googlemap/googlemap.component';
import { PinboardPlansComponent } from './component/pinboard-plans/pinboard-plans.component';
import { BlogComponent } from './component/blog/blog.component';

const routes: Routes = [
  { path: 'blog', component: BlogComponent },
  { path: 'googlemap', component: GooglemapComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'login', component: LoginComponent },
  { path: 'login/:id', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'mypinboard', component: MypinboardComponent, canActivate: [AuthGuard] },
  { path: 'notification', component: NotificationComponent, canActivate: [AuthGuard] },
  /* { path: 'pinboard', component: PinboardComponent, canActivate: [AuthGuard] }, */
  { path: 'pinboard/:id', component: PinboardComponent },
  { path: 'pinboard/:catId/:id', component: PinboardComponent },
  { path: 'pinboard/:catId/:id/:offer', component: PinboardComponent },
  { path: 'pinboard/:id/:offer', component: PinboardComponent },
  { path: 'pricing', component: PinboardPlansComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'posts', component: PostComponent, canActivate: [AuthGuard] },
  { path: 'offers', component: OfferComponent, canActivate: [AuthGuard] },
  { path: 'resetpassword/:id', component: ResetPasswordComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'credits', component: UserSubscriptionComponent, canActivate: [AuthGuard] },
  { path: 'contact', component: ContactComponent},
  { path: 'my-offer-credits', component: SubscriptionPlanComponent },
  { path: 'terms-conditions', component: TermAndConditionComponent },
  { path: 'howto', component: HowtoComponent },
  { path: 'stripe', component: StripeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'user/:id', component: UserComponent},
  { path: '', component: HomeComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },

];

@NgModule({
  imports: [
    [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {

}
